<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$bkk = $this->Models->get_data('tbl_bkk')->result();
		$data = array('content' => 'Pengguna/main', 'header'=>'Pengguna','table_header'=>'Data Pengguna','bkk'=>$bkk );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_pengguna();
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->username;
			$data[] = $value->nama_bkk;
			$data[] = $value->alamat_bkk;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Pengguna/get_data/').'" onclick="edit('.$value->id_users.');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>';
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_bkk()
	{
		$get = $this->Models->get_data('tbl_bkk');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data['id'] = $value->id_bkk;
			$data['text'] = $value->nama_bkk;
			
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode($output);
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tbl_users','id_users',$id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id_users');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                $save = array(
                    'username' => $this->input->post('username'),
                    'id_bkk' => $this->input->post('id_bkk'),
                    'password' => md5($this->input->post('password')),
                    'level' => $this->input->post('level'),
                    );
                $save = $this->Models->update('tbl_users','id_users',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
                $save = array(
                    'username' => $this->input->post('username'),
                    'id_bkk' => $this->input->post('id_bkk'),
                    'password' => md5($this->input->post('password')),
                    'level' => $this->input->post('level'),
                    );
                $save = $this->Models->insert('tbl_users',$save);
                if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Tambahkan"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('username','Username', 'required');
		$this->form_validation->set_rules('id_bkk','BKK', 'required');
		$this->form_validation->set_rules('password','Password', 'required');
		$this->form_validation->set_rules('level','Level', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */