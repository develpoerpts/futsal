<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Admin/main', 'header'=>'Admin','table_header'=>'Data Admin' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tb_admin');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama;
			$data[] = $value->username;
			$data[] = $value->email;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Admin/get_data/').'" onclick="edit('.$value->id.');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tb_Admin','id',$id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                    'nama' => $this->input->post('nama'),
										'username' => $this->input->post('username'),
										'password' => $this->input->post('password'),
										'email' => $this->input->post('email'),
                    );
                $save = $this->Models->update('tb_admin','id',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
                $save = array(
									'nama' => $this->input->post('nama'),
									'username' => $this->input->post('username'),
									'password' => $this->input->post('password'),
									'email' => $this->input->post('email'),
                    );
                $save = $this->Models->insert('tb_admin',$save);
                if ($save==true) {
                    echo json_encode(array("success" => TRUE));
                } else {
                    echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
                }
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama','Nama', 'required');
		$this->form_validation->set_rules('username','Username', 'required');
		$this->form_validation->set_rules('password','Password', 'required');
		$this->form_validation->set_rules('email','Email', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
