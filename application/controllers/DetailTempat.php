<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailTempat extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('App_models', 'Models');
    $this->load->helper('My_helper');
  }


	public function detail()
	{
    $isi['content']		= 'frontend/tampilan_content_futsal';
    $id = $this->uri->segment(3);
    $isi['data']		= $this->Models->get_lapangan_Id($id);

  	$this->load->view('frontend/tampilan_frontend', $isi);
	}


}
