<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Futsal extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Futsal/main', 'header' => 'Futsal', 'table_header' => 'Data Futsal');
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tb_futsal');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_futsal;
			$data[] = $value->alamat;
			$data[] = $value->keterangan;
			$data[] = $value->nama_bank;
			$data[] = $value->no_rek;
			$data[] = $value->no_telepon;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="' . base_url('Futsal/get_data/') . '" onclick="edit(' . $value->id_futsal . ');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tb_Futsal', 'id_futsal', $id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_futsal');
		$username = $this->input->post('username');
		$metode = $this->input->post('metode');
		if ($id != '') {
			if ($valid == TRUE) {
				$cekusername = $this->Models->cekUsernameFutsal($username);
				if ($cekusername == true) {
					$err = array('username' => '<p class="text-danger">Username sudah ada</p>');
					echo json_encode(array('fail' => true, 'notif' => "Username sudah ada", "messages" => $err));
				} else {
					$save = array(
						'nama_futsal' => $this->input->post('nama_futsal'),
						'alamat' => $this->input->post('alamat'),
						'keterangan' => $this->input->post('keterangan'),
						'nama_bank' => $this->input->post('nama_bank'),
						'no_rek' => $this->input->post('no_rek'),
						'username' => $this->input->post('username'),
						'password' => md5($this->input->post('password')),
						'no_telepon' => $this->input->post('no_telepon'),
					);
					$save = $this->Models->update('tb_futsal', 'id_futsal', $id, $save);
					if ($save == true) {
						echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
					} else {
						echo json_encode(array('error_db' => $this->db->error()));
					}
				}
			}
		} else {
			if ($valid == TRUE) {
				$cekusername = $this->Models->cekUsernameFutsal($username);
				if ($cekusername == true) {
					$err = array('username' => '<p class="text-danger">Username sudah ada</p>');
					echo json_encode(array('fail' => true, 'notif' => "Username sudah ada", "messages" => $err));
				} else {
					$save = array(
						'nama_futsal' => $this->input->post('nama_futsal'),
						'alamat' => $this->input->post('alamat'),
						'keterangan' => $this->input->post('keterangan'),
						'nama_bank' => $this->input->post('nama_bank'),
						'no_rek' => $this->input->post('no_rek'),
						'username' => $this->input->post('username'),
						'password' => md5($this->input->post('password')),
						'no_telepon' => $this->input->post('no_telepon'),
					);
					$save = $this->Models->insert('tb_futsal', $save);
					if ($save == true) {
						echo json_encode(array("success" => TRUE, 'notif' => "Data Berhasil Di Simpan"));
					} else {
						echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
					}
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_futsal', 'Nama Tempat', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'required');
		$this->form_validation->set_rules('no_rek', 'No Rekening', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}
}

/* End of file Futsal.php */
/* Location: ./application/controllers/Futsal.php */
