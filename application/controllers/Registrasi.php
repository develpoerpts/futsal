<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registrasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}
	public function index()
	{
		$data['title']     = 'Register';
		$data['sub_judul'] = '';
		$this->load->view('register', $data);
	}
	public function register()
	{

		$input = $this->input->post();
		// config upload surat ijin
		$config = array(
			'upload_path' => './assets/member',
			'allowed_types' => 'jpeg|jpg|png',
			'max_size' => '2048',
			'max_width' => '2000',
			'max_height' => '2000',
			'file_name' => $this->input->post('nama_member')
		);
		$this->load->library('upload', $config);
		if (!empty($_FILES['foto_bukti_transfer_daftar']['name'])) {
			if (!$this->upload->do_upload('foto_bukti_transfer_daftar')) {
				$this->session->set_flashdata('error',  $this->upload->display_errors());
				redirect(site_url('Registrasi'));
			} else {
				$file = $this->upload->data();
				$member = array(
					'id_member' => $input['id_member'],
					'nama_member' 	=> $input['nama_member'],
					'alamat_member'			=> $input['alamat_member'],
					'email'		=> $input['email'],
					'status'		=> 'Non Aktif',
					'password'		=> md5($input['password']),
					'no_telephone'		=> $input['no_telephone'],
					'foto_bukti_transfer_daftar'			=> $file['file_name']
				);
				$insert = $this->Models->insert('tb_member', $member);
				if ($insert == TRUE) {
					$this->session->set_flashdata('success', 'Registerasi Berhasil');
					redirect(site_url('Welcome'));
				} else {
					$this->session->set_flashdata('error', 'Data gagal disimpan!');
					redirect(site_url('Registrasi'));
				}
			}
		}
	}
	public function check_email()
	{
		$email = $this->input->post('email');
		$check = $this->Models->get_dataId('tb_member', 'email', $email);
		if ($check->num_rows() > 0) {
			echo "false";
		} else {
			echo "true";
		}
	}
	public function Login_Member()
	{
		$data['title']     = 'Register';
		$data['sub_judul'] = '';

		$this->load->view('login_member', $data);
	}
	public function loginProgres()
	{
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$check = $this->Models->cekLogin($email, $password);
		if ($check->num_rows() > 0) {
			$row = $check->row();
			$sess['member_sess'] = array(
				'logged_member' 		=> true,
				'email_member' 		 => $row->email,
				'id_member'  			 => $row->id_member,
				'nama_member' 		 => $row->nama_member
			);
			$this->session->set_userdata($sess);
			redirect(base_url('Home'));
		} else {
			$this->session->set_flashdata('failed', 'Maaf Username atau Password salah');
			redirect(base_url('Login_member'));
		}
	}
	public function forget()
	{
		$data['title']     = 'Lupa Password';
		$data['sub_judul'] = '';

		$this->load->view('lupa_pass', $data);
	}
	public function logout()
	{
		$this->session->unset_userdata('member_sess');
		$this->session->unset_userdata('tamu');
		redirect('Home');
	}
}
