<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
		$this->load->helper('My_helper');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$isi['content']		= 'frontend/tampilan_content_frontend';
		$isi['data']		= $this->Models->get_lapangan();
		$this->load->view('frontend/tampilan_frontend', $isi);
	}
	public function carapemesanan()
	{
		$isi['content']		= 'frontend/tampilan_carapembayaran';
		$isi['data']		= $this->Models->get_lapangan();
		$this->load->view('frontend/tampilan_frontend', $isi);
	}
	public function send()
	{
		$email = 'sandytesar@gmail.com';
		$confEmail = array(
			'password' => 'QWasa',
			'subject' => 'ResetPassword',
		);
		$theme = 'mail/succes_mail';
		sendEmail($email, $theme, $confEmail);
	}
	public function cetak()
	{
		$this->load->library('pdf');
		$this->load->view('xample');
		$html = $this->output->get_output();
		$customPaper = array(0, 0, 10, 160);
		// $this->dompdf->set_paper($customPaper);
		// $this->dompdf->set_paper(array(0, 0, 600, 800));
		// $this->dompdf->set_paper('a10', 'landscape');
		$this->dompdf->load_html($html);
		$this->dompdf->render();

		$this->dompdf->stream("Pemberitahuan_kp.pdf", array('Attachment' => 0));
	}
}
