<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sektor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Sektor/main', 'header'=>'Sektor Usaha','table_header'=>'Data Sektor Usaha' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tbl_sektor');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_sektor;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Sektor/get_data/').'" onclick="edit('.$value->id_sektor.');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tbl_sektor','id_sektor',$id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id_sektor');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                    'nama_sektor' => $this->input->post('nama_sektor'),
                    );
                $save = $this->Models->update('tbl_sektor','id_sektor',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
                $save = array(
                    'nama_sektor' => $this->input->post('nama_sektor'),
                    );
                $save = $this->Models->insert('tbl_sektor',$save);
                if ($save==true) {
                    echo json_encode(array("success" => TRUE));
                } else {
                    echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
                }
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_sektor','Nama Sektor', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;			
		}
		echo json_encode($data);
	}

}

/* End of file Sektor.php */
/* Location: ./application/controllers/Sektor.php */