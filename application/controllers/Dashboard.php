<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
		if (empty($this->session->userdata('logged'))) {
			redirect(base_url('Login'), 'refresh');
		}
	}

	public function index()
	{
		$this->load->model('Chart_model', 'Chart');
		$data = array('content' => 'Dashboard', 'header' => 'Dashboard');
		$data['member']  = $this->Models->get_data('tb_member')->num_rows();
		$data['futsal']  = $this->Models->get_data('tb_futsal')->num_rows();
		$data['lapangan']  = $this->Models->get_data('tb_lapangan')->num_rows();
		$data['booking']  = $this->Models->get_data('tb_booking')->num_rows();
		$member = $this->Chart->memberBooking();
		if ($member != false) {
			$data['top_member'] = $member->result();
		}
		$this->load->view('theme', $data);
	}
	public function getChart()
	{
		$this->load->model('Chart_model', 'Chart');
		$label = $this->Chart->getLabel_lapangan();
		$data = array();
		if ($label->num_rows() > 0) {
			foreach ($label->result() as $key => $futsal) {
				$count = $this->Chart->count_jam($futsal->id_lapangan);
				if ($count != false) {
					$jam = $count->lama_booking;
				} else {
					$jam = 0;
				}
				$datas = array();
				$datas['lapangan'] = $futsal->nama_futsal . '-' . $futsal->jenis;
				$datas['nilai'] = (int) $jam;
				$data[] = $datas;
			}
		}
		echo json_encode(array('data' => $data));
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
