<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DetailLapangan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
		$this->load->helper('My_helper');
	}


	public function detail()
	{
		$isi['title']       = 'Detail Lapangan';
		$isi['content']     = 'frontend/detail_lapangan';
		// $isi['judul']       = 'home';
		// $isi['sub_judul']   = '';
		$id = $this->uri->segment(3);
		//	$this->db->where('id_lapangan',$key);
		$query = $this->Models->get_lapangan_Id_front($id);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$isi['id_lapangan']		= $row->id_lapangan;
				$isi['nama_futsal']		= $row->nama_futsal;
				$isi['harga_non_member']	= $row->harga_non_member;
				$isi['diskon_member']			= $row->diskon_member;
				$isi['foto_lapangan']			= $row->foto_lapangan;
				$isi['jenis']		= $row->jenis;
				$isi['alamat']			= $row->alamat;
				$isi['ukuran_lapangan']		= $row->ukuran_lapangan;
				$isi['info_lainnya']			= $row->info_lainnya;
				$isi['fasilitas']		= $row->fasilitas;
				$isi['keterangan']		= $row->keterangan;
				$isi['no_telepon']		= $row->no_telepon;
			}
		} else {
			$isi['id_lapangan']		= '';
			$isi['nama_futsal']		= '';
			$isi['harga_non_member']	= '';
			$isi['diskon_member']			= '';
			$isi['foto_lapangan']	= '';
			$isi['jenis']		= '';
			$isi['alamat']	= '';
			$isi['ukuran_lapangan']		= '';
			$isi['info_lainnya']	= '';
			$isi['fasilitas']	= '';
			$isi['keterangan']			= '';
			$isi['no_telepon']			= '';
		}

		$this->load->view('frontend/tampilan_frontend', $isi);
	}
	public function getInfobooking()
	{
		$this->load->model('App_models', 'Models');
		$dates = $this->input->post('tanggal');
		$hari = date('d', strtotime($dates));
		$bulan = date('m', strtotime($dates));
		$year = date('Y', strtotime($dates));
		$no = 1;
		$output = array();
		$get = $this->Models->get_booking_info($hari, $bulan, $year);
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->no_booking;
			$data[] = $value->tanggal_booking;
			$data[] = $value->jam_booking;
			$data[] = $value->selesai_booking;
			$data[] = $value->nama_futsal;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
}
