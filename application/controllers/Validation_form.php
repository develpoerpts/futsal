<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Validation_form extends CI_Controller
{
    public function nama()
    {
        $data = array('messages' => array());

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('nama_booking', 'Nama Booking ', 'trim|required|alpha|min_length[3]');
        if ($this->form_validation->run() == FALSE) {
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => true));
        }
    }
    public function no_hp()
    {
        $data = array('messages' => array());

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('no_hp_booking', 'No Telephone', 'trim|required|integer');
        if ($this->form_validation->run() == FALSE) {
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => true));
        }
    }
    public function email()
    {
        $data = array('messages' => array());

        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|min_length[6]');
        if ($this->form_validation->run() == FALSE) {
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
            echo json_encode($data);
        } else {
            echo json_encode(array('success' => true));
        }
    }
}

/* End of file Validation_form.php */
