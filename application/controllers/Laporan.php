<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App_models', 'Models');
    }

    public function index()
    {
        $futsal = $this->Models->get_data('tb_futsal')->result();
        $jenis_lapangan = $this->Models->get_data('tb_jenis_lapangan')->result();
        $data = array(
            'content' => 'Laporan/main',
            'header' => 'Laporan Penghasilan',
            'table_header' => 'Data Lapangan Futsal',
            'futsal' => $futsal,
            'jenis_lapangan' => $jenis_lapangan
        );
        $this->load->view('theme', $data);
    }
    public function getLapangan()
    {

        $id = $this->input->post('id');
        $get = $this->Models->get_dataId('tb_lapangan', 'id_futsal', $id);
        $futsal = $this->Models->get_dataId('tb_futsal', 'id_futsal', $id)->row();
        $data = array();
        if ($get->num_rows() > 0) {
            $data[] = array(
                'id_lapangan' => '_all',
                'nama' => 'Semua',
            );
            foreach ($get->result() as $key => $lapangan) {
                $jenis = $this->Models->get_dataId('tb_jenis_lapangan', 'id_jenis', $lapangan->id_jenis)->row();
                $data[] = array(
                    'id_lapangan' => $lapangan->id_lapangan,
                    'nama' => $futsal->nama_futsal . ' - ' . $jenis->jenis,
                );
            }
        } else {
            $data[] = array(
                'id_lapangan' => '',
                'nama' => 'Tidak Ada Data',
            );
        }

        echo json_encode($data);
    }
    public function cetak()
    {
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $id_futsal = $this->input->post('id_futsal');
        $id_lapangan = $this->input->post('id_lapangan');
        $this->load->library('pdf');
        $kode = time() . ".pdf";
        $isi['title'] = 'Laporan Data Closed Book';
        if (isset($_POST['cetak_all'])) {
            $this->input->post('cetak_all');
            $get = $this->Models->penghasilan_all($bulan, $tahun);
            $isi['data'] = $get;
        } else {
            $get_futsal = $this->Models->get_dataId('tb_futsal', 'id_futsal', $id_futsal)->row();
            $get = $this->Models->penghasilan($bulan, $tahun, $id_futsal, $id_lapangan);
            $isi['data'] = $get;
            $isi['futsal'] = array(
                'bulan' => date('F', strtotime('01-' . $bulan . '-' . $tahun)),
                'tahun' => $tahun,
                'futsal_name' => $get_futsal->nama_futsal,
            );
        }

        $this->load->view('Laporan/cetak_penghasilan', $isi);

        $paper_size  = 'A4';
        $orientation = 'portrait';
        $html = $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream($kode, array('Attachment' => 0));
    }
}

/* End of file Laporan.php */
