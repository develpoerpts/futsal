<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Perusahaan/main', 'header'=>'Perusahaan','table_header'=>'Data Perusahaan' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tbl_perusahaan');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_perusahaan;
			$data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}

}

/* End of file Perusahaan.php */
/* Location: ./application/controllers/Perusahaan.php */