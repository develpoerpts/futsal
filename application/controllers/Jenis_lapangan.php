<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_lapangan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Jenis_lapangan/main', 'header'=>'Jenis lapangan Usaha','table_header'=>'Data Jenis lapangan' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tb_jenis_lapangan');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->jenis;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Jenis_lapangan/get_data/').'" onclick="edit('.$value->id_jenis.');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>
			<a href="javascript:void(0)" class="btn btn-danger btn-xs" data-url="'.base_url('Jenis_lapangan/delData/').'" onclick="delet('.$value->id_jenis.');" id="delBtn"><i class="fa fa-trash"></i> Hapus</a>';
			$output[] = $data;

		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tb_jenis_lapangan','id_jenis',$id)->row();
		echo json_encode($data);
	}

	public function delData($id)
{
	$delete = $this->Models->deletData('tb_jenis_lapangan','id_jenis',$id);
	if ($delete) {
		echo json_encode( array('notif'=> "Data Berhasil Dihapus"));
	}
}

	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id_jenis');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                    'jenis' => $this->input->post('jenis'),
                    );
                $save = $this->Models->update('tb_jenis_lapangan','id_jenis',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
                $save = array(
                    'jenis' => $this->input->post('jenis'),
                    );
                $save = $this->Models->insert('tb_jenis_lapangan',$save);
                if ($save==true) {
                    echo json_encode(array("success" => TRUE));
                } else {
                    echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
                }
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('jenis','Nama Jenis lapangan', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Jenis_lapangan.php */
/* Location: ./application/controllers/Jenis_lapangan.php */
