<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turnamen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Turnamen/main', 'header'=>'Turnamen','table_header'=>'Kelola Turnamen' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		//$id=   $this->session->userdata('id_futsal');
		$output = array();
		$get = $this->Models->get_data_turnamen();
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_turnamen;
			$data[] = $value->tanggal_turnamen;
			$data[] = $value->file_pendaftaran;
			$data[] = $value->biaya_daftar;
			$data[] = $value->no_telephone;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="view_futsal" data-url="'.base_url('Turnamen/view_page/').''.$value->id_turnamen.'"><i class="fa fa-eye"></i> View</a>
			<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit_futsal" data-url="'.base_url('Turnamen/get_data/').''.$value->id_turnamen.'"><i class="fa fa-pencil"></i> Edit</a>
			<a href="javascript:void(0)" class="btn btn-danger btn-xs" data-url="'.base_url('Turnamen/delData/').'" onclick="delet('.$value->id_turnamen.');" id="delBtn"><i class="fa fa-trash"></i> Hapus</a>';

			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		// $data = $this->Models->get_dataId('tb_futsal','id_futsal',$id)->row();
		// echo json_encode($data);
		$data = $this->Models->get_dataId('tb_futsal', 'id_futsal',$id)->row();
		$futsal = array(

				'id_futsal' => $data->id_futsal,
				'nama_futsal'=> $data->nama_futsal,
				'alamat' => $data->alamat,
				'keterangan' => $data->keterangan,
				'fasilitas' => $data->fasilitas,
				'nama_bank' => $data->nama_bank,
				'no_rek' => $data->no_rek,
				'username' => $data->username

		 );
			echo json_encode($futsal);
	}
	public function view_page($id)
	{
			$data = $this->Models->get_dataId('tb_futsal', 'id_futsal',$id)->row();
			$futsal = array(

					'id_futsal' => $data->id_futsal,
					'nama_futsal'=> $data->nama_futsal,
					'alamat' => $data->alamat,
					'keterangan' => $data->keterangan,
					'fasilitas' => $data->fasilitas,
					'nama_bank' => $data->nama_bank,
					'no_rek' => $data->no_rek,
					'username' => $data->username

			 );
				echo json_encode($futsal);

	}

	public function delData($id)
{
	$delete = $this->Models->deletData('tb_turnamen','id_turnamen',$id);
	if ($delete) {
		echo json_encode( array('notif'=> "Data Berhasil Dihapus"));
	}
}

  public function save()
  {
    $valid = $this->validation_check();
        $id = $this->input->post('id_turnamen');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                   'nama_turnamen' => $this->input->post('nama_turnamen'),
                   'tanggal_turnamen' => $this->input->post('tanggal_turnamen'),
                   'id_futsal' => $this->session->userdata('id_futsal'),
                   'keterangan' => $this->input->post('keterangan'),
                   'biaya_daftar' => $this->input->post('biaya_daftar'),
                   'syarat' => $this->input->post('syarat'),
                   'file_pendaftaran' => $this->input->post('file_pendaftaran'),
                   'no_telephone' => $this->input->post('no_telephone'),
                   'status_turnamen' => 'Incomming',
          );
          if(!empty($_FILES['foto_turnamen']['name'])){
            $getData = $this->Models->get_dataId('tb_turnamen','id_turnamen',$id)->row();
            unlink(FCPATH.'assets/turnamen/'.$getData->foto_turnamen);
            $upload = $this->do_upload();
            $save['foto_turnamen'] = $upload['file_name'];
          }
                $save = $this->Models->update('tb_turnamen','id_turnamen',$id, $save);
        if ($save==true) {
          echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
        } else {
          echo json_encode( array('error_db' =>$this->db->error())) ;
        }
            }
        }else {
      if(!empty($_FILES['foto_turnamen']['name'])||!empty($_FILES['file_pendaftaran']['name']))
      {
        $upload = $this->do_upload();
        $uploadFile = $this->do_upload_file();
        if ($valid==TRUE &&  $upload==true && $uploadFile==true) {
            $save = array(
              'nama_turnamen' => $this->input->post('nama_turnamen'),
              'tanggal_turnamen' => $this->input->post('tanggal_turnamen'),
              'id_futsal' => $this->session->userdata('id_futsal'),
              'keterangan' => $this->input->post('keterangan'),
              'biaya_daftar' => $this->input->post('biaya_daftar'),
              'syarat' => $this->input->post('syarat'),
              'file_pendaftaran'=> $uploadFile['file_name_file'],
              'no_telephone' => $this->input->post('no_telephone'),
              'foto_turnamen'=> $upload['file_name'],
              'status_turnamen' => 'Incomming',
                );
            $save = $this->Models->insert('tb_turnamen',$save);
            if ($save==true) {
                echo json_encode(array("success" => TRUE,"notif" =>"Data Berhasil ditambahkan"));
            } else {
                echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
            }
        }
      }

        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_turnamen','Nama Tempat', 'required');
		$this->form_validation->set_rules('tanggal_turnamen','Alamat', 'required');
		$this->form_validation->set_rules('keterangan','Keterangan', 'required');
		$this->form_validation->set_rules('biaya_daftar','Nama Bank', 'required');
		$this->form_validation->set_rules('syarat','No Rekening', 'required');
		$this->form_validation->set_rules('no_telephone','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

  public function do_upload()
{
	$config['upload_path']          = './assets/turnamen';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 1000; //set max size allowed in Kilobyte
			$config['max_width']            = 1000; // set max width image allowed
			$config['max_height']           = 1000; // set max height allowed
			$config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

			$this->load->library('upload', $config);

	// 		if(!$this->upload->do_upload('foto_lapangan')) //upload and validate
	// 		{
	// 				$data['inputerror'][] = 'photo';
	// 				$data['messages'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
	// 				$data['status'] = FALSE;
	// 				echo json_encode($data);
	// 				exit();
	// 		}
	// return $this->upload->data();
	if (isset($_FILES['foto_turnamen']['name'])) {
            if (0 < $_FILES['foto_turnamen']['error']) {
                $msg =  'Error during file upload' . $_FILES['foto_turnamen']['error'];
								// echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
								$data['messages']['foto_turnamen'] = '<p class="text-danger">'.$msg.' </p>';
								echo json_encode($data);
            } else {
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('foto_turnamen')) {
												$msg = $this->upload->display_errors();
													$data['messages']['foto_turnamen'] = '<p class="text-danger">'.$msg.' </p>';
								echo json_encode($data);
                        // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
                    } else {
                        return $this->upload->data();
                    }
            }
        } else {
            $data['messages']['foto_turnamen'] = '<p class="text-danger">pilih gambar </p>';
            echo json_encode($data);
        }
}


public function do_upload_file()
{
$config['upload_path']          = './assets/file_turnamen';
    $config['allowed_types']        = 'pdf|doc|docx';
    $config['file_name_file']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

    $this->load->library('upload', $config);

if (isset($_FILES['file_pendaftaran']['name'])) {
          if (0 < $_FILES['file_pendaftaran']['error']) {
              $msg =  'Error during file upload' . $_FILES['file_pendaftaran']['error'];
              // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
              $data['messages']['file_pendaftaran'] = '<p class="text-danger">'.$msg.' </p>';
              echo json_encode($data);
          } else {
                  $this->load->library('upload', $config);
                  if (!$this->upload->do_upload('file_pendaftaran')) {
                      $msg = $this->upload->display_errors();
                        $data['messages']['file_pendaftaran'] = '<p class="text-danger">'.$msg.' </p>';
              echo json_encode($data);
                      // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
                  } else {
                      return $this->upload->data();
                  }
          }
      } else {
          $data['messages']['file_pendaftaran'] = '<p class="text-danger">pilih file </p>';
          echo json_encode($data);
      }
}


}

/* End of file Futsal.php */
/* Location: ./application/controllers/Futsal.php */
