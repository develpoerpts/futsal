<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	//	$this->load->model('frontend/Model_login_user');
	//	$this->load->helper('My_helper');
	$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$isi['content']		= 'frontend/tampilan_content_frontend';
		$isi['data']		= $this->db->get('tb_lapangan');
		$this->load->view('frontend/tampilan_frontend', $isi);
	}
	public function carapemesanan()
	{
		$isi['content']		= 'frontend/tampilan_carapembayaran';
		// $isi['data']		= $this->db->get('barang');
		$this->load->view('frontend/tampilan_frontend', $isi);
	}

	public function profil_cv()
	{
		$isi['content']		= 'frontend/tampilan_profil_cv';
		// $isi['data']		= $this->db->get('barang');
		$this->load->view('frontend/tampilan_frontend', $isi);
	}

	public function kontak()
	{
		$isi['content']		= 'frontend/tampilan_kontak';
		// $isi['data']		= $this->db->get('barang');
		$this->load->view('frontend/tampilan_frontend', $isi);
	}

	public function getlogin_user()
	{
		$u 	= $this->input->post('email');
		$p 	= $this->input->post('password');
		$this->load->model('frontend/Model_login_user');
		$this->Model_login_user->getlogin_user($u,$p);
	}

	public function daftar()
	{
		$this->load->view('frontend/tampilan_register_user');
	}

	public function register() 	{


			// $data =  array('nama'		 => $this->input->post('nama'),
			// 			   'email'		 => $this->input->post('email'),
			// 			   'password'	 => md5($this->input->post('password')),
			// 			   	'no_hp'		 => $this->input->post('no_hp'),
			// 			   	'alamat'		 => $this->input->post('alamat'));
			// $this->Model_login_user->getinsert($data);
			// redirect('frontend/C_login_user');
		$nama = $this->input->post('nama');
		$email= $this->input->post('email');
		$password	 = md5($this->input->post('password'));
						   	$no_hp		 = $this->input->post('no_hp');
						   	$alamat		 = $this->input->post('alamat');
						   	$smt = $this->db->where('email',$email)->get('user');
						   	if ($smt->num_rows()>0) {
						   		$this->session->set_flashdata("error", 'Maaf email sudah ada!');
						   		redirect('C_home_frontend/daftar');
						   	}else{
						   		$data= array('nama' => $nama , 'email' => $email, 'password' => $password, 'no_hp' => $no_hp, 'alamat' => $alamat);
						   		$smt2 = $this->db->insert('user',$data);
						   		if ($smt2) {
						   			redirect('frontend/C_login_user');
						   		}
						   	}


	}

}
