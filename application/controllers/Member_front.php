<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Member_front extends CI_Controller
{

    public function index()
    {

        $isi['title']       = 'Histori Booking Lapangan';
        $isi['content']     = 'frontend/booking_member';
        $isi['content_member'] = 'frontend/member/histori';
        $this->load->view('frontend/tampilan_frontend', $isi);
    }
    public function getInfobooking()
    {
        $this->load->model('App_models', 'Models');
        $no = 1;
        $output = array();
        if (isset($this->session->userdata('tamu')['email'])) {
            $get =  $this->Models->get_booking_tamu();
        } else {
            $get = $this->Models->get_booking_member();
        }

        foreach ($get->result() as $key => $value) {
            $button = '';
            if (($value->status_booking == 'Pending') and ($value->payment_stat != 3)) {
                $button .= '<a href="' . base_url('Riwayat_Booking/bayar/' . $value->no_booking) . '" class="btn btn-warning"> Bayar Sekarang</a>';
            }
            if ($value->status_booking == 'Booked') {
                $button .= '<a href="" class="btn btn-success"> Detail</a>';
            }
            if ($value->payment_stat == 3) {
                $status = '<span class="badge bg-danger"> Expired</span>';
            } else {
                switch ($value->status_booking) {
                    case 'Pending':
                        $status = '<span class="badge bg-danger"> Belum Bayar</span>';
                        break;
                    case 'On Process':
                        $status = '<span class="badge bg-warning"> Menunggu Konfirmasi</span>';
                        break;
                    case 'Closed':
                        $status = '<span class="badge bg-success"> Selesai</span>';
                        break;
                    case 'Expired':
                        $status = '<span class="badge bg-warning"> Expired</span>';
                        break;
                    default:
                        $status = '';
                        break;
                }
            }


            $data = array();
            $data[] = $no++;
            $data[] = $value->no_booking;
            $data[] = $value->nama_futsal . '-' . $value->jenis;
            $data[] = $value->tanggal_booking;
            $data[] = $value->jam_booking;
            $data[] = $value->selesai_booking;
            $data[] = $status;
            $data[] = $button;
            $output[] = $data;
        }
        echo json_encode(array('data' => $output));
    }
    public function Tamu()
    {
        refresPayment();
        $isi['title']       = 'Histori Booking Lapangan';
        $isi['content']     = 'frontend/booking_member';
        $isi['content_member'] = 'frontend/member/histori';
        $this->load->view('frontend/tampilan_frontend', $isi);
    }
}

/* End of file Member_front.php */
