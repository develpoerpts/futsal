<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pencarian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('My_helper');
		$this->load->model('Model_cari');
		$this->load->model('App_models', 'Models');
	}

	function cari()
    {
		$cari = $this->input->post('cari');

		$isi['content']     = 'frontend/hasil_pencarian';
	  $keyword = $this->input->get('cari', TRUE); //mengambil nilai dari form input cari
		$isi['tempat']		= $this->Models->get_lapanganCari($cari);
		$isi['data']		= $this->Models->get_lapangan();
	//	$isi['barang'] = $this->Model_cari->cari($cari); //mencari data karyawan berdasarkan inputan
		$this->load->view('frontend/tampilan_frontend', $isi); //menampilkan data yang sudah dicari
    }

    function cari_barang()
    {
    	$cari = $this->input->post('cari');

		$isi['content']     = 'frontend/user/hasil_pencarian';
	    $keyword = $this->input->get('cari', TRUE); //mengambil nilai dari form input cari
		$isi['barang'] = $this->Model_cari->cari($cari); //mencari data karyawan berdasarkan inputan
		$this->load->view('frontend/user/tampilan_home_user', $isi); //menampilkan data yang sudah dicari
    }

}
