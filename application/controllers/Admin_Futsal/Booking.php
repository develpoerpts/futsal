<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$lapangan = $this->Models->get_data('tb_lapangan')->result();
		$jenis_lapangan = $this->Models->get_data('tb_jenis_lapangan')->result();
		//		$futsal = $this->Models->get_data('tb_futsal')->result();
		$data = array('content' => 'Admin_Futsal/Booking/main', 'header' => 'Booking', 'table_header' => 'Data Booking');
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$no = 1;
		$output = array();
		$get = $this->Models->get_booking_Id_futsal('id_futsal', $this->session->userdata('id_futsal'), $start, $end);
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->no_booking;
			$data[] = $value->nama_booking;
			$data[] = $value->tanggal_booking;
			$data[] = $value->jam_booking;
			$data[] = $value->status_member;
			$data[] = $value->jenis;
			//		$data[] = $value->nama_futsal;
			$data[] = $value->total_harga;
			$data[] = $value->status_booking;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="edit" data-url="' . base_url('Admin_Futsal/Booking/get_data/') . '' . $value->id_booking . '"><i class="fa fa-eye"></i> View</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataBookingbyId('id_booking', $id)->row();
		$booking = array(
			'id_booking' => $data->id_booking,
			'no_booking' => $data->no_booking,
			'nama_booking' => $data->nama_booking,
			'tanggal_booking' => $data->tanggal_booking,
			'jam_booking' => $data->jam_booking,
			'no_hp_booking' => $data->no_hp_booking,
			'status_member' => $data->status_member,
			'jenis' => $data->jenis,
			'total_harga' => $data->total_harga,
			'status_booking' => $data->status_booking,
			'foto' => base_url('assets/bookingTransfer/' . $data->foto_transfer)
		);
		echo json_encode($booking);
	}
	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_booking');
		$metode = $this->input->post('metode');
		if ($id != '') {
			if ($valid == TRUE) {
				$save = array(
					// 'no_booking' => $this->input->post('no_booking'),
					// 'nama_booking' => $this->input->post('nama_booking'),
					// 'tanggal_booking' => $this->input->post('tanggal_booking'),
					// 'jam_booking' => $this->input->post('jam_booking'),
					// 'no_hp_booking' => $this->input->post('no_hp_booking'),
					// 'status_member' => $this->input->post('status_member'),
					// 'id_lapangan' => $this->input->post('id_lapangan'),
					// 'total_harga' => $this->input->post('total_harga'),
					'status_booking' => $this->input->post('status_booking'),
					// 'foto_tr' => $this->input->post('password'),
				);
				$getData = $this->Models->get_dataId('tb_booking', 'id_booking', $id)->row();
				$save = $this->Models->update('tb_booking', 'id_booking', $id, $save);
				if ($save == true) {
					if ($this->input->post('status_booking') == 'Booked') {
						$send = array('subject' => 'Ferivikasi Booking', 'data' => $getData);
						$theme = 'mail/succes_mail';
						sendEmail($getData->email, $theme, $send);
					}
					echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode(array('error_db' => $this->db->error()));
				}
			}
		} else {
			if ($valid == TRUE) {
				$save = array(
					'no_booking' => $this->input->post('no_booking'),
					'nama_booking' => $this->input->post('nama_booking'),
					'tanggal_booking' => $this->input->post('tanggal_booking'),
					'jam_booking' => $this->input->post('jam_booking'),
					'no_hp_booking' => $this->input->post('no_hp_booking'),
					'status_member' => $this->input->post('status_member'),
					'id_lapangan' => $this->input->post('id_lapangan'),
					'total_harga' => $this->input->post('total_harga'),
					'status_booking' => $this->input->post('status_booking'),
					// 'foto_tr' => $this->input->post('password'),
				);
				$save = $this->Models->insert('tb_booking', $save);
				if ($save == true) {
					echo json_encode(array("success" => TRUE));
				} else {
					echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('status_booking', 'Status Pemesanan', 'required');
		// $this->form_validation->set_rules('alamat','Alamat', 'required');
		// $this->form_validation->set_rules('keterangan','Keterangan', 'required');
		// $this->form_validation->set_rules('nama_bank','Nama Bank', 'required');
		// $this->form_validation->set_rules('no_rek','No Rekening', 'required');
		// $this->form_validation->set_rules('username','Username', 'required');
		// $this->form_validation->set_rules('password','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}
}

/* End of file Booking.php */
/* Location: ./application/controllers/Booking.php */
