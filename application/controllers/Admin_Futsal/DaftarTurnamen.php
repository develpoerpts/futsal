<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarTurnamen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Admin_Futsal/DaftarTurnamen/main', 'header'=>'Pendaftaran Turnamen','table_header'=>'Pendaftaran Turnamen' );
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		//$id=   $this->session->userdata('id_futsal');
		$output = array();
		$get = $this->Models->get_dataIdTurnamen('id_futsal',$this->session->userdata('id_futsal'));
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_pendaftar;
			$data[] = $value->no_telephone;
			$data[] = $value->file_formulir;
			$data[] = $value->nama_turnamen;
			$data[] = $value->kode_daftar;
			$data[] = $value->status_daftar;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="view_daftar" data-url="'.base_url('Admin_Futsal/DaftarTurnamen/view_page/').''.$value->id_daftar.'"><i class="fa fa-eye"></i> View</a>
			<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit_daftar" data-url="'.base_url('Admin_Futsal/DaftarTurnamen/get_data/').''.$value->id_daftar.'"><i class="fa fa-pencil"></i> Edit</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		// $data = $this->Models->get_dataId('tb_futsal','id_futsal',$id)->row();
		// echo json_encode($data);
		$data = $this->Models->get_dataIdTurnamen('id_daftar',$id)->row();
		$daftarTurnamen = array(

				'id_daftar' => $data->id_daftar,
				'nama_pendaftar'=> $data->nama_pendaftar,
				'no_telephone' => $data->no_telephone,
				'file_formulir' => $data->file_formulir,
				'nama_turnamen' => $data->nama_turnamen,
				'kode_daftar' => $data->kode_daftar,
				'status_daftar' => $data->status_daftar,
				'foto_bukti_transfer' => base_url('assets/bukti/').''.$data->foto_bukti_transfer

		 );
			echo json_encode($daftarTurnamen);
	}
	public function view_page($id)
	{
    $data = $this->Models->get_dataIdTurnamen('id_daftar',$id)->row();
    $daftarTurnamen = array(

        'id_daftar' => $data->id_daftar,
        'nama_pendaftar'=> $data->nama_pendaftar,
        'no_telephone' => $data->no_telephone,
        'file_formulir' => $data->file_formulir,
        'nama_turnamen' => $data->nama_turnamen,
        'kode_daftar' => $data->kode_daftar,
        'foto_bukti_transfer' => base_url('assets/bukti/').''.$data->foto_bukti_transfer

     );
      echo json_encode($daftarTurnamen);

	}

	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id_daftar');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(

										'status_daftar' => $this->input->post('status_daftar'),
										// 'keterangan' => $this->input->post('keterangan'),
										// 'fasilitas' => $this->input->post('fasilitas'),
										// 'nama_bank' => $this->input->post('nama_bank'),
										// 'no_rek' => $this->input->post('no_rek'),
										// 'username' => $this->input->post('username'),

                    );
                $save = $this->Models->update('tb_daftar_turnamen','id_daftar',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
							$save = array(
								 'nama_futsal' => $this->input->post('nama_futsal'),
								 'alamat' => $this->input->post('alamat'),
								 'keterangan' => $this->input->post('keterangan'),
								 'fasilitas' => $this->input->post('fasilitas'),
								 'nama_bank' => $this->input->post('nama_bank'),
								 'no_rek' => $this->input->post('no_rek'),
								 'username' => $this->input->post('username'),

								 );
                $save = $this->Models->insert('tb_daftar_turnamen',$save);
                if ($save==true) {
                    echo json_encode(array("success" => TRUE));
                } else {
                    echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
                }
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('status_daftar','Status Pendaftaran', 'required');

		// $this->form_validation->set_rules('password','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Futsal.php */
/* Location: ./application/controllers/Futsal.php */
