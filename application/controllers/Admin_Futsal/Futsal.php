<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Futsal extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Admin_Futsal/Futsal/main', 'header' => 'Futsal', 'table_header' => 'Kelola Futsal');
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		//$id=   $this->session->userdata('id_futsal');
		$output = array();
		$get = $this->Models->get_dataId('tb_futsal', 'id_futsal', $this->session->userdata('id_futsal'));
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_futsal;
			$data[] = $value->alamat;
			$data[] = $value->keterangan;
			$data[] = $value->nama_bank;
			$data[] = $value->no_rek;
			$data[] = $value->no_telepon;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="view_futsal" data-url="' . base_url('Admin_Futsal/Futsal/view_page/') . '' . $value->id_futsal . '"><i class="fa fa-eye"></i> View</a>
			<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit_futsal" data-url="' . base_url('Admin_Futsal/Futsal/get_data/') . '' . $value->id_futsal . '"><i class="fa fa-pencil"></i> Edit</a>
			<a href="' . base_url('Admin_Futsal/Futsal/stampel/') . '" class="btn btn-default btn-xs"> Stemel</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		// $data = $this->Models->get_dataId('tb_futsal','id_futsal',$id)->row();
		// echo json_encode($data);
		$data = $this->Models->get_dataId('tb_futsal', 'id_futsal', $id)->row();
		$futsal = array(

			'id_futsal' => $data->id_futsal,
			'nama_futsal' => $data->nama_futsal,
			'alamat' => $data->alamat,
			'keterangan' => $data->keterangan,
			'fasilitas' => $data->fasilitas,
			'nama_bank' => $data->nama_bank,
			'no_rek' => $data->no_rek,
			'username' => $data->username,
			'username' => $data->no_telepon,
		);
		echo json_encode($futsal);
	}
	public function view_page($id)
	{
		$data = $this->Models->get_dataId('tb_futsal', 'id_futsal', $id)->row();
		$futsal = array(

			'id_futsal' => $data->id_futsal,
			'nama_futsal' => $data->nama_futsal,
			'alamat' => $data->alamat,
			'keterangan' => $data->keterangan,
			'fasilitas' => $data->fasilitas,
			'nama_bank' => $data->nama_bank,
			'no_rek' => $data->no_rek,
			'username' => $data->username,
			'username' => $data->no_telepon,

		);
		echo json_encode($futsal);
	}

	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_futsal');
		$metode = $this->input->post('metode');
		if ($id != '') {
			if ($valid == TRUE) {
				$save = array(
					'nama_futsal' => $this->input->post('nama_futsal'),
					'alamat' => $this->input->post('alamat'),
					'keterangan' => $this->input->post('keterangan'),
					'fasilitas' => $this->input->post('fasilitas'),
					'nama_bank' => $this->input->post('nama_bank'),
					'no_rek' => $this->input->post('no_rek'),
					'username' => $this->input->post('username'),
					'no_telepon' => $this->input->post('no_telepon'),

				);
				$save = $this->Models->update('tb_futsal', 'id_futsal', $id, $save);
				if ($save == true) {
					echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode(array('error_db' => $this->db->error()));
				}
			}
		} else {
			if ($valid == TRUE) {
				$save = array(
					'nama_futsal' => $this->input->post('nama_futsal'),
					'alamat' => $this->input->post('alamat'),
					'keterangan' => $this->input->post('keterangan'),
					'fasilitas' => $this->input->post('fasilitas'),
					'nama_bank' => $this->input->post('nama_bank'),
					'no_rek' => $this->input->post('no_rek'),
					'username' => $this->input->post('username'),
					'no_telepon' => $this->input->post('no_telepon'),

				);
				$save = $this->Models->insert('tb_futsal', $save);
				if ($save == true) {
					echo json_encode(array("success" => TRUE));
				} else {
					echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_futsal', 'Nama Tempat', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'required');
		$this->form_validation->set_rules('no_rek', 'No Rekening', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('no_telepon', 'No Telepon', 'required');
		// $this->form_validation->set_rules('password','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}
	public function stampel()
	{
		$id = $this->session->userdata('id_futsal');
		$data = array(
			'content' => 'Admin_Futsal/Stempel/form',
			'header' => 'Futsal',
			'table_header' => 'Stempel Invoice'
		);
		$get = $this->Models->get_dataId('tb_stemple', 'id_futsal', $id);
		if ($get->num_rows() > 0) {
			$gets = $get->row();
			$data['stampel'] = $gets->stampel;
		}
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function save_stample()
	{
		// config upload surat ijin
		$id = $this->session->userdata('id_futsal');
		$config = array(
			'upload_path' => './assets/stample',
			'allowed_types' => 'png',
			'max_size' => '1048',
			// 'max_width' => '2000',
			// 'max_height' => '2000',
			'overwrite' => true,
			'file_name' => 'stample' . $id,
		);
		$this->load->library('upload', $config);
		if (!empty($_FILES['stample']['name'])) {
			if (!$this->upload->do_upload('stample')) {
				$this->session->set_flashdata('error',  $this->upload->display_errors());
				redirect(site_url('Admin_futsal/Futsal/stampel'));
			} else {
				$file = $this->upload->data();
				$data = array(
					'id_futsal' => $id,
					'stampel' => $file['file_name']
				);
				$get = $this->Models->get_dataId('tb_stemple', 'id_futsal', $id);
				if ($get->num_rows() > 0) {
					$insert = $this->Models->update_table('tb_stemple', 'id_futsal', $id, $data);
				} else {
					$insert = $this->Models->insert('tb_stemple', $data);
				}

				if ($insert == TRUE) {
					$this->session->set_flashdata('success', 'Berhasil');
					redirect(site_url('Admin_futsal/Futsal'));
				} else {
					$this->session->set_flashdata('error', 'Data gagal disimpan!');
					redirect(site_url('Registrasi'));
				}
			}
		}
	}
	public function getNotif()
	{
		$notif = notifBooking();
		if ($notif !== false) {
			$data = array();
			$data['count'] = $notif->num_rows();
			$data['data'] = $notif->result();
			foreach ($notif->result() as $key => $notif) {
				$futsal = $this->Models->get_dataId('tb_futsal', 'id_futsal', $notif->id_futsal)->row();
				// $data['message'] = array('val' => $futsal->nama_futsal . ', ');
				$val = array('<li class="nav-item">
				 	<a class="dropdown-item">
				 	<span>
				 	  <a href="' . base_url('Admin_Futsal/Booking') . '"><span> ' . $data['count'] . ' Booking Masuk</span> </a>
				 	</span>
				 	<span class="message">
	
				 	</span>
				   </a>
				</li>');
				$data['message'] = $val;
			}

			echo json_encode($data);
		}
	}
}

/* End of file Futsal.php */
/* Location: ./application/controllers/Futsal.php */
