<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
		if (empty($this->session->userdata('logged'))) {
			redirect(base_url('Login'), 'refresh');
		}
	}

	public function index()
	{
		$data = array('content' => 'Admin_Futsal/Dashboard', 'header' => 'Dashboard');
		$data['lapangan']  = $this->Models->get_dataId('tb_lapangan', 'id_futsal', ($this->session->userdata('id_futsal')))->num_rows();
		$data['report']  = $this->Models->get_booking_report_Id_futsal_ds('id_futsal', $this->session->userdata('id_futsal'))->num_rows();
		$data['member']  = $this->Models->get_data('tb_member')->num_rows();
		$data['booking']  = $this->Models->get_booking_Id_futsal_ds('id_futsal', $this->session->userdata('id_futsal'))->num_rows();
		$this->load->view('Admin_Futsal/theme', $data);
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
