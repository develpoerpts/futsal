<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Admin_Futsal/Member/main', 'header'=>'Member','table_header'=>'Data Member' );
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tb_member');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_member;
			$data[] = $value->alamat_member;
			$data[] = $value->no_telephone;
			// $data[] = $value->foto_member;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Admin_Futsal/Member/get_data/').'" onclick="edit('.$value->id_member.');" id="editBtn"><i class="fa fa-eye"></i> Detail</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tb_member','id_member',$id)->row();
		echo json_encode($data);
	}
	public function delData($id)
{
	$delete = $this->Models->deletData('tb_member','id_member',$id);
	if ($delete) {
		echo json_encode( array('notif'=> "Data Berhasil Dihapus"));
	}
}


	public function save()
	{
		$valid = $this->validation_check();
        $id = $this->input->post('id_Member');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                    'nama_Member' => $this->input->post('nama_Member'),
										'alamat' => $this->input->post('alamat'),
										'keterangan' => $this->input->post('keterangan'),
										'nama_bank' => $this->input->post('nama_bank'),
										'no_rek' => $this->input->post('no_rek'),
										'username' => $this->input->post('username'),
										'password' => $this->input->post('password'),
                    );
                $save = $this->Models->update('tb_Member','id_Member',$id, $save);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
            }
        }else {
            if ($valid==TRUE) {
							$save = array(
								 'nama_Member' => $this->input->post('nama_Member'),
								 'alamat' => $this->input->post('alamat'),
								 'keterangan' => $this->input->post('keterangan'),
								 'nama_bank' => $this->input->post('nama_bank'),
								 'no_rek' => $this->input->post('no_rek'),
								 'username' => $this->input->post('username'),
								 'password' => $this->input->post('password'),
								 );
                $save = $this->Models->insert('tb_Member',$save);
                if ($save==true) {
                    echo json_encode(array("success" => TRUE));
                } else {
                    echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
                }
            }
        }
    }

    function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_Member','Nama Tempat', 'required');
		$this->form_validation->set_rules('alamat','Alamat', 'required');
		$this->form_validation->set_rules('keterangan','Keterangan', 'required');
		$this->form_validation->set_rules('nama_bank','Nama Bank', 'required');
		$this->form_validation->set_rules('no_rek','No Rekening', 'required');
		$this->form_validation->set_rules('username','Username', 'required');
		$this->form_validation->set_rules('password','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */
