<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
		// $this->load->library('dompdf_gen');
	}

	public function index()
	{
		$lapangan = $this->Models->get_data('tb_lapangan')->result();
		$jenis_lapangan = $this->Models->get_data('tb_jenis_lapangan')->result();
		//		$futsal = $this->Models->get_data('tb_futsal')->result();
		$data = array('content' => 'Admin_Futsal/Report/main', 'header' => 'Report', 'table_header' => 'Data Report');
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$no = 1;
		$output = array();
		$get = $this->Models->get_booking_report_Id_futsal('id_futsal', $this->session->userdata('id_futsal'), $start, $end);
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->no_booking;
			$data[] = $value->nama_booking;
			$data[] = $value->tanggal_booking;
			$data[] = $value->jam_booking;
			$data[] = $value->lama_booking;
			$data[] = $value->status_member;
			$data[] = $value->jenis;
			//		$data[] = $value->nama_futsal;
			$data[] = $value->total_harga;
			$data[] = $value->status_booking;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="' . base_url('Admin_Futsal/Report/get_data/') . '" onclick="edit(' . $value->id_booking . ');" id="editBtn"><i class="fa fa-eye"></i> Detail</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataIdBooking_Id_futsal('id_booking', $id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_booking');
		$metode = $this->input->post('metode');
		if ($id != '') {
			if ($valid == TRUE) {
				$save = array(
					// 'no_Report' => $this->input->post('no_Report'),
					// 'nama_Report' => $this->input->post('nama_Report'),
					// 'tanggal_Report' => $this->input->post('tanggal_Report'),
					// 'jam_Report' => $this->input->post('jam_Report'),
					// 'no_hp_Report' => $this->input->post('no_hp_Report'),
					// 'status_member' => $this->input->post('status_member'),
					// 'id_lapangan' => $this->input->post('id_lapangan'),
					// 'total_harga' => $this->input->post('total_harga'),
					'status_booking' => $this->input->post('status_booking'),
					// 'foto_tr' => $this->input->post('password'),
				);
				$save = $this->Models->update('tb_booking', 'id_booking', $id, $save);
				if ($save == true) {
					echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode(array('error_db' => $this->db->error()));
				}
			}
		} else {
			if ($valid == TRUE) {
				$save = array(
					// 	'no_Report' => $this->input->post('no_Report'),
					// 	'nama_Report' => $this->input->post('nama_Report'),
					// 	'tanggal_Report' => $this->input->post('tanggal_Report'),
					// 	'jam_Report' => $this->input->post('jam_Report'),
					// 	'no_hp_Report' => $this->input->post('no_hp_Report'),
					// 	'status_member' => $this->input->post('status_member'),
					// 	'id_lapangan' => $this->input->post('id_lapangan'),
					// 	'total_harga' => $this->input->post('total_harga'),
					'status_Report' => $this->input->post('status_Report'),
					// 'foto_tr' => $this->input->post('password'),
				);
				$save = $this->Models->insert('tb_booking', $save);
				if ($save == true) {
					echo json_encode(array("success" => TRUE));
				} else {
					echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('status_booking', 'Status Pemesanan', 'required');
		// $this->form_validation->set_rules('alamat','Alamat', 'required');
		// $this->form_validation->set_rules('keterangan','Keterangan', 'required');
		// $this->form_validation->set_rules('nama_bank','Nama Bank', 'required');
		// $this->form_validation->set_rules('no_rek','No Rekening', 'required');
		// $this->form_validation->set_rules('username','Username', 'required');
		// $this->form_validation->set_rules('password','Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}


	public function cetak()
	{

		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$this->load->library('pdf');

		$kode = time() . ".pdf";
		$isi['title'] = 'Laporan Data Closed Book';

		$isi['data']  = $this->Models->get_booking_report_Id_futsal('id_futsal', $this->session->userdata('id_futsal'), $start, $end);

		$this->load->view('Admin_Futsal/cetak_laporan', $isi);

		$paper_size  = 'A4';
		$orientation = 'portrait';
		$html = $this->output->get_output();


		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream($kode, array('Attachment' => 0));
	}
}

/* End of file Report.php */
/* Location: ./application/controllers/Report.php */
