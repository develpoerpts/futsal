<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RecyclerLapangan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('App_models', 'Models');
    }

    public function index()
    {
        //$futsal = $this->Models->get_data('tb_futsal')->result();
        $jenis_lapangan = $this->Models->get_data('tb_jenis_lapangan')->result();
        $data = array('content' => 'Admin_Futsal/RecyclerLapangan/main', 'header' => 'Recycler Lapanagan', 'table_header' => 'Data Recycler Lapangan Futsal', 'jenis_lapangan' => $jenis_lapangan);
        $this->load->view('Admin_Futsal/theme', $data);
    }
    public function get_tables()
    {
        $no = 1;
        $output = array();
        $val = $this->session->userdata('id_futsal');
        $get = $this->Models->get_lapangan_Id_rl($val);
        foreach ($get->result() as $key => $value) {
            $data = array();
            $data[] = $no++;
            $data[] = $value->jenis;
            $data[] = $value->harga_non_member;
            $data[] = $value->diskon_member;
            $data[] = $value->ukuran_lapangan;
            $data[] = '<a href="javascript:void(0)" class="btn btn-success btn-xs" data-url="' . base_url('Admin_Futsal/RecyclerLapangan/restoredata/') . '" onclick="restore(' . $value->id_lapangan . ');" id="restore_lapangan"><i class="fa fa-pencil"></i> Restore</a>
			<a href="javascript:void(0)" class="btn btn-danger btn-xs" data-url="' . base_url('Admin_Futsal/RecyclerLapangan/delData/') . '" onclick="delet(' . $value->id_lapangan . ');" id="delBtn"><i class="fa fa-trash"></i> Hapus Permanen</a>';
            // $data[] = $value->nama_ketua;
            $output[] = $data;
        }
        echo json_encode(array('data' => $output));
    }
    public function get_lap()
    {
        $get = $this->Models->get_data('tb_jenis_lapangan');
        foreach ($get->result() as $key => $value) {
            $data = array();
            $data['id'] = $value->id_jenis;
            $data['text'] = $value->jenis;

            // $data[] = $value->nama_ketua;
            $output[] = $data;
        }
        echo json_encode($output);
    }

    public function view_page($id)
    {
        $data = $this->Models->get_dataId('tb_lapangan', 'id_lapangan', $id)->row();
        $lapangan = array(

            'id_lapangan' => $data->id_lapangan,
            'id_futsal' => $data->id_futsal,
            'id_jenis' => $data->id_jenis,
            'harga_non_member' => $data->harga_non_member,
            'diskon_member' => $data->diskon_member,
            'ukuran_lapangan' => $data->ukuran_lapangan,
            'info_lainnya' => $data->info_lainnya,
            'foto' => base_url('assets/lapangan/') . '' . $data->foto_lapangan
        );
        echo json_encode($lapangan);
    }

    public function get_data($id)
    {
        $data = $this->Models->get_dataId('tb_lapangan', 'id_lapangan', $id)->row();
        $lapangan = array(
            'id_lapangan' => $data->id_lapangan,
            'id_futsal' => $data->id_futsal,
            'id_jenis' => $data->id_jenis,
            'harga_non_member' => $data->harga_non_member,
            'diskon_member' => $data->diskon_member,
            'ukuran_lapangan' => $data->ukuran_lapangan,
            'info_lainnya' => $data->info_lainnya,
            'foto' => base_url('assets/lapangan/') . '' . $data->foto_lapangan
        );
        echo json_encode($lapangan);
    }
    public function save()
    {
        $valid = $this->validation_check();
        $id = $this->input->post('id_lapangan');
        $metode = $this->input->post('metode');
        if ($id != '') {
            if ($valid == TRUE) {
                $save = array(
                    'id_futsal' => $this->session->userdata('id_futsal'),
                    'id_jenis' => $this->input->post('id_jenis'),
                    'harga_non_member' => $this->input->post('harga_non_member'),
                    'ukuran_lapangan' => $this->input->post('ukuran_lapangan'),
                    'info_lainnya' => $this->input->post('info_lainnya'),
                    // 'foto_lapangan'=> $upload,
                    'diskon_member' => $this->input->post('diskon_member'),
                );
                if (!empty($_FILES['foto_lapangan']['name'])) {
                    $getData = $this->Models->get_dataId('tb_lapangan', 'id_lapangan', $id)->row();
                    unlink(FCPATH . 'assets/lapangan/' . $getData->foto_lapangan);
                    $upload = $this->do_upload();
                    $save['foto_lapangan'] = $upload['file_name'];
                }
                $save = $this->Models->update('tb_lapangan', 'id_lapangan', $id, $save);
                if ($save == true) {
                    echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
                } else {
                    echo json_encode(array('error_db' => $this->db->error()));
                }
            }
        } else {
            if (!empty($_FILES['foto_lapangan']['name'])) {
                $upload = $this->do_upload();
                if ($valid == TRUE &&  $upload == true) {
                    $save = array(
                        'id_futsal' => $this->session->userdata('id_futsal'),
                        'id_jenis' => $this->input->post('id_jenis'),
                        'harga_non_member' => $this->input->post('harga_non_member'),
                        'ukuran_lapangan' => $this->input->post('ukuran_lapangan'),
                        'info_lainnya' => $this->input->post('info_lainnya'),
                        'foto_lapangan' => $upload['file_name'],
                        'diskon_member' => $this->input->post('diskon_member'),
                    );
                    $save = $this->Models->insert('tb_lapangan', $save);
                    if ($save == true) {
                        echo json_encode(array("success" => TRUE, "notif" => "Data Berhasil ditambahkan"));
                    } else {
                        echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
                    }
                }
            }
        }
    }

    public function delData($id)
    {
        $delete = $this->Models->deletData('tb_lapangan', 'id_lapangan', $id);
        if ($delete) {
            echo json_encode(array('notif' => "Data Berhasil Dihapus"));
        }
    }

    public function delDataSmt($id)
    {
        $data = array('status_lapangan' => '1');

        $deletesmt = $this->Models->deletDatasmt('tb_lapangan', 'id_lapangan', $id, $data);
        if ($deletesmt) {
            echo json_encode(array('notif' => "Data Berhasil Dihapus"));
        }
    }

    public function restoredata($id)
    {
        $data = array('status_lapangan' => '0');

        $deletesmt = $this->Models->deletDatasmt('tb_lapangan', 'id_lapangan', $id, $data);
        if ($deletesmt) {
            echo json_encode(array('notif' => "Data Berhasil Dihapus"));
        }
    }

    function validation_check()
    {
        $data = array('messages' => array());
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('id_jenis', 'Jenis Lapangan', 'required');
        $this->form_validation->set_rules('harga_non_member', 'Harga Non Member', 'required');
        $this->form_validation->set_rules('diskon_member', 'Diskon Member', 'required');

        if ($this->form_validation->run() == FALSE) {
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        } else {
            return true;
        }
        echo json_encode($data);
    }

    public function do_upload()
    {
        $config['upload_path']          = './assets/lapangan';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000; //set max size allowed in Kilobyte
        $config['max_width']            = 1000; // set max width image allowed
        $config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

        $this->load->library('upload', $config);

        // 		if(!$this->upload->do_upload('foto_lapangan')) //upload and validate
        // 		{
        // 				$data['inputerror'][] = 'photo';
        // 				$data['messages'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
        // 				$data['status'] = FALSE;
        // 				echo json_encode($data);
        // 				exit();
        // 		}
        // return $this->upload->data();
        if (isset($_FILES['foto_lapangan']['name'])) {
            if (0 < $_FILES['foto_lapangan']['error']) {
                $msg =  'Error during file upload' . $_FILES['foto_lapangan']['error'];
                // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
                $data['messages']['foto_lapangan'] = '<p class="text-danger">' . $msg . ' </p>';
                echo json_encode($data);
            } else {
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('foto_lapangan')) {
                    $msg = $this->upload->display_errors();
                    $data['messages']['foto_lapangan'] = '<p class="text-danger">' . $msg . ' </p>';
                    echo json_encode($data);
                    // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
                } else {
                    return $this->upload->data();
                }
            }
        } else {
            $data['messages']['foto_lapangan'] = '<p class="text-danger">pilih gambar </p>';
            echo json_encode($data);
        }
    }
}

/* End of file Lapangan.php */
/* Location: ./application/controllers/Lapangan.php */
