<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turnamen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Admin_Futsal/Turnamen/main', 'header'=>'Turnamen','table_header'=>'Kelola Turnamen' );
		$this->load->view('Admin_Futsal/theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		//$id=   $this->session->userdata('id_futsal');
		$output = array();
		$get = $this->Models->get_dataId('tb_turnamen','id_futsal',$this->session->userdata('id_futsal'));
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_turnamen;
			$data[] = $value->tanggal_turnamen;
			$data[] = $value->file_pendaftaran;
			$data[] = $value->biaya_daftar;
			$data[] = $value->no_telephone;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="view_turnamen" data-url="'.base_url('Admin_Futsal/Turnamen/view_page/').''.$value->id_turnamen.'"><i class="fa fa-eye"></i> View</a>
			<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit_turnamen" data-url="'.base_url('Admin_Futsal/Turnamen/get_data/').''.$value->id_turnamen.'"><i class="fa fa-pencil"></i> Edit</a>
			<a href="javascript:void(0)" class="btn btn-danger btn-xs" data-url="'.base_url('Admin_Futsal/Turnamen/delData/').'" onclick="delet('.$value->id_turnamen.');" id="delBtn"><i class="fa fa-trash"></i> Hapus</a>';

			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		// $data = $this->Models->get_dataId('tb_futsal','id_futsal',$id)->row();
		// echo json_encode($data);
		$data = $this->Models->get_dataId('tb_turnamen', 'id_turnamen',$id)->row();
		$turnamen = array(

				'id_turnamen' => $data->id_turnamen,
				'nama_turnamen'=> $data->nama_turnamen,
				'tanggal_turnamen' => $data->tanggal_turnamen,
				'keterangan' => $data->keterangan,
				'biaya_daftar' => $data->biaya_daftar,
				'syarat' => $data->syarat,
				'no_telephone' => $data->no_telephone,
				'file_pendaftaran' => base_url('assets/turnamen/file/').''.$data->file_pendaftaran,
				'foto_turnamen' => base_url('assets/turnamen/foto/').''.$data->foto_turnamen

		 );
			echo json_encode($turnamen);
	}


	public function view_page($id)
	{
		$data = $this->Models->get_dataId('tb_turnamen', 'id_turnamen',$id)->row();
		$turnamen = array(

				'id_turnamen' => $data->id_turnamen,
				'nama_turnamen'=> $data->nama_turnamen,
				'tanggal_turnamen' => $data->tanggal_turnamen,
				'keterangan' => $data->keterangan,
				'biaya_daftar' => $data->biaya_daftar,
				'syarat' => $data->syarat,
				'no_telephone' => $data->no_telephone,
				'file_pendaftaran' => $data->file_pendaftaran,
				'foto_turnamen' => base_url('assets/turnamen/foto/').''.$data->foto_turnamen

		 );
			echo json_encode($turnamen);
	}
	public function delData($id)
	{
		$delete = $this->Models->deletData('tb_turnamen','id_turnamen',$id);
		if ($delete) {
			echo json_encode( array('notif'=> "Data Berhasil Dihapus"));
		}
	}

  public function save()
  {
    $valid = $this->validation_check();
        $id = $this->input->post('id_turnamen');
        $metode = $this->input->post('metode');
        if ($id!='') {
            if ($valid==TRUE) {
                 $save = array(
                   'nama_turnamen' => $this->input->post('nama_turnamen'),
                   'tanggal_turnamen' => $this->input->post('tanggal_turnamen'),
                   'id_futsal' => $this->session->userdata('id_futsal'),
                   'keterangan' => $this->input->post('keterangan'),
                   'biaya_daftar' => $this->input->post('biaya_daftar'),
                   'syarat' => $this->input->post('syarat'),
                   'no_telephone' => $this->input->post('no_telephone'),
                   'status_turnamen' => 'Incomming',
								);
								if(!empty($_FILES['foto_turnamen']['name'])){
									$getData = $this->Models->get_dataId('tb_turnamen','id_turnamen',$id)->row();
									unlink(FCPATH.'assets/turnamen/foto/'.$getData->foto_turnamen);
									$upload = $this->do_upload();
									$save['foto_turnamen'] = $upload['file_name'];
								}
								if(!empty($_FILES['file_pendaftaran']['name'])){
									$getData = $this->Models->get_dataId('tb_turnamen','id_turnamen',$id)->row();
									unlink(FCPATH.'assets/turnamen/file/'.$getData->file_pendaftaran);
									$uploadFile = $this->fileUpload();
									$save['file_pendaftaran'] = $uploadFile['file_name'];
								}
								$save = $this->Models->update('tb_turnamen','id_turnamen',$id, $save);
							if ($save==true) {
								echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
							} else {
								echo json_encode( array('error_db' =>$this->db->error())) ;
							}
            }
        }else {
					if(!empty($_FILES['foto_turnamen']['name'])&&!empty($_FILES['file_pendaftaran']['name']))
					{
						$uploadFoto = $this->do_upload();
						$uploadFile = $this->fileUpload();
						if ($valid==TRUE &&  $uploadFoto==true && $uploadFile==true) {
								$save = array(
									'nama_turnamen' => $this->input->post('nama_turnamen'),
									'tanggal_turnamen' => $this->input->post('tanggal_turnamen'),
									'id_futsal' => $this->session->userdata('id_futsal'),
									'keterangan' => $this->input->post('keterangan'),
									'biaya_daftar' => $this->input->post('biaya_daftar'),
									'syarat' => $this->input->post('syarat'),
									'file_pendaftaran'=> $uploadFile['file_name'],
									'no_telephone' => $this->input->post('no_telephone'),
									'foto_turnamen'=> $uploadFoto['file_name'],
									'status_turnamen' => 'Incomming',
										);
								$save = $this->Models->insert('tb_turnamen',$save);
								if ($save==true) {
										echo json_encode(array("success" => TRUE,"notif" =>"Data Berhasil ditambahkan"));
								} else {
										echo json_encode(array("fail" => TRUE, "message"=>$this->db->error()));
								}
						}
					}
        }
    }

    function validation_check() {
			$data = array('messages' => array() );
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			$this->form_validation->set_rules('nama_turnamen','Nama Turnamen', 'required');
			$this->form_validation->set_rules('tanggal_turnamen','Tanggal', 'required');
			$this->form_validation->set_rules('keterangan','Keterangan', 'required');
			$this->form_validation->set_rules('biaya_daftar','Baiaya', 'required');
			$this->form_validation->set_rules('syarat','Syarata', 'required');
			$this->form_validation->set_rules('no_telephone','No Telephone', 'required');
			if ($this->form_validation->run() == FALSE) {
					foreach ($_POST as $key => $value) {
					$data['messages'][$key] = form_error($key);
				}
			}
			else {
				return true;
			}
			echo json_encode($data);
	}

  public function do_upload()
{
			$config['upload_path']          = './assets/turnamen/foto';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 1000; //set max size allowed in Kilobyte
			$config['max_width']            = 1000; // set max width image allowed
			$config['max_height']           = 1000; // set max height allowed
			$config['file_name']            = round(microtime(true) * 1000);

			$this->load->library('upload', $config, 'foto');
			$this->foto->initialize($config);
			if (isset($_FILES['foto_turnamen']['name'])) {
					if (0 < $_FILES['foto_turnamen']['error']) {
							$msg =  'Error during file upload' . $_FILES['foto_turnamen']['error'];
							$data['messages']['foto_turnamen'] = '<p class="text-danger">'.$msg.' </p>';
							echo json_encode($data);
					} else {
						if (!$this->foto->do_upload('foto_turnamen')) {
								$msg = '<p class="text-danger">'.$this->foto->display_errors().' </p>';
									$data['messages']['foto_turnamen'] = $msg;
									echo json_encode($data);
						} else {
								return $this->foto->data();
						}
					}
			} else {
					$data['messages']['foto_turnamen'] = '<p class="text-danger">pilih gambar </p>';
					echo json_encode($data);
			}
}


// public function do_upload_file()
// {
// 		$config['upload_path']          = './assets/turnamen';
//     $config['allowed_types']        = 'pdf|doc|docx';
//     $config['file_name_file']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

//     $this->load->library('upload', $config);

// if (isset($_FILES['file_pendaftaran']['name'])) {
//           if (0 < $_FILES['file_pendaftaran']['error']) {
//               $msg =  'Error during file upload' . $_FILES['file_pendaftaran']['error'];
//               // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
//               $data['messages']['file_pendaftaran'] = '<p class="text-danger">'.$msg.' </p>';
//               echo json_encode($data);
//           } else {
//                   $this->load->library('upload', $config);
//                   if (!$this->upload->do_upload('file_pendaftaran')) {
//                       $msg = $this->upload->display_errors();
//                         $data['messages']['file_pendaftaran'] = '<p class="text-danger">'.$msg.' </p>';
//               echo json_encode($data);
//                       // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
//                   } else {
//                       return $this->upload->data();
//                   }
//           }
//       } else {
//           $data['messages']['file_pendaftaran'] = '<p class="text-danger">pilih file </p>';
//           echo json_encode($data);
//       }
// }
	public function fileUpload()
	{
			$config = array();
			$config['upload_path']   = './assets/turnamen/file';
			$config['allowed_types'] = 'pdf|doc|docx';
			$config['file_name']     = round(microtime(true) * 1000);
			$this->load->library('upload', $config, 'uploadFile');
			$this->uploadFile->initialize($config);

			if (isset($_FILES['file_pendaftaran']['name'])) {
						if (0 < $_FILES['file_pendaftaran']['error']) {
								$msg =  'Error during file upload' . $_FILES['file_pendaftaran']['error'];
								$data['messages']['file_pendaftaran'] = '<p class="text-danger">'.$msg.' </p>';
								echo json_encode($data);
						} else {
							if (!$this->uploadFile->do_upload('file_pendaftaran')) {
									$msg = '<p class="text-danger">'.$this->uploadFile->display_errors().' </p>';
									$this->uploadFile->display_errors('<p>', '</p>');
										$data['messages']['file_pendaftaran'] = $msg;
										echo json_encode($data);
							} else {
									return  $this->uploadFile->data();
							}
						}
				} else {
						$data['messages']['file_pendaftaran'] = '<p class="text-danger">pilih file </p>';
						echo json_encode($data);
				}
	}


}

/* End of file Futsal.php */
/* Location: ./application/controllers/Futsal.php */
