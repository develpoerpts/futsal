<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('App_models', 'Models');
    $this->load->helper('My_helper');
  }


  public function jumlahTime()
  {
    $lama = $this->input->post('lama_booking');
    $mulai = $this->input->post('tanggal_booking');
    $waktu = $this->input->post('waktu');

    $new_time = date("Y-m-d H:i:s", strtotime('+' . $lama . ' hours', strtotime($mulai . $waktu)));
    $data = array();
    $data['tanggal'] = date("Y-m-d", strtotime($new_time));
    $data['waktu'] = date("H:i:s", strtotime($new_time));

    // $data[] = date_format($new_time,"H:i:s");
    echo json_encode($data);
  }

  public function booking()
  {
    $isi['title']       = 'Detail Lapangan';
    $isi['content']     = 'frontend/transaksi';
    $kode = $this->generatecode(7);
    // $isi['judul']       = 'home';
    // $isi['sub_judul']   = '';
    $id = $this->uri->segment(3);
    //	$this->db->where('id_lapangan',$key);
    $query = $this->Models->get_lapangan_Id_front($id);
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $isi['id_lapangan']    = $row->id_lapangan;
        $isi['nama_futsal']    = $row->nama_futsal;
        $isi['harga_non_member']  = $row->harga_non_member;
        $isi['diskon_member']      = $row->diskon_member;
        $isi['foto_lapangan']      = $row->foto_lapangan;
        $isi['jenis']    = $row->jenis;
        $isi['alamat']      = $row->alamat;
        $isi['fasilitas']    = $row->fasilitas;
        $isi['keterangan']    = $row->keterangan;
        $isi['kode']    = $kode;
      }
    } else {
      $isi['id_lapangan']    = '';
      $isi['nama_futsal']    = '';
      $isi['harga_non_member']  = '';
      $isi['diskon_member']      = '';
      $isi['foto_lapangan']  = '';
      $isi['jenis']    = '';
      $isi['alamat']  = '';
      $isi['fasilitas']  = '';
      $isi['keterangan']      = '';
      $isi['kode']      = '';
    }
    $this->load->view('frontend/tampilan_frontend', $isi);
  }
  public function bookingMember()
  {
    $isi['title']       = 'Detail Lapangan';
    $isi['content']     = 'frontend/transaksiMember';
    $kode = $this->generatecode(7);
    // $isi['judul']       = 'home';
    // $isi['sub_judul']   = '';
    $id = $this->uri->segment(3);
    //	$this->db->where('id_lapangan',$key);
    $query = $this->Models->get_lapangan_Id_front($id);
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $isi['id_lapangan']    = $row->id_lapangan;
        $isi['nama_futsal']    = $row->nama_futsal;
        $isi['harga_non_member']  = $row->harga_non_member;
        $isi['diskon_member']      = $row->diskon_member;
        $isi['foto_lapangan']      = $row->foto_lapangan;
        $isi['jenis']    = $row->jenis;
        $isi['alamat']      = $row->alamat;
        $isi['fasilitas']    = $row->fasilitas;
        $isi['keterangan']    = $row->keterangan;
        $isi['kode']    = $kode;
      }
    } else {
      $isi['id_lapangan']    = '';
      $isi['nama_futsal']    = '';
      $isi['harga_non_member']  = '';
      $isi['diskon_member']      = '';
      $isi['foto_lapangan']  = '';
      $isi['jenis']    = '';
      $isi['alamat']  = '';
      $isi['fasilitas']  = '';
      $isi['keterangan']      = '';
      $isi['kode']      = '';
    }

    $this->load->view('frontend/tampilan_frontend', $isi);
  }

  function generatecode($length)
  {
    $random = "";
    srand((float) microtime() * 1000000);

    $data = "123456789";
    // $data . = "0FGH45OP89";

    for ($i = 0; $i < $length; $i++) {
      $random  .= substr($data, (rand() % (strlen($data))), 1);
    }

    return $random;
  }


  public function save()
  {

    $valid = $this->validation_check();
    $id = $this->input->post('id_booking');
    // $jam = $this->input->post('jam_booking');
    $lama = $this->input->post('lama_booking');
    $jam = $this->input->post('jam_booking');
    $strtime = strtotime("$jam");
    // $start = date('H:i:s', $strtime);
    $end = date('H:i:s', strtotime('+' . $lama . ' hour', $strtime));
    $metode = $this->input->post('metode');
    if ($id != '') {
      if ($valid === TRUE) {

        $save = array(
          'no_booking' => $this->input->post('no_booking'),
          'nama_booking' => $this->input->post('nama_booking'),
          'tanggal_booking' => $this->input->post('tanggal_booking'),
          'jam_booking' => $this->input->post('jam_booking'),
          'selesai_booking' => $end,
          'lama_booking' => $this->input->post('lama_booking'),
          // 'jam_selesai' => $this->input->post('jam_selesai'),
          // 'tanggal_selesai' => $this->input->post('tanggal_selesai'),
          'no_hp_booking' => $this->input->post('no_hp_booking'),
          'status_member' => 'Non Member',
          'id_lapangan' => $this->input->post('id_lapangan'),
          'total_harga' => $this->input->post('total_harga'),
          'status_booking' => 'Pending',
        );
        $save = $this->Models->update('tb_booking', 'id_booking', $id, $save);
        if ($save == true) {
          echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
        } else {
          echo json_encode(array('error_db' => $this->db->error()));
        }
      }
    } else {
      if ($valid === TRUE) {
        $save = array(
          'no_booking' => $this->input->post('no_booking'),
          'nama_booking' => $this->input->post('nama_booking'),
          'tanggal_booking' => $this->input->post('tanggal_booking'),
          'jam_booking' => $this->input->post('jam_booking'),
          'email' => $this->input->post('email'),
          'selesai_booking' => $end,
          'lama_booking' => $this->input->post('lama_booking'),
          // 'jam_selesai' => $this->input->post('jam_selesai'),
          // 'tanggal_selesai' => $start,
          'no_hp_booking' => $this->input->post('no_hp_booking'),
          'status_member' => 'Non Member',
          'id_lapangan' =>  $this->input->post('id_lapangan'),
          'total_harga' => $this->input->post('total_harga'),
          'status_booking' => 'Pending',
          'expired_time' => date('Y-m-d H:i:s', strtotime('+1 hour'))
        );
        $save = $this->Models->insert('tb_booking', $save);
        $no_booking = $this->input->post('no_booking');

        if ($save == true) {
          $ses['tamu'] = array(
            'non_member' => true,
            'email' => $this->input->post('email'),
            'nama' => $this->input->post('nama_booking')
          );
          $this->session->set_userdata($ses);
          $redirect = base_url('Riwayat_Booking/bayar/' . $no_booking);
          echo json_encode(array("success" => true, 'go_to' => $redirect));
        } else {
          echo json_encode(array("fail" => TRUE, "message" => $valid));
        }
      }
    }
  }

  public function saveBookingAsMember()
  {
    $checkmember = $this->check_login();
    if ($checkmember !== false) {
      $valid = $this->validation_checkMember();
      $id = $this->input->post('id_booking');
      $metode = $this->input->post('metode');
      $lama = $this->input->post('lama_booking');
      $jam = $this->input->post('jam_booking');
      $strtime = strtotime("$jam");
      // $start = date('H:i:s', $strtime);
      $end = date('H:i:s', strtotime('+' . $lama . ' hour', $strtime));
      if ($id != '') {
        if ($valid === TRUE) {
          $save = array(
            'no_booking' => $this->input->post('no_booking'),
            'nama_booking' => $this->input->post('nama_booking'),
            'tanggal_booking' => $this->input->post('tanggal_booking'),
            'jam_booking' => $this->input->post('jam_booking'),
            'selesai_booking' => $end,
            'lama_booking' => $this->input->post('lama_booking'),
            // 'jam_selesai' => $this->input->post('jam_selesai'),
            // 'tanggal_selesai' => $this->input->post('tanggal_selesai'),
            'no_hp_booking' => $this->input->post('no_hp_booking'),
            'status_member' => 'Member',
            'id_lapangan' => $this->input->post('id_lapangan'),
            'total_harga' => $this->input->post('total_harga'),
            'status_booking' => 'Pending',
          );
          // $save = $this->Models->update('tb_booking', 'id_booking', $id, $save);
          if ($save == true) {
            echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
          } else {
            echo json_encode(array('error_db' => $this->db->error()));
          }
        }
      } else {
        if ($valid === TRUE) {
          $save = array(
            'no_booking' => $this->input->post('no_booking'),
            'nama_booking' =>  $checkmember->nama_member,
            'tanggal_booking' => $this->input->post('tanggal_booking'),
            'jam_booking' => $this->input->post('jam_booking'),
            'lama_booking' => $this->input->post('lama_booking'),
            'selesai_booking' => $end,
            // 'tanggal_selesai' => $this->input->post('tanggal_selesai'),
            'no_hp_booking' => $checkmember->no_telephone,
            'status_member' => 'Member',
            'id_lapangan' =>  $this->input->post('id_lapangan'),
            'id_member' => $checkmember->id_member,
            'email' => $checkmember->email,
            'total_harga' => $this->input->post('total_harga'),
            'status_booking' => 'Pending',
            'expired_time' => date('Y-m-d H:i:s', strtotime('+1 hour'))
          );
          $save = $this->Models->insert('tb_booking', $save);
          $no_booking = $this->input->post('no_booking');

          if ($save == true) {
            // echo json_encode(array("success" => TRUE));
            // redirect('Riwayat_Booking/bayar/' . $no_booking);
            $redirect = base_url('Riwayat_Booking/bayar/' . $no_booking);
            echo json_encode(array("success" => true, 'go_to' => $redirect));
          } else {
            echo json_encode(array("fail" => TRUE, "message" => $valid));
          }
        }
      }
    } else {
      // redirect('Transaksi/booking/');
      $data['messages']['email'] = '<p>Email dan Password Salah !</p>';
      echo json_encode($data);
    }
  }

  function toRiwayat()
  {
    $no_booking = $this->input->post('no_booking');
    redirect('Riwayat_Booking/bayar/' . $no_booking);
  }

  public function cekBook()
  {
    $id_lapangan = $this->input->post('id_lapangan');
    $tanggal_booking = $this->input->post('tanggal_booking');
    $jam_booking = $this->input->post('jam_booking');
    $tanggal_selesai = $this->input->post('tanggal_selesai');
    $jam_selesai = $this->input->post('jam_selesai');

    $query = "select * from tb_booking where (tanggal_booking > '$tanggal_booking' and tanggal_selesai < '$tanggal_selesai')or (tanggal_booking = '$tanggal_booking' and jam_booking >= '$jam_booking')or (tanggal_selesai = '$tanggal_selesai' and jam_selesai <= '$jam_selesai') and id_lapangan = '$id_lapangan'";
    $output = array();
    $get = $this->db->query($query);

    if ($get->num_rows() > 0) {
      $this->session->set_flashdata('error', 'Maaf Username atau Password salah');
      redirect('Transaksi/booking/' . $id_lapangan);
    } else {
      $this->save();
    }
  }

  public function cekBookMember()
  {
    $id_lapangan = $this->input->post('id_lapangan');
    $tanggal_booking = $this->input->post('tanggal_booking');
    $jam_booking = $this->input->post('jam_booking');
    $tanggal_selesai = $this->input->post('tanggal_selesai');
    $jam_selesai = $this->input->post('jam_selesai');

    $query = "select * from tb_booking where (tanggal_booking > '$tanggal_booking' and tanggal_selesai < '$tanggal_selesai')or (tanggal_booking = '$tanggal_booking' and jam_booking >= '$jam_booking')or (tanggal_selesai = '$tanggal_selesai' and jam_selesai <= '$jam_selesai') and id_lapangan = '$id_lapangan'";
    $output = array();
    $get = $this->db->query($query);

    if ($get->num_rows() > 0) {
      $this->session->set_flashdata('error', 'Maaf Username atau Password salah');
      redirect('Transaksi/booking/' . $id_lapangan);
    } else {
      $this->saveBookingAsMember();
    }
  }

  public function check_login()
  {
    $email = $this->input->post('email');
    $password = md5($this->input->post('password'));
    $check = $this->Models->cekLogin($email, $password);
    if ($check->num_rows() > 0) {
      $data = $check->row();
      return $data;
    } else {
      return false;
    }
  }

  function validation_check()
  {
    $data = array('messages' => array());
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    $this->form_validation->set_rules('nama_booking', 'Nama Booking ', 'trim|required|alpha|min_length[3]');
    $this->form_validation->set_rules('email', 'Email ', 'required|valid_email|min_length[6]');
    $this->form_validation->set_rules('tanggal_booking', 'Tanggal', 'required');
    $this->form_validation->set_rules('jam_booking', 'jam_booking', 'required');
    $this->form_validation->set_rules('no_hp_booking', 'No Telephone', 'required|integer');
    $this->form_validation->set_rules('lama_booking', 'Lama Booking', 'required');

    if ($this->form_validation->run() == FALSE) {
      foreach ($_POST as $key => $value) {
        $data['messages'][$key] = form_error($key);
      }
      echo json_encode($data);
    } else {
      return true;
    }
  }

  function validation_checkMember()
  {
    $data = array('messages' => array());
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
    $this->form_validation->set_rules('tanggal_booking', 'Tanggal', 'required');
    $this->form_validation->set_rules('jam_booking', 'jam_booking', 'required');
    $this->form_validation->set_rules('password', 'Passwors', 'required');
    $this->form_validation->set_rules('lama_booking', 'Lama Booking', 'required');

    if ($this->form_validation->run() == FALSE) {
      foreach ($_POST as $key => $value) {
        $data['messages'][$key] = form_error($key);
      }
    } else {
      return true;
    }
    echo json_encode($data);
  }
  public function visiblecheck()
  {
    $id_lapangan = $this->input->post('id_lapangan');
    $tanggal = $this->input->post('tanggal_booking');
    $lama = $this->input->post('lama_booking');
    if ($lama == '') {
      $slama = 0;
    } else {
      $slama = $lama;
    }
    $jam = $this->input->post('jam_booking');
    $strtime = strtotime("$jam");
    $strdate = date('Y-m-d H:i:s', strtotime('+' . $slama . ' hour', strtotime("$tanggal $jam")));
    $start = date('H:i:s', $strtime);
    $end = date('H:i:s', strtotime('+' . $slama . ' hour', $strtime));


    $n_tanggal = date('Y-m-d', strtotime('+' . $slama . ' hour', $strtime));
    $dates_now = date('Y-m-d', strtotime($strdate));
    $check = $this->Models->check_booking($id_lapangan, $tanggal, $start, $end);
    $d_now = date('Y-m-d');
    $n_start = date('H:i', $strtime);
    $_now = date('H:i');
    $t_now = date('H:i', strtotime('+10 Minutes', strtotime("$_now")));
    if (($d_now == $tanggal) && ($n_start <= $t_now)) {
      $check = false;
    }
    // for ($i = 0; $i < 6; $i++) {
    //   if ($end = date('H:i', strtotime('+' . $i++ . ' hour', strtotime('23:00:00')))) {
    //     $check = false;
    //   }
    // }

    if ($end > date('H:i:s', strtotime('23:00:00')) or date('H', strtotime($end)) ==  date('H', strtotime('00:00:00')) or date('H', strtotime($end)) <=  date('H', strtotime('08:00:00'))) {
      $check = false;
    }
    if ($check !== false) {
      echo json_encode(array('start' => $n_start, 'end' => $dates_now, 'lama' => $t_now, 'success' => true));
    } else {
      echo json_encode(array('start' => $n_start, 'end' => $dates_now, 'lama' => $t_now, 'gagal' => true));
    }
  }
}
