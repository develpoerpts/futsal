<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bkk extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Bkk/main', 'header'=>'BKK','table_header'=>'Data BKK' );
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tbl_bkk');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_bkk;
			$data[] = $value->alamat_bkk;
			$data[] = $value->nama_ketua;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="'.base_url('Bkk/get_data/').'" onclick="edit('.$value->id_bkk.');" id="editBtn"><i class="fa fa-pencil"></i> Edit</a>';
			$output[] = $data;
		}
		echo json_encode(array('data'=>$output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tbl_bkk','id_bkk',$id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_bkk');
        $metode = $this->input->post('metode');
        if ($id!=='') {
        	if ($valid==true) {
				$input = array(
					'nama_bkk' => $this->input->post('nama_bkk'),
					'alamat_bkk' => $this->input->post('alamat_bkk'),
					'nama_ketua' => $this->input->post('nama_ketua'),
					);
				$save = $this->Models->update('tbl_bkk','id_bkk',$id, $input);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
			}
        }else {
        	if ($valid==true) {
				$input = array(
					'nama_bkk' => $this->input->post('nama_bkk'),
					'alamat_bkk' => $this->input->post('alamat_bkk'),
					'nama_ketua' => $this->input->post('nama_ketua'),
					);
				$save = $this->Models->insert('tbl_bkk', $input);
				if ($save==true) {
					echo json_encode( array('success'=>true, 'notif' => "Data Berhasil Di Tambahkan"));
				} else {
					echo json_encode( array('error_db' =>$this->db->error())) ;
				}
			}
        }
	}

	function validation_check() {
		$data = array('messages' => array() );
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_bkk','Nama BKK', 'required');
		$this->form_validation->set_rules('alamat_bkk','Alamat', 'required');
		$this->form_validation->set_rules('nama_ketua','Nama Ketua','required');
		if ($this->form_validation->run() == FALSE) {
		    foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		}
		else {
			return true;
		}
		echo json_encode($data);
	}

}

/* End of file Bkk.php */
/* Location: ./application/controllers/Bkk.php */