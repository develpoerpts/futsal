<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Booking extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Booking/main', 'header' => 'Booking', 'table_header' => 'Data Booking');
		$this->load->view('theme', $data);
	}
	// public function filter()
	// {
	// 	$start = $this->input->post('start');
	// 	$end = $this->input->post('end');

	// }
	public function get_tables()
	{
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$no = 1;
		$output = array();
		$get = $this->Models->get_bookingfilter($start, $end);
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->no_booking;
			$data[] = $value->nama_booking;
			$data[] = $value->tanggal_booking;
			$data[] = $value->jam_booking;
			$data[] = $value->status_member;
			$data[] = $value->jenis;
			$data[] = $value->nama_futsal;
			$data[] = $value->total_harga;
			$data[] = $value->status_booking;
			$data[] = '<a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="' . base_url('Booking/get_data/') . '" onclick="edit(' . $value->id_booking . ');" id="editBtn"><i class="fa fa-eye"></i> Detail</a>';
			// $data[] = $value->alamat_perusahaan;
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataIdBooking('id_booking', $id)->row();
		echo json_encode($data);
	}
	public function save()
	{
		$valid = $this->validation_check();
		$id = $this->input->post('id_booking');
		$metode = $this->input->post('metode');
		if ($id != '') {
			if ($valid == TRUE) {
				$save = array(
					'no_booking' => $this->input->post('no_booking'),
					'nama_booking' => $this->input->post('nama_booking'),
					'tanggal_booking' => $this->input->post('tanggal_booking'),
					'jam_booking' => $this->input->post('jam_booking'),
					'no_hp_booking' => $this->input->post('no_hp_booking'),
					'stattus_member' => 'Non Member',
					'id_lapangan' => $id_lapangan,
					'total_harga' => $this->input->post('total_harga'),
					'stattus_member' => 'Pending',
				);
				$save = $this->Models->update('tb_booking', 'id_booking', $id, $save);
				if ($save == true) {
					echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
				} else {
					echo json_encode(array('error_db' => $this->db->error()));
				}
			}
		} else {
			if ($valid == TRUE) {
				$save = array(
					'no_booking' => $this->input->post('no_booking'),
					'nama_booking' => $this->input->post('nama_booking'),
					'tanggal_booking' => $this->input->post('tanggal_booking'),
					'jam_booking' => $this->input->post('jam_booking'),
					'no_hp_booking' => $this->input->post('no_hp_booking'),
					'stattus_member' => 'Non Member',
					'id_lapangan' => $id_lapangan,
					'total_harga' => $this->input->post('total_harga'),
					'stattus_member' => 'Pending',
				);
				$save = $this->Models->insert('tb_booking', $save);
				if ($save == true) {
					echo json_encode(array("success" => TRUE));
				} else {
					echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_booking', 'Nama Tempat', 'required');
		$this->form_validation->set_rules('tanggal_booking', 'Alamat', 'required');
		$this->form_validation->set_rules('jam_booking', 'Keterangan', 'required');
		$this->form_validation->set_rules('no_hp_booking', 'Nama Bank', 'required');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}
}

/* End of file Booking.php */
/* Location: ./application/controllers/Booking.php */
