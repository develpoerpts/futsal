<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('App_models', 'Models');
	}

	public function index()
	{
		$data = array('content' => 'Member/main', 'header' => 'Member', 'table_header' => 'Data Member');
		$this->load->view('theme', $data);
	}
	public function get_tables()
	{
		$no = 1;
		$output = array();
		$get = $this->Models->get_data('tb_member');
		foreach ($get->result() as $key => $value) {
			$data = array();
			$data[] = $no++;
			$data[] = $value->nama_member;
			$data[] = $value->alamat_member;
			$data[] = $value->no_telephone;
			$data[] = $value->email;
			$data[] = $value->status;
			$data[] = '<a href="javascript:void(0)" class="btn btn-info btn-xs" id="view_daftar_member" data-url="' . base_url('Member/get_data_edit/') . '' . $value->id_member . '"><i class="fa fa-eye"></i> View</a>
				<a href="javascript:void(0)" class="btn btn-danger btn-xs" data-url="' . base_url('Member/delData/') . '" onclick="delet(' . $value->id_member . ');" id="delBtn"><i class="fa fa-trash"></i> Hapus</a>';
			// $data[] = $value->alamat_perusahaan; <a href="javascript:void(0)" class="btn btn-primary btn-xs" id="edit" data-url="' . base_url('Member/get_data/') . '" onclick="edit(' . $value->id_member . ');" id="editBtn"><i class="fa fa-eye"></i> Detail</a>
			// $data[] = $value->nama_ketua;
			$output[] = $data;
		}
		echo json_encode(array('data' => $output));
	}
	public function get_data($id)
	{
		$data = $this->Models->get_dataId('tb_member', 'id_member', $id)->row();
		echo json_encode($data);
	}
	public function get_data_edit($id)
	{
		$data = $this->Models->get_dataId('tb_member', 'id_member', $id)->row();
		$daftarMember = array(
			'id_member' => $data->id_member,
			'nama_member' => $data->nama_member,
			'alamat_member' => $data->alamat_member,
			'email' => $data->email,
			'no_telephone' => $data->no_telephone,
			'status' => $data->status,
			'foto_bukti_transfer_daftar' => base_url('assets/member/') . '' . $data->foto_bukti_transfer_daftar
		);
		echo json_encode($daftarMember);
	}

	public function delData($id)
	{
		$delete = $this->Models->deletData('tb_member', 'id_member', $id);
		if ($delete) {
			echo json_encode(array('notif' => "Data Berhasil Dihapus"));
		}
	}

	public function save()
	{

		$id = $this->input->post('id_member');
		$metode = $this->input->post('metode');
		if ($metode == 'status') {
			$save = array(
				'status' => $this->input->post('status'),
			);
			$save = $this->Models->update('tb_Member', 'id_Member', $id, $save);
			if ($save == true) {
				echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
			} else {
				echo json_encode(array('error_db' => $this->db->error()));
			}
		} else {
			$valid = $this->validation_check();
			if ($id != '') {
				if ($valid == TRUE) {
					$save = array(
						'nama_member' => $this->input->post('nama_member'),
						'alamat' => $this->input->post('alamat'),
						'keterangan' => $this->input->post('keterangan'),
						'nama_bank' => $this->input->post('nama_bank'),
						'no_rek' => $this->input->post('no_rek'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'),
						'status' => $this->input->post('status'),
					);
					$save = $this->Models->update('tb_Member', 'id_Member', $id, $save);
					if ($save == true) {
						echo json_encode(array('success' => true, 'notif' => "Data Berhasil Di Ubah"));
					} else {
						echo json_encode(array('error_db' => $this->db->error()));
					}
				}
			} else {
				if ($valid == TRUE) {
					$save = array(
						'nama_member' => $this->input->post('nama_member'),
						'alamat' => $this->input->post('alamat'),
						'keterangan' => $this->input->post('keterangan'),
						'nama_bank' => $this->input->post('nama_bank'),
						'no_rek' => $this->input->post('no_rek'),
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password'),
					);
					$save = $this->Models->insert('tb_Member', $save);
					if ($save == true) {
						echo json_encode(array("success" => TRUE));
					} else {
						echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
					}
				}
			}
		}
	}

	function validation_check()
	{
		$data = array('messages' => array());
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('nama_Member', 'Nama Tempat', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'required');
		$this->form_validation->set_rules('no_rek', 'No Rekening', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Pasword', 'required');
		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$data['messages'][$key] = form_error($key);
			}
		} else {
			return true;
		}
		echo json_encode($data);
	}
}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */
