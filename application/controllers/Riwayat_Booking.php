<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_Booking extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('App_models', 'Models');
    $this->load->helper('My_helper');
  }

  public function index()
  {


    $isi['title']       = 'Riwayat Booking';
    $isi['content']     = 'frontend/riwayat_booking';
    //  $isi['data']		= $this->db->where('id_user',$this->session->userdata('id_user'))->order_by('id_transaksi', 'DESC')->get('transaksi');
    // $isi['judul']       = 'home';
    // $isi['sub_judul']   = '';
    $this->load->view('frontend/tampilan_frontend', $isi);
  }


  public function bayar()
  {
    $isi['title']       = 'Pemabayaran';
    $isi['content']     = 'frontend/bayar';

    $no_booking = $this->uri->segment(3);
    $query = $this->Models->get_dataIdBooking('no_booking', $no_booking);
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $isi['id_booking']      = $row->id_booking;
        $isi['no_booking']        = $row->no_booking;
        $isi['nama_booking']        = $row->nama_booking;
        $isi['tanggal_booking']    = $row->tanggal_booking;
        $isi['jam_booking']      = $row->jam_booking;
        // $isi['tanggal_selesai']		= $row->tanggal_selesai;
        $isi['jam_selesai']      = $row->selesai_booking;
        $isi['lama_booking']    = $row->lama_booking;
        $isi['no_hp_booking']         = $row->no_hp_booking;
        $isi['status_member']          = $row->status_member;
        $isi['nama_futsal']      = $row->nama_futsal;
        $isi['jenis']    = $row->jenis;
        $isi['alamat']         = $row->alamat;
        $isi['nama_bank']          = $row->nama_bank;
        $isi['no_rek']      = $row->no_rek;
        $isi['nama_rek']    = $row->nama_rek;
        $isi['foto_lapangan']    = $row->foto_lapangan;
        $isi['total_harga']         = $row->total_harga;
        $isi['status_booking']          = $row->status_booking;
        $this->load->view('frontend/tampilan_frontend', $isi);
      }
    } else {
      // dalahi alert nang kene
      $this->session->set_flashdata('error', 'Maaf kode booking tersebut tidak ada!');
      redirect(base_url('Welcome'), 'refresh');
    }
  }

  public function save()
  {
    $valid = $this->validation_check();
    $id_booking = $this->input->post('id_booking');
    $no_booking = $this->input->post('no_booking');
    $metode = $this->input->post('metode');
    if ($id_booking != '') {
      $input = array(
        'nama_rekening_booking' => $this->input->post('nama_rekening_booking'),
        'status_booking' => 'On Process',
      );
      if (!empty($_FILES['foto_transfer']['name'])) {
        $getData = $this->Models->get_dataId('tb_booking', 'id_booking', $id_booking)->row();
        // //	unlink(FCPATH.'assets/bookingTransfer/'.$getData->foto_transfer);
        $upload = $this->do_upload();
        $input['foto_transfer'] = $upload['file_name'];
      }
      $save = $this->Models->update('tb_booking', 'id_booking', $id_booking, $input);
      if ($save == true) {
        sendEmail($getData->email, 'mail/confrim');
        redirect('Riwayat_Booking/bayar/' . $no_booking);
      }
    } else {
      if ($valid == TRUE) {
        $save = array(
          'no_booking' => $this->input->post('no_booking'),
          'nama_booking' => $this->input->post('nama_booking'),
          'tanggal_booking' => $this->input->post('tanggal_booking'),
          'jam_booking' => $this->input->post('jam_booking'),
          'lama_booking' => $this->input->post('lama_booking'),
          'no_hp_booking' => $this->input->post('no_hp_booking'),
          'status_member' => 'Non Member',
          'id_lapangan' =>  $this->input->post('id_lapangan'),
          'total_harga' => $this->input->post('total_harga'),
          'status_booking' => 'Pending',
          'payment_stat' => '1',
        );
        $save = $this->Models->insert('tb_booking', $save);
        $no_booking = $this->input->post('no_booking');

        if ($save == true) {
          echo json_encode(array("success" => TRUE));
          redirect('Riwayat_Booking/bayar/' . $no_booking);
        } else {
          echo json_encode(array("fail" => TRUE, "message" => $this->db->error()));
        }
      }
    }
  }


  public function do_upload()
  {
    $config['upload_path']          = './assets/bookingTransfer';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 2400; //set max size allowed in Kilobyte
    $config['max_width']            = 2400; // set max width image allowed
    $config['max_height']           = 2400; // set max height allowed
    $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name

    $this->load->library('upload', $config);

    if (isset($_FILES['foto_transfer']['name'])) {
      if (0 < $_FILES['foto_transfer']['error']) {
        $msg =  'Error during file upload' . $_FILES['foto_transfer']['error'];
        // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
        $data['messages']['foto_transfer'] = '<p class="text-danger">' . $msg . ' </p>';
        echo json_encode($data);
      } else {
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto_transfer')) {
          $msg = $this->upload->display_errors();
          $data['messages']['foto_transfer'] = '<p class="text-danger">' . $msg . ' </p>';
          echo json_encode($data);
          // echo json_encode(array("gagal" => TRUE,"messages"=> $msg));
        } else {
          return $this->upload->data();
        }
      }
    } else {
      $data['messages']['foto_transfer'] = '<p class="text-danger">pilih gambar </p>';
      echo json_encode($data);
    }
  }

  function validation_check()
  {
    $data = array('messages' => array());
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    $this->form_validation->set_rules('id_booking', 'Nama ', 'required');
    $this->form_validation->set_rules('nama_rekening', 'Nama Rekening ', 'required');
    $this->form_validation->set_rules('foto_transfer', 'Foro Bukti ', 'required');


    if ($this->form_validation->run() == FALSE) {
      foreach ($_POST as $key => $value) {
        $data['messages'][$key] = form_error($key);
      }
    } else {
      return true;
    }
    echo json_encode($data);
  }

  public function cetak()
  {
    // $this->load->library('dompdf_gen');
    $this->load->library('pdf');

    $kode = time() . ".pdf";
    // $pondok = $this->db->where('id_pondok', $id)->join('tb_pondok', 'tb_pondok.id=tb_users.id_pondok')->get('tb_users')->row();
    // $nama = $this->db->where('id ', $id)->get('tb_users')->row()->nama;

    $isi['title'] = 'Cetak Bukti';
    $no_booking = $this->uri->segment(3);
    $isi['data']  = $this->Models->get_dataIdBooking('no_booking', $no_booking)->row();
    $isi['stempel']  = $this->Models->get_data('tb_stemple', 'id_futsal', $isi['data']->id_futsal)->row();
    //  $isi['nama'] = $nama;
    // $data['data'] = $this->MTransaksi->getdata();
    $this->load->view('frontend/cetak_laporan', $isi);

    $paper_size  = 'A4';
    $orientation = 'portrait';
    $html = $this->output->get_output();


    $this->dompdf->set_paper($paper_size, $orientation);

    $this->dompdf->load_html($html);
    $this->dompdf->render();
    $this->dompdf->stream($kode, array('Attachment' => 0));
  }
}
