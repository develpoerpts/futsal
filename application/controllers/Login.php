<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	//tampilan login admin
	public function index()
	{
		$isi['title']       = 'Login Admin';
		$this->load->view('tampilan_login', $isi);
	}

	//mengambil data login dari model
	public function getlogin()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$a = $this->input->post('status');
		$this->load->model('M_model_login');
		$rowAdmin = $this->M_model_login->getlogin($u, $p);
	}
	public function logout()
	{
		$this->session->sess_destroy();

		redirect(base_url(), 'refresh');
	}
}
