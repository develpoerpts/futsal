<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Chart_model extends CI_Model
{
    public function getLabel_lapangan()
    {
        $isAdmin = $this->session->userdata('tb_admin');
        if ($isAdmin) {
            $this->db->select('tb_jenis_lapangan.jenis, tb_futsal.nama_futsal, tb_lapangan.id_lapangan');
            $this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
            $this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
            $get = $this->db->get('tb_lapangan');
        } else {
            $id_futsal = $this->session->userdata('id_futsal');
            $this->db->select('tb_jenis_lapangan.jenis, tb_futsal.nama_futsal, tb_lapangan.id_lapangan');
            $this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
            $this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
            $this->db->where('tb_lapangan.id_futsal', $id_futsal);
            $get = $this->db->get('tb_lapangan');
        }
        return $get;
    }
    public function count_jam($id)
    {
        $this->db->select_sum('lama_booking');
        $this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
        $this->db->where('tb_booking.id_lapangan', $id);
        $this->db->where('tb_booking.status_booking', 'Booked');
        $get = $this->db->get('tb_booking');
        if ($get->num_rows() > 0) {
            return $get->row();
        } else {
            return false;
        }
    }
    public function memberBooking()
    {
        $this->db->select('nama_member, sum(lama_booking) as jml, tb_member.email');
        $this->db->join('tb_member', 'tb_member.id_member = tb_booking.id_member', 'inner');
        $this->db->group_by('tb_booking.id_member');
        $this->db->order_by('jml', 'desc');
        $get = $this->db->get('tb_booking');
        if ($get->num_rows() > 0) {
            return $get;
        } else {
            return false;
        }
    }
}

/* End of file Chart_model.php */
