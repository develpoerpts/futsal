<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_model_login extends CI_model
{

	public function getlogin($u, $p)
	{
		$pwd = md5($p);
		$where = array(
			'username' => $u,
			'password' => $pwd
		);

		$query2 = $this->db->where($where)->get('tb_admin');
		$query3 = $this->db->where($where)->get('tb_futsal');

		if ($query2->num_rows() > 0) {
			foreach ($query2->result() as $row) {
				$sess = array(
					'logged' 		=> true,
					'username' 		 => $row->username,
					'nama'  			 => $row->nama,
					'id_admin' 		 => $row->id_admin,
					'tb_admin'	 		 => 'login'
				);
				$this->session->set_userdata($sess);
				redirect('Dashboard');
			}
		} elseif ($query3->num_rows() > 0) {
			foreach ($query3->result() as $row) {
				$sess = array(
					'id_futsal'		=> $row->id_futsal,
					'nama_futsal' 	=> $row->nama_futsal,
					'logged' 		=> true,
					// 'alamat'			=> $row->alamat,
					// 'keterangan'		=> $row->keterangan,
					'username'		=> $row->username,
					'password'		=> $row->password,
					// 'latitude'		=> $row->latitude,
					//  'longitude'		=> $row->longitude,
					//   'foto'		=> $row->foto,
					'tb_futsal'	 		 => 'login'
				);
				$this->session->set_userdata($sess);
				redirect('Admin_Futsal/Dashboard');
			}
		} else {
			$this->session->set_flashdata('error', 'Maaf Username atau Password salah');
			redirect('Login');
		}









		// if ($query->num_rows() > 0) {
		// 	if ($a == "tb_admin") {
		// 		foreach ($query->result() as $row) {
		// 			$sess = array(
		// 				'logged' 		=> true,
		// 				'username' 		 => $row->username,
		// 				'nama'  			 => $row->nama,
		// 				'id_admin' 		 => $row->id_admin,
		// 				'tb_admin'	 		 => 'login'
		// 			);
		// 			$this->session->set_userdata($sess);
		// 			redirect('Dashboard');
		// 		}
		// 	} else if ($a == "tb_futsal") {
		// 		foreach ($query->result() as $row) {
		// 			$sess = array(
		// 				'id_futsal'		=> $row->id_futsal,
		// 				'nama_futsal' 	=> $row->nama_futsal,
		// 				'logged' 		=> true,
		// 				// 'alamat'			=> $row->alamat,
		// 				// 'keterangan'		=> $row->keterangan,
		// 				'username'		=> $row->username,
		// 				'password'		=> $row->password,
		// 				// 'latitude'		=> $row->latitude,
		// 				//  'longitude'		=> $row->longitude,
		// 				//   'foto'		=> $row->foto,
		// 				'tb_futsal'	 		 => 'login'
		// 			);
		// 			$this->session->set_userdata($sess);
		// 			redirect('Admin_Futsal/Dashboard');
		// 		}
		// 	}
		// } else {
		// 	$this->session->set_flashdata('error', 'Maaf Username atau Password salah');
		// 	redirect('Login');
		// }
	}


	public function getloginMember($u, $p)
	{
		$pwd = md5($p);
		$this->db->where('email', $u);
		$this->db->where('password', $pwd);
		$this->db->where('status', 'Aktif');
		$query = $this->db->get('tb_member');
		if ($query->num_rows() > 0) {

			foreach ($query->result() as $row) {
				$sess = array(
					'logged' 		=> true,
					'email' 		 => $row->email,
					'password'  			 => $row->password,
				);
				$this->session->set_userdata($sess);
			}
		} else {
			$this->session->set_flashdata('error', 'Maaf Username atau Password salah');
			redirect('Welcome');
		}
	}
}
