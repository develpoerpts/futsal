<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cari extends CI_model {

	function __construct()
    {
        parent::__construct();
        $this->load->helper('My_helper');
    }

    function cari($keyword)
    {
        $this->db->like('nama_futsal',$keyword);
        $query  =   $this->db->get('tb_futsal');
        return $query->result();
    }
}
