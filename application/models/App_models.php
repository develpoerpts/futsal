<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App_models extends CI_Model
{

	public function __construct()
	{

		parent::__construct();
	}

	public function loginCheck($uName, $uPass)
	{
		$get = $this->db->where('username', $uName)->get('tb_admin');
		$row = $get->row();
		if ($get->num_rows() > 0) {
			if ($this->encryption->decrypt($row->password) == $uPass) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function cekUsernameFutsal($uName)
	{
		$getadmin = $this->db->where('username', $uName)->get('tb_admin');
		$get = $this->db->where('username', $uName)->get('tb_futsal');
		$row = $get->row();
		if ($getadmin->num_rows() > 0) {
			return true;
		} else {
			if ($get->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function get_data($table)
	{
		return $this->db->get($table);
	}

	public function get_data_turnamen()
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_turnamen.id_futsal', 'left');
		return $this->db->get('tb_turnamen');
	}

	public function get_dataId($table, $key, $val)
	{
		$this->db->where($key, $val);
		return $this->db->get($table);
	}


	public function deletData($table, $key, $val)
	{
		$this->db->where($key, $val);
		$this->db->delete($table);
		return true;
	}

	public function deletDataSmt($table, $key, $val, $data)
	{
		$this->db->where($key, $val);
		$this->db->update($table, $data);
		return true;
	}

	public function get_dataIdBooking($key, $val)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		return $this->db->get('tb_booking');
	}

	public function get_dataBookingbyId($key, $val)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->where($key, $val);
		return $this->db->get('tb_booking');
	}


	public function get_dataIdTurnamen($key, $val)
	{
		$this->db->join('tb_turnamen', 'tb_turnamen.id_turnamen = tb_daftar_turnamen.id_turnamen', 'left');
		$this->db->where($key, $val);
		return $this->db->get('tb_daftar_turnamen');
	}

	public function get_alldataDaftarTurnamen()
	{
		$this->db->join('tb_turnamen', 'tb_turnamen.id_turnamen = tb_daftar_turnamen.id_turnamen', 'left');
		return $this->db->get('tb_daftar_turnamen');
	}

	public function get_dataIdBooking_Id_futsal($key, $val)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		//	$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		return $this->db->get('tb_booking');
	}

	public function insert($table, $value)
	{
		$this->db->insert($table, $value);
		return true;
	}

	public function cekLogin($uname, $pass)
	{
		return $this->db->get_where('tb_member', array('email' => $uname, 'password' => $pass, 'status' => 'Aktif'));
	}

	public function update($table, $key, $val, $data)
	{
		$this->db->where($key, $val);
		$this->db->update($table, $data);
		return true;
	}

	public function update_table($table, $indx, $par, $data)
	{
		$this->db->where($indx, $par);
		$this->db->update($table, $data);
		$exc = $this->db->affected_rows();
		if ($exc > 0) {
			return true;
		} else {
			return false;
		}
	}


	public function get_lapangan()
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->where('tb_lapangan.status_lapangan', '0');
		return $this->db->get('tb_lapangan');
	}

	public function get_lapanganCari($keyword)
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->like('tb_futsal.nama_futsal', $keyword);
		$this->db->where('tb_lapangan.status_lapangan', '0');
		return $this->db->get('tb_lapangan');
	}


	public function get_lapangan_Id($val)
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->where('tb_lapangan.id_futsal', $val);
		$this->db->where('tb_lapangan.status_lapangan', '0');
		return $this->db->get('tb_lapangan');
	}

	public function get_lapangan_Id_rl($val)
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->where('tb_lapangan.id_futsal', $val);
		$this->db->where('tb_lapangan.status_lapangan', '1');
		return $this->db->get('tb_lapangan');
	}

	public function get_lapangan_Id_front($val)
	{
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->where('id_lapangan', $val);
		return $this->db->get('tb_lapangan');
	}

	public function get_booking()
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		return $this->db->get('tb_booking');
	}
	public function get_bookingfilter($start, $end)
	{
		$year = date('Y');
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where('month(tanggal_booking) >=', $start);
		$this->db->where('month(tanggal_booking) <=', $end);
		$this->db->where('year(tanggal_booking) >=', $year);

		return $this->db->get('tb_booking');
	}

	public function get_booking_Id_futsal($key, $val, $start, $end)
	{
		$year = date('Y');
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		//$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		$this->db->where('month(tanggal_booking) >=', $start);
		$this->db->where('month(tanggal_booking) <=', $end);
		$this->db->where('year(tanggal_booking) >=', $year);
		$this->db->where_not_in('status_booking', 'Closed');

		return $this->db->get('tb_booking');
	}

	public function get_booking_info($day, $monht, $year)
	{
		// $year = date('Y');
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		// $this->db->where($key, $val);
		$this->db->where('day(tanggal_booking) ', $day);
		$this->db->where('month(tanggal_booking) ', $monht);
		$this->db->where('year(tanggal_booking) ', $year);
		$this->db->where('status_booking', 'Booked');

		return $this->db->get('tb_booking');
	}
	public function get_booking_member()
	{
		$id_member = $this->session->userdata('member_sess')['id_member'];

		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where('tb_booking.id_member', $id_member);
		$this->db->order_by('tb_booking.tanggal_booking', 'desc');
		return $this->db->get('tb_booking');
	}
	public function get_booking_tamu()
	{
		$email = $this->session->userdata('tamu')['email'];

		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where('tb_booking.email', $email);
		$this->db->where('tb_booking.status_member', 'Non Member');
		$this->db->where('payment_stat !=', '2');
		$this->db->order_by('tb_booking.tanggal_booking', 'desc');
		return $this->db->get('tb_booking');
	}
	public function get_Notifmember()
	{
		$id_member = $this->session->userdata('member_sess')['id_member'];
		$this->db->where('tb_booking.id_member', $id_member);
		$this->db->where('status_booking', 'Pending');
		return $this->db->get('tb_booking')->num_rows();
	}
	public function get_Notiftamu()
	{
		$email = $this->session->userdata('tamu')['email'];
		$this->db->where('tb_booking.email', $email);
		$this->db->where('status_booking', 'Pending');
		$this->db->where('payment_stat', '0');
		return $this->db->get('tb_booking')->num_rows();
	}

	public function get_booking_Id_futsal_ds($key, $val)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		//$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		$this->db->where_not_in('status_booking', 'Closed');

		return $this->db->get('tb_booking');
	}

	public function get_booking_report_Id_futsal($key, $val, $start, $end)
	{
		$year = date('Y');
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		//$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		$this->db->where('status_booking', 'Closed');
		$this->db->where('month(tanggal_booking) >=', $start);
		$this->db->where('month(tanggal_booking) <=', $end);
		$this->db->where('year(tanggal_booking) >=', $year);
		return $this->db->get('tb_booking');
	}

	public function penghasilan($month, $tahun, $id_futsal, $id_lapangan)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where('MONTH(tanggal_booking)', $month);
		$this->db->where('YEAR(tanggal_booking)', $tahun);
		$this->db->where('tb_lapangan.id_futsal', $id_futsal);
		if ($id_lapangan !== '_all') {
			$this->db->where('tb_lapangan.id_lapangan', $id_lapangan);
		}

		$this->db->order_by('tb_lapangan.id_lapangan', 'ASC');

		$get = $this->db->get('tb_booking');
		if ($get->num_rows() > 0) {
			return $get;
		} else {
			return false;
		}
	}
	public function penghasilan_all($month, $tahun)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where('MONTH(tanggal_booking)', $month);
		$this->db->where('YEAR(tanggal_booking)', $tahun);
		// $this->db->where('tb_lapangan.id_futsal', $id_futsal);
		// if ($id_lapangan !== '_all') {
		// 	$this->db->where('tb_lapangan.id_lapangan', $id_lapangan);
		// }

		$this->db->order_by('tb_lapangan.id_lapangan', 'ASC');

		$get = $this->db->get('tb_booking');
		if ($get->num_rows() > 0) {
			return $get;
		} else {
			return false;
		}
	}

	public function get_booking_report_Id_futsal_ds($key, $val)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'left');
		$this->db->join('tb_jenis_lapangan', 'tb_jenis_lapangan.id_jenis = tb_lapangan.id_jenis', 'left');
		//$this->db->join('tb_futsal', 'tb_futsal.id_futsal = tb_lapangan.id_futsal', 'left');
		$this->db->where($key, $val);
		$this->db->where('status_booking', 'Closed');

		return $this->db->get('tb_booking');
	}
	public function check_booking($id_lapangan, $tanggal, $start, $end)
	{
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'inner');
		$this->db->where('tb_booking.id_lapangan', $id_lapangan);
		$this->db->where('tanggal_booking', $tanggal);
		$this->db->where('jam_booking <=', $start);
		$this->db->where('selesai_booking >=', $start);
		$this->db->where('status_booking', 'Booked');
		// $this->db->where('selesai_booking <=', $end);


		$select = $this->db->get('tb_booking');
		$this->db->join('tb_lapangan', 'tb_lapangan.id_lapangan = tb_booking.id_lapangan', 'inner');
		$this->db->where('tb_booking.id_lapangan', $id_lapangan);
		$this->db->where('tanggal_booking', $tanggal);
		$this->db->where('selesai_booking >=', $end);
		$this->db->where('jam_booking <=', $end);
		$this->db->where('status_booking', 'Booked');
		$select2 = $this->db->get('tb_booking');
		if (($select->num_rows() > 0) || ($select2->num_rows() > 0)) {
			return false;
		} else {
			return true;
		}
	}
}

/* End of file App_models.php */
/* Location: ./application/models/App_models.php */
