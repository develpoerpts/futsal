<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets2/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets2/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets2/plugins/iCheck/square/blue.css">

  <script type="text/javascript">
    function cekform() {
      if (!$("#username").val()) {
        alert("maaf username tidak boleh kosong");
        $("#username").focus();
        return false;
      }

      if (!$("#password").val()) {
        alert("maaf password tidak boleh kosong");
        $("#password").focus();
        return false;
      }
    }
  </script>

  <script type="text/javascript">
    function ShowPassword() {
      if (document.getElementById("password").value != "") {
        document.getElementById("password").type = "text";
        document.getElementById("show").style.display = "none";
        document.getElementById("hide").style.display = "block";
      }
    }

    function HidePassword() {
      if (document.getElementById("password").type == "text") {
        document.getElementById("password").type = "password"
        document.getElementById("show").style.display = "block";
        document.getElementById("hide").style.display = "none";
      }
    }
  </script>

</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Admin BookingFut</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Silahkan Login</p>

      <!-- //form login -->
      <form method="POST" action="<?php echo base_url(); ?>Login/getlogin" onsubmit="return cekform();">
        <div class="form-group has-feedback">
          <input type="text" id="username" name="username" class="form-control" placeholder="Username">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" id="password" name="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <br>
          <!-- <input type=button id="show" value="Lihat Password" onclick="ShowPassword()">
          <input type=button style="display:none" id="hide" value="Tutup Password" onclick="HidePassword()"> -->
        </div>

        <?php if ($this->session->flashdata('error') == TRUE) : ?>
          <center>
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="glyphicon glyphicon-check"></i> Gagal!</h4>
              <?php echo $this->session->flashdata('error') ?>
            </div>
          </center>
        <?php endif; ?>

        <!-- <div class="form-groupnhas has-feedback">
          <select class="form-control" name="status" required>
            <option value="" selected disabled>Pilih Akses</option>
            <option value="tb_admin">Super Admin</option>
            <option value="tb_futsal">Admin Futsal</option>
          </select></br>
        </div> -->

        <div class="row">
          <div class="col-xs-8">
            <!-- <div class="checkbox icheck">
            <label>
              <a href="<?php echo base_url(); ?>Lupa_password"><font face="Arial Black">Lupa Password ?</font></a>
            </label>
          </div> -->
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>


      <!-- <a href="#">Lupa Password ?</a><br>
    <a href="register.html" class="text-center">Registrasi Member Baru</a> -->

    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url(); ?>assets2/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?php echo base_url(); ?>assets2/bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url(); ?>assets2/plugins/iCheck/icheck.min.js"></script>
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  </script>
</body>

</html>