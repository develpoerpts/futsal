<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>BookingFut | <?php if (!empty($table_header)) echo $table_header ?></title>

  <!-- Bootstrap -->
  <link href="<?= base_url('assets') ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?= base_url('assets') ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Select2 -->
  <link href="<?= base_url('assets') ?>/select2/dist/css/select2.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="<?= base_url('assets') ?>/nprogress/nprogress.css" rel="stylesheet">
  <!-- PNotify -->
  <!-- <link href="<?= base_url('assets') ?>/pnotify/dist/pnotify.css" rel="stylesheet">
  <link href="<?= base_url('assets') ?>/pnotify/dist/pnotify.buttons.css" rel="stylesheet"> -->
  <!-- Custom Theme Style -->
  <link href="<?= base_url('assets') ?>/build/css/custom.min.css" rel="stylesheet">
  <!-- Datatables -->
  <link href="<?= base_url('assets') ?>/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="#" class="site_title"><span>BookingFut</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="<?= base_url('assets2/dist') ?>/img/profil.png" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2><?= $this->session->userdata('nama'); ?></h2>
            </div>
            <div class="clearfix"></div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <?php $this->load->view('sidebar'); ?>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <!-- <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="image/user.png" alt=""><?= $this->session->userdata('nama'); ?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a href="<?= base_url('Login/logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
              </li>

            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3><?php if (!empty($header)) echo $header; ?></h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <!-- <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div> -->
              </div>
            </div>
          </div>

          <div class="clearfix"></div>
          <?php if (!empty($content)) $this->load->view($content); ?>

        </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
        <div class="pull-right">
          <!-- Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a> -->
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>

  <div id="delModal" class="modal fade">
    <div class="modal-dialog modal-confirm modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Modal title</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <strong>Anda Yakin?</strong>
          <p>Data yang dihapus tidak dapat dikembalikan</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
          <a type="button" class="btn btn-danger" href="#" id="delaction">Delete</a>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery -->
  <script src="<?= base_url('assets') ?>/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?= base_url('assets') ?>/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url('assets') ?>/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="<?= base_url('assets') ?>/nprogress/nprogress.js"></script>
  <!-- Datatables -->
  <script src="<?= base_url('assets') ?>/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?= base_url('assets') ?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <!-- PNotify -->
  <script src="<?= base_url('assets') ?>/pnotify/dist/pnotify.js"></script>
  <!-- <script src="<?= base_url('assets') ?>/pnotify/dist/pnotify.buttons.js"></script> -->
  <script src="<?= base_url('assets') ?>/pnotify/dist/pnotify.nonblock.js"></script>
  <!-- morris.js -->
  <script src="<?= base_url('assets') ?>/raphael/raphael.min.js"></script>
  <script src="<?= base_url('assets') ?>/morris.js/morris.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url('assets') ?>/select2/dist/js/select2.full.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="<?= base_url('assets') ?>/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="<?= base_url('assets') ?>/build/js/custom.js"></script>
  <script type="text/javascript" src="<?= base_url('assets') ?>/build/js/main.js"></script>
  <script type="text/javascript" src="<?= base_url('assets') ?>/build/js/del_modal.js"></script>
  <script type="text/javascript">
    $('#save_pengguna').on("click", function() {
      save_pengguna();
      // console.log('pengguna');
    });
    $(document).ready(function() {
      $('#id_futsal_lap').change(function() {
        var id = $('#id_futsal_lap').val();
        $.ajax({
          url: "<?php echo base_url(); ?>index.php/Laporan/getLapangan",
          method: "POST",
          data: {
            id: id
          },
          async: false,
          dataType: 'json',
          success: function(data) {
            var html = '';
            var i;
            for (i = 0; i < data.length; i++) {
              html += '<option value="' + data[i].id_lapangan + '">' + data[i].nama + '</option>';
            }
            $('#id_lapangan').html(html);

          }
        });
      });
      $('#delaction').click(function(e) {
        e.preventDefault();
        $.ajax({
          type: "post",
          url: $(this).attr('href'),
          // data: "data",
          dataType: "JSON",
          success: function(response) {
            if (response.success) {
              new PNotify({
                title: 'Success!',
                text: response.notif,
                type: 'success',
                styling: 'bootstrap3'
              });
              $('#delModal').modal('hide');
              reload_table();
            }
            if (response.error) {
              new PNotify({
                title: 'Failed!',
                text: response.notif,
                type: 'error',
                styling: 'bootstrap3'
              });
              $('#delModal').modal('hide');
              reload_table();
            }

          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            // alert(thrownError);
          },
        });

      });

    })

    function edit(id) {
      $('#saveBtn').attr('data-metode', 'update');
      edit_modal(id);
      // console.log(id)
    }

    function delet(id) {
      $('.modal-title').text('Hapus Data');
      var url = $('#delBtn').data('url') + id;
      console.log(url);
      $('#delModal').modal({
        backdrop: "static",
        show: true
      });
      $('#delaction').attr("href", url);
    }

    function modal() {
      remove_validation();
      $('#form_input')[0].reset();
      $('.modal-title').text('Tambah Data');
      $('input[type=hidden]').val('');
      $('#saveBtn').removeAttr('data-metode');
      $('#modal_form').modal({
        backdrop: "static",
        show: true
      });
    }
  </script>
</body>

</html>