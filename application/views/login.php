
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BookingFut | </title>

    <!-- Bootstrap -->
    <link href="<?=base_url('assets')?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url('assets')?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url('assets')?>/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <!-- <link href="<?=base_url('assets')?>/animate.css/animate.min.css" rel="stylesheet"> -->

    <!-- Custom Theme Style -->
    <link href="<?=base_url('assets')?>/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
        <?php if (!empty($this->session->flashdata('info'))) {
            echo ' <div id="alert"><div class="alert alert-warning alert-dismissible fade in info" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Maaf!</strong> Silahkan Login Terlebih Dahulu
                  </div></div>';
        }?>

          <section class="login_content">

            <form action="<?=base_url('Login/loginProgres')?>" method="POST" role="form" id="form_login">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" name="username" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="" />
              </div>
              <div>
              <button class="btn btn-default submit" id="login">Log in</button>
                <!-- <a class="btn btn-default submit" type="submit" href="#"></a> -->
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>

              <div class="clearfix"></div>
              <div id="alert"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw"></i> BookingFut</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
  <!-- jQuery -->
  <script src="<?=base_url('assets')?>/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?=base_url('assets')?>/bootstrap/dist/js/bootstrap.min.js"></script>
  <script>
  $(document).ready(function () {


  });
  $('#login').click(function (e) {
      e.preventDefault();
      var url = $('#form_login').attr('action')
      var dataForm = $('#form_login').serialize();
      var message = '';
      console.log(url)
      $.ajax({
          type: "post",
          url: url,
          data: dataForm,
          dataType: "JSON",
          success: function (response) {
              $('.alert').remove();
              if (response.loged==true) {
                message = '<div class="alert alert-success alert-dismissible fade in" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>'+
                    '</button>'+
                    '<strong>'+response.message+
                 '</div>';
                $('#alert').append(message);
                window.setTimeout(function(){
                window.location.href = response.url;

                    }, 600);

              }else{
                message = '<div class="alert alert-danger alert-dismissible fade in" role="alert">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>'+
                    '</button>'+
                    '<strong>'+response.message+
                 ' </div>';
                 $('#alert').append(message);
                // console.log(response.message);
                $('#form_login')[0].reset();
              }
              $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                  $("#alert").slideUp(500);
                })
          },
		  error: function(ts) { alert(ts.responseText) }
      });
  });
  </script>
</html>
