<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-user"></i></div>
      <div class="count"><?php echo $member ?></div>
      <h3>Member</h3>
      <p></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i></div>
      <div class="count"><?php echo $futsal ?></div>
      <h3>Tempat Futsal</h3>
      <p></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i></div>
      <div class="count"><?php echo $lapangan ?></div>
      <h3>Lapangan Futsal</h3>
      <p></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
      <div class="count"><?php echo $booking ?></div>
      <h3>Booking</h3>
      <p></p>
    </div>
  </div>
</div>
<div class="row">
  <!-- bar chart -->
  <div class="col-md-9 col-sm-9 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Penggunaan Lapangan </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="jam_graph" style="width:100%; height:280px; margin-bottom: 20px;" data-url="<?= base_url('Dashboard/getChart') ?>"></div>
      </div>
    </div>
  </div>
  <!-- /bar charts -->
  <!-- Top Member -->
  <div class="col-md-3 col-sm-3 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Top Members </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" style="width: auto; height: auto; overflow-y: auto;">
        <!-- <div id="graph_bar" style="width:100%; height:280px;"></div> -->
        <ul class="list-unstyled top_profiles scroll-view">
          <?php if (isset($top_member)) :
            foreach ($top_member as $key => $member) : ?>
              <li class="media event">
                <a class="pull-left border-aero profile_thumb">
                  <i class="fa fa-user aero"></i>
                </a>
                <div class="media-body">
                  <a class="title" href="#"><?= $member->nama_member ?></a>
                  <p><strong><?= $member->jml ?> </strong> Jam </p>
                  <p> <small><?= $member->email ?></small>
                  </p>
                </div>
              </li>
          <?php endforeach;
          endif ?>
        </ul>
      </div>
    </div>
  </div>
</div>