<div class="col-md-12 border" style="border: 1px solid #000000!important">
	<br>
	<div class="row">
		<?php if ($status_booking == 'Pending') : ?>
			<div class="col-md-12">
				<div class="col-md-12 border" style="background: #ccc;">
					<center>
						<h2>
							<font face="Arial Black">Tagihan pembayaran</font>
						</h2>
					</center>
				</div>
			</div>
		<?php endif ?>
		<?php if ($status_booking != 'Pending') : ?>
			<div class="col-md-12">
				<div class="col-md-12 border" style="background: #ccc;">
					<center>
						<h2>
							<font face="Arial Black">Detail Booking</font>
						</h2>
					</center>
				</div>
			</div>
		<?php endif ?>


		<br>
		<br>
		<br>
		<br>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Informasi Booking
				</div>
				<div class="panel-body">
					<div class="col-md-12">
						<div class="col-md-12">
							<h4>Total Harga : <font face="Arial Black" name="harga" id="harga" style="font-size: 20px">Rp. <?php echo nominal($total_harga); ?></font>
							</h4>
							<ul class="list-group list-group-unbordered">
								<li class="list-group-item">
									<a>Kode Booking</a> <b class="pull-right"><?php echo $no_booking; ?></b>
								</li>
								<li class="list-group-item">
									<a>Nama Booking</a> <b class="pull-right"><?php echo $nama_booking; ?></b>
								</li>
								<li class="list-group-item">
									<a>Tanggal Booking</a> <b class="pull-right"><?php echo $tanggal_booking; ?></b>
								</li>
								<li class="list-group-item">
									<a>Jam Booking</a> <b class="pull-right"><?php echo $jam_booking; ?></b>
								</li>

								<li class="list-group-item">
									<a>Jam Selesai</a> <b class="pull-right"><?php echo $jam_selesai; ?></b>
								</li>
								<li class="list-group-item">
									<a>Lama Booking</a> <b class="pull-right"><?php echo $lama_booking; ?> jam</b>
								</li>
								<li class="list-group-item">
									<a>Status Member</a> <b class="pull-right"><?php echo $status_member; ?></b>
								</li>
								<li class="list-group-item">
									<a>Status Booking</a> <b class="pull-right"><?php echo $status_booking; ?></b>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Informasi Tempat Futsal
				</div>
				<div class="panel-body panel-profile">

					<div class="col-md-12">
						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<a>Nama Tempat</a> <b class="pull-right"><?php echo $nama_futsal; ?></b>
							</li>
							<li class="list-group-item">
								<a>Alamat</a> <b class="pull-right"><?php echo $alamat; ?></b>
							</li>
							<li class="list-group-item">
								<a>Jenis Lapangan</a> <b class="pull-right"><?php echo $jenis; ?></b>
							</li>
						</ul>



					</div>
				</div>
			</div>
			<?php if ($status_booking != 'Pending') : ?>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<p>
								<a href="<?php echo base_url('Riwayat_Booking/cetak/' . $no_booking); ?>" class="btn btn-success">
									<i class="fa fa-print"></i> Cetak Invoice </a>
							</p>
						</div>
						<div class="col-md-6">
							<p>
								<a href="<?php echo base_url('Welcome'); ?>" class="btn btn-primary">
									<i class="fa fa-home"></i> Home </a>
							</p>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
	<br />
	<?php if ($status_booking == 'Pending') : ?>
		<div class="col-md-12">
			<h2>Silahkan transfer ke :</h2>
		</div>
		<div class="col-md-12">
			<div class="row" style="border: 1px solid #000000!important">
				<div class="col-md-6" style="border: 1px solid #000000!important">
					<div class="row">
						<div class="col-md-12">
							<font face="Arial Black">
								<h3><?php echo $no_rek; ?></h3>
							</font>
						</div>
						<div class="col-md-12">
							an <font face="Arial Black"><?php echo $nama_rek; ?></font>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="height: 100px; border: 1px solid #000000!important">
					<font face="Arial blue text-center">
						<h1><?php echo $nama_bank; ?></h1>
					</font>
				</div>
			</div>
		</div>

		<div class="col-md-12">


			<br>
			<br>
			<div class="col-md-5">
				<form id="form_input" enctype="multipart/form-data" method="POST" action="<?= base_url('Riwayat_Booking/save') ?>" class="form-horizontal form-label-left">
					<input type="hidden" name="id_booking" id="id_booking" value="<?php echo $id_booking ?>">
					<input type="hidden" name="no_booking" id="no_booking" value="<?php echo $no_booking ?>">


					<div class="flex-m flex-w p-b-10">
						<div class="s-text15 w-size15 t-center">
							Pemilik Rekening
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							<div class="col-sm-10">
								<input type="text" id="nama_rekening_booking" name="nama_rekening_booking" required="required" class="form-control col-md-7 col-xs-12" placeholder="Nama Rekening...">
							</div>
						</div>

					</div>

					<div class="flex-m flex-w p-b-10">
						<div class="s-text15 w-size15 t-center">
							Foto Struk
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							<div class="col-sm-10">
								<input style="margin:1%;" id="foto_transfer" name="foto_transfer" class="btn btn-default" type="file" value="Pilih" onchange="PreviewImagesp();" />
							</div>
						</div>
					</div>


					<div class="col-md-12">

						<a data-toggle="modal" data-target="#exampleModalKodeBooking" href="<?php echo base_url(); ?>frontend/user/C_riwayat_transaksi" class="btn btn-danger">BAYAR NANTI</a>
						&nbsp;
						<button type="submit" class="btn btn-primary">BAYAR</button>
					</div>
					<br />

				</form>
				<br>
				<br>
			</div>
		</div>
	<?php endif ?>
</div>