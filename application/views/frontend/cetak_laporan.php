<html>

<head>
    <title>Invoice</title>
</head>

<body>
    <table width="100%">
        <tr>
            <td width="20%" align="left">
                <img src="<?php echo $_SERVER['DOCUMENT_ROOT'] . "/futsal/assets/stample/" . $stempel->stampel ?>" width="80px" />
            </td>
            <td width="80%" align="left">
                <h3><?php echo $data->nama_futsal ?></h3><br><?= $data->alamat . ' Telephone : ' . $data->no_telepon ?>
            </td>
        </tr>
    </table>
    <hr> <br />
    <table width="100%">
        <tr>
            <td align="left" width="20%">Jenis Lapangan </td>
            <td width="80%"> : <?= $data->jenis ?></td>
        </tr>
        <tr>
            <td align="left">No Booking </td>
            <td> : <?= $data->no_booking ?></td>
        </tr>
        <tr>
            <td align="left">Nama Booking </td>
            <td> : <?= $data->nama_booking ?></td>
        </tr>
        <tr>
            <td align="left" valign="top">Tanggal Booking </td>
            <td> : <?= $data->tanggal_booking ?> </td>
        </tr>
        <tr>
            <td align="left" valign="top">Waktu Mulai </td>
            <td> : <?= $data->jam_booking ?> </td>
        </tr>

        <tr>
            <td align="left" valign="top">Waktu Selesai </td>
            <td> : <?= $data->selesai_booking ?> </td>
        </tr>
        <tr>
            <td align="left" valign="top">Status </td>
            <td> : <?= $data->status_member ?> </td>
        </tr>
        <tr>
            <td align="left" valign="top">Status Booking </td>
            <td> : <?= $data->status_booking ?> </td>
        </tr>
        <tr>
            <td align="left">Total Pembayaran </td>
            <td> : <?= $data->total_harga ?></td>
        </tr>
        <tr height="">
            <td align="right" valign="bottom" colspan="2"><br />
                <br /> <?= date('d F Y') ?><br />
                <br />
                <img src="<?php echo $_SERVER['DOCUMENT_ROOT'] . "/futsal/assets/stample/" . $stempel->stampel ?>" width="80px" />
                <br>
                <p class="text-center"><?= $data->nama_futsal ?></p>
            </td>
        </tr>


    </table>
</body>

</html>