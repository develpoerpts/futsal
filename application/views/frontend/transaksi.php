<div class="mail-w3agile">
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-primary">
        <div class="panel-body panel-profile">
          <h3 class="profile-username text-center">Jadilah Member Untuk Mendapatkan Banyak Keuntungan</h3>
          <p class="text-muted text-center"></p>
          <ul class="list-group list-group-unbordered">
            <b class="profile-username text-center">Potongan Harga Sewa Mencapai</b>
            <h1 class="profile-username text-center"><?= $diskon_member ?> %</h1>

            <!-- <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li> -->
          </ul>
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          Informasi Kontak Daftar Member
        </div>
        <div class="panel-body">
          <li class="list-group-item">
            <strong><i class="fa fa-bank margin-r-5"></i> Nama Bank = </strong><strong> BRI -> 1345 1234 1243 1232 </strong>
            <p class="text-muted text-center"> A/n Feri Arianto</p>
          </li>
          <br>
          <li class="list-group-item">
            <strong><i class="fa fa-money margin-r-5"></i> Biaya Pendaftaran</strong>
            <p class="text-muted"> Rp. 30.000</p>
          </li>
          <hr>
          <strong><i class="fa fa fa-info margin-r-5" text-center></i> Setelah Melakukan Transfer Daftarkan Diri Anda Dengan Menekan Tombol Di Bawah</strong>
          <p></p>
          <hr>

          <a href="<?php echo base_url(); ?>Registrasi" class="btn btn-primary">Daftar Member</a>
        </div>
      </div>
    </div>
    <div class="col-md-8">

      <?php
      $saiki = date("Y-m-d");
      ?>
      <div class="table-agile-info">
        <div class="panel panel-default">
          <div class="panel-heading">
            Input Data Booking
          </div>
          <div class="panel-body">
            <form id="form_input1" method="POST" action="<?= base_url('Transaksi/save') ?>" class="form-horizontal form-label-left">
              <input type="hidden" name="id_booking" id="id_booking" value="">
              <input type="hidden" name="id_lapangan" id="id_lapangan" value="<?php echo $id_lapangan ?>">
              <input type="hidden" name="harga_non_member" id="harga_non_member" onkeyup="sum();" value="<?php echo $harga_non_member ?>">
              <div class="wrap-pic-w">
                <img style="width: 250px; height: 174px;" class="border" src="<?php echo base_url('assets/lapangan/') . '' . $foto_lapangan ?>" />
              </div>

              <div class="form-group">

                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_booking"><?php echo $nama_futsal; ?><span class="required"></span>
                </label>
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_booking"><?php echo $jenis; ?><span class="required"></span>
                </label>

              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_booking">Kode Booking <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-9 col-xs-12">
                  <input type="text" id="no_booking" name="no_booking" value="<?= $kode ?>" required="required" class="form-control col-md-7 col-xs-12" readonly>
                  <label>Catat Kode Booking Baik Baik <span class="required">*</span>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_booking">Nama Pemesan <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="nama_booking" name="nama_booking" data-validation="<?= base_url('Validation_form/nama') ?>" required="required" class="form-control col-md-4 col-xs-12" placeholder="Nama Pemesan...">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp_booking">No Telephone <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="no_hp_booking" name="no_hp_booking" data-validation="<?= base_url('Validation_form/no_hp') ?>" required="required" class="form-control col-md-4 col-xs-12" placeholder="Nomer Telephone...">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="email" id="email" name="email" data-validation="<?= base_url('Validation_form/email') ?>" required="required" class="form-control col-md-4 col-xs-12" placeholder="Alamat Email...">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_booking">Tanggal Booking <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="date" min="<?= $saiki ?>" id="tanggal_booking" name="tanggal_booking" required="required" class="form-control col-md-4 col-xs-12" placeholder="Tanggal Booking...">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lama_booking">Lama Boking (Jam) <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="number" min="1" max="9" id="lama_booking" onchange="sum();" name="lama_booking" required="required" class="form-control col-md-4 col-xs-12" placeholder="Lama Booking...">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jam_booking">Waktu Mulai <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="time" min="" id="jam_booking" name="jam_booking" data-url="<?= base_url('Transaksi/visiblecheck') ?>" required="required" class="form-control col-md-3 col-xs-6" placeholder="Waktu...">
                  <span class="text-danger" style="display:none ;" id="span-booking"> Lapangan tidak tersedia</span>
                </div>
              </div>
              <!-- <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jam_booking">Waktu Selesai <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="time" id="jam_selesai" name="jam_selesai" required="required" class="form-control col-md-4 col-xs-12" readonly>
                </div>
              </div> -->
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_harga">Total Pembayaran <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="total_harga" name="total_harga" required="required" class="form-control col-md-4 col-xs-12" readonly>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-sm-1 col-xs-12 col-md-offset-1">
                  <a href="<?php echo base_url(); ?>Welcome" class="btn btn-danger">Cancel</a>
                  <a href="<?php echo base_url(); ?>Transaksi/bookingMember/<?php echo $id_lapangan ?>" class="btn btn-primary" id="btn-member">Booking as a Member</a>
                  <button type="button" class="btn btn-success" id="btn-submit" data-url="<?= base_url('Transaksi/save') ?>">Booking Now</button>
                  <a href="<?php echo base_url(); ?>DetailLapangan/detail/<?php echo $id_lapangan ?>" class="btn btn-warning">Lihat Info Lapangan</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function sum() {
    var txtFirstNumberValue = document.getElementById('harga_non_member').value;
    var txtSecondNumberValue = document.getElementById('lama_booking').value;
    // var txtThirdNumberValue  = document.getElementById('txt3').value;
    //	  var txtThirdNumberValue  = document.getElementById('kota').value;
    var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
    if (!isNaN(result)) {

      // document.getElementById('txt4').value = result;
      document.getElementById('total_harga').value = result;
    }
  }

  $("[type='number']").keypress(function(evt) {
    evt.preventDefault();
  });
</script>