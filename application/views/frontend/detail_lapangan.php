<!-- Product Detail -->
<div class="container bgwhite p-t-35 p-b-80">
	<div class="flex-w flex-sb">
		<div class="w-size13 p-t-30 respon5">
			<div class="wrap-slick3 flex-sb flex-w">
				<div class="wrap-slick3-dots"></div>
				<div class="wrap-pic-w">
					<img style="width: 600px; height: 450px" class="border" src="<?php echo base_url('assets/lapangan/') . '' . $foto_lapangan ?>" />
				</div>
			</div>
		</div>

		<div class="w-size14 p-t-30 respon5">
			<h4 class="product-detail-name m-text16 p-b-13" style="font-weight: bold; font-family: Arial Black;">
				<?php echo $nama_futsal; ?>
			</h4>
			<h6 class="product-detail-name m-text16 p-b-13" style="font-weight: bold; font-family: Arial Blue;">
				<?php echo $no_telepon; ?>
			</h6>
			<h6 class="product-detail-name m-text16 p-b-13" style="font-weight: bold; font-family: Arial Blue;">
				<?php echo $jenis; ?>
			</h6>
			<h5 class="product-detail-name m-text16 p-b-13" style="font-weight: small; font-family: Arial Yellow;">
				<?php echo $alamat; ?>
			</h5>

			<span>-------------------------------------------------------------------------------------------------------</span>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<h4 style="font-weight: bold;">Sewa (/jam)</h4>
					</div>

					<div class="col-md-6">
						<h4>: Rp. <?php echo nominal($harga_non_member); ?></h4>
					</div>

					<div class="col-md-6">
						<h4 style="font-weight: bold;">Ukuran Lapangan</h4>
					</div>

					<div class="col-md-6">
						<h4>: <?php echo $ukuran_lapangan; ?></h4>
					</div>

					<div class="col-md-6">
						<h4 style="font-weight: bold;">Info Lainnya</h4>
					</div>

					<div class="col-md-6">
						<h4>: <?php echo $info_lainnya; ?></h4>
					</div>

					<div class="col-md-6">
						<h4 style="font-weight: bold;">Diskon Member</h4>
					</div>

					<div class="col-md-6">
						<h4>: <?php echo $diskon_member; ?> %</h4>
					</div>

					<div class="col-md-6">
						<h4 style="font-weight: bold;">Fasilitas</h4>
					</div>

					<div class="col-md-6">
						<h4>: <?php echo $fasilitas; ?></h4>
					</div>

					<div class="col-md-6">
						<h4 style="font-weight: bold;">Keterangan</h4>
					</div>

					<div class="col-md-6">
						<h4>: <?php echo $keterangan; ?></h4>
					</div>


				</div>

				<!--  -->
				<div class="p-t-33 p-b-60">
					<center><a style="height: 40px; width: 480px; font-family: Arial" href="<?php echo base_url(); ?>Transaksi/booking/<?php echo $id_lapangan; ?>" class="btn btn-success">BOOKING NOW</a></center>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-4">
			<div id="datepicker" data-date="<?= date('Y-m-d') ?>">
			</div>
			<input type="hidden" id="date_hidden" value="<?= date('Y-m-d') ?>">
		</div>
		<div class="col-md-8">
			<center>
				<h1>Informasi Booking</h1>
			</center>
			<div class="table-responsive">
				<table id="info-booking" data-url="<?= base_url('DetailLapangan/getInfobooking') ?>" class="table table-striped table-bordered" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No Booking</th>
							<th>Tanggal </th>
							<th>Mulai</th>
							<th>Selesai</th>
							<th>Lapangan</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>