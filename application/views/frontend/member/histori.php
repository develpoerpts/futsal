<div class="table-responsive">
    <table id="info-booking" data-url="<?= base_url('Member_front/getInfobooking') ?>" class="table table-striped" style="width: 100%;">
        <thead>
            <tr>
                <th>No</th>
                <th>No Booking</th>
                <th>Lapangan</th>
                <th>Tanggal </th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>