<!DOCTYPE html>
<html lang="en">

<head>
  <title>BookingFut</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="<?= base_url('assetsfront/') ?>images/icons/favicon.png" />
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>fonts/themify/themify-icons.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>fonts/elegant-font/html-css/style.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/slick/slick.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/noui/nouislider.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
  <!-- Datatables -->
  <link href="<?= base_url('assets') ?>/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>css/util.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>css/main.css">
  <!--===============================================================================================-->

  <link rel="stylesheet" type="text/css" href="<?= base_url('assetsfront/') ?>css/bootstrap.css">
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>js/jQuery-2.1.4.min.js"></script>


  <style type="text/css">
    .form {
      margin: 250px auto;
      width: 600px;
      padding: 10px;
      object-position: center;
    }

    .rating {
      position: absolute;
      transform: rotateY(180deg);
      display: flex;
    }

    .rating input {
      display: none;
    }

    .rating label {
      display: block;
      cursor: pointer;
      width: 30px;

    }

    .rating label:before {
      content: '\f005';
      font-family: fontAwesome;
      position: relative;
      display: block;
      font-size: 20px;
      color: #ccc;

    }

    .rating label:after {
      content: '\f005';
      font-family: fontAwesome;
      position: absolute;
      display: block;
      font-size: 20px;
      color: yellow;
      top: 0;
      opacity: 0;
      transition: .5s;
      text-shadow: 0 2px 5px rgba(0, 0, 0, .5);
    }

    .rating label:hover:after,
    .rating label:hover~label:after,
    .rating input:checked~label:after {
      opacity: 1;
    }

    .rating2 {
      position: absolute;
      transform: rotateY(180deg);
      display: flex;
    }

    .rating2 input {
      display: none;
    }

    .rating2 label {
      display: block;
      cursor: pointer;
      width: 30px;

    }

    .rating2 label:before {
      content: '\f005';
      font-family: fontAwesome;
      position: relative;
      display: block;
      font-size: 20px;
      color: #ccc;

    }

    .rating2 label:after {
      content: '\f005';
      font-family: fontAwesome;
      position: absolute;
      display: block;
      font-size: 20px;
      color: yellow;
      top: 0;
      opacity: 0;
      transition: .5s;
      text-shadow: 0 2px 5px rgba(0, 0, 0, .5);
    }

    .rating2 label:hover:after,
    .rating2 label:hover~label:after,
    .rating2 input:checked~label:after {
      opacity: 1;
    }

    * {
      margin: 0;
      padding: 0;
    }

    img {
      max-width: 100%;
    }

    .cycle-slideshow {
      width: 100%;
      max-width: 1500px;
      display: block;
      position: relative;
      margin: 20px auto;
      overflow: hidden;
    }

    .cycle-prev,
    .cycle-next {
      font-size: 200%;
      color: #fff;
      display: block;
      position: absolute;
      top: 50%;
      z-index: 9999;
      cursor: pointer;
      margin-top: -16px;
    }

    .cycle-prev {
      left: 42px;
    }

    .cycle-next {
      right: 62px;
    }

    .cycle-pager {
      position: absolute;
      width: 100%;
      height: 10px;
      bottom: 10px;
      z-index: 9999;
      text-align: center;
    }

    .cycle-pager span {
      text-indent: 100%;
      top: 100px;
      width: 10px;
      height: 10px;
      display: inline-block;
      border: 1px solid #fff;
      border-radius: 50%;
      margin: 0 10px;
      white-space: nowrap;
      cursor: pointer;
    }

    .cycle-pager-active {
      background-color: #25a0b3;
    }

    .tulisan {
      color: white;
    }
  </style>
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>js/jquery.cycle2.min.js"></script>

</head>

<!-- <body class="animsition"> -->

<body class="animsition">

  <!-- Header -->
  <header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">
      <div class="topbar">
      </div>
      <div class="wrap_header">
        <!-- Logo -->
        <a href="http://localhost/futsal/Welcome" class="logo">
          <img src="<?= base_url('assetsfront/') ?>images/icons/logo2.png" alt="IMG-LOGO">
        </a>
        <div class="header-wrapicon2">
          <div class="form-group">
            <a href="<?php echo base_url('Home'); ?>">
              <font face="Arial Black">Home</font>
            </a>&nbsp;|&nbsp;
            <a data-toggle="modal" data-target="#exampleModal" href="<?php echo base_url(); ?>Riwayat_Booking/bayar">
              <font face="Arial Black">Check MyBooked</font>
            </a>&nbsp;&nbsp;
          </div>
        </div>
        <div class="col-md-5">
          <li>
            <form action="<?php echo base_url('Pencarian/cari'); ?>" method="post">
              <div class="row">
                <div class="col-md-8">
                  <input style="width: 350px" type="text" class="form-control flat" id="cari" name="cari" placeholder="Cari Tempat Futsal...">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
            </form>
          </li>
        </div>

        <div class="header-icons">
          <div class="header-wrapicon2">
            <div class="form-group">

              <?php if (isset($this->session->userdata('member_sess')['logged_member'])) : ?>
                <a href="<?= base_url('Member_front') ?>">
                  <font face="Arial Black"><?= $this->session->userdata('member_sess')['nama_member'] ?></font>
                  <?= (getNotif() > 0) ? ' <span class="badge badge-info right">' . getNotif() . '</span>' : ''; ?>

                </a>
              <?php elseif (isset($this->session->userdata('tamu')['email'])) : ?>
                <a href="<?= base_url('Member_front/Tamu') ?>">
                  <font face="Arial Black"><?= $this->session->userdata('tamu')['email'] ?></font>
                  <?= (getNotiftamu() > 0) ? ' <span class="badge badge-info right">' . getNotiftamu() . '</span>' : ''; ?>
                </a>&nbsp;|&nbsp;
              <?php else : ?>
                <a href="<?= base_url('Login_member') ?>">
                  <font face="Arial Black">Login</font>
                </a>&nbsp;|&nbsp;
                <a href="<?= base_url('Registrasi') ?>">
                  <font face="Arial Black">Daftar Member</font>
                </a>
              <?php endif ?>

              &nbsp;|&nbsp;
              <a href="<?= base_url('Cara_pemesanan') ?>">
                <font face="Arial Black"> Cara Pemesanan</font>
              </a>
            </div>
          </div>

        </div>
        <!-- Button trigger modal -->


        <!-- Menu -->

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Isi Kode Booking Anda</h4>
              </div>
              <form id="form_input" enctype="multipart/form-data" method="POST" action="<?= base_url('Transaksi/toRiwayat') ?>" class="form-horizontal form-label-left">
                <div class="modal-body">
                  <div class="col-md-12 col-sm-12">
                    <input type="text" id="no_booking" name="no_booking" required="required" class="form-control col-md-12 col-xs-12" placeholder="Kode Booking">
                  </div>
                </div>

                <div class="modal-footer">

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" type="submit">View MyBooked</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal fade" id="exampleModalKodeBooking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Simpan Kode Booking Anda</h4>
              </div>
              <form id="form_input" enctype="multipart/form-data" method="POST" action="<?= base_url('Transaksi/toRiwayat') ?>" class="form-horizontal form-label-left">
                <div class="modal-body">
                  <div class="col-md-12 col-sm-12">
                    <input type="text" id="no_booking" name="no_booking" required="required" class="form-control col-md-12 col-xs-12" value="<?php echo (isset($no_booking)) ? $no_booking : ''; ?>" readonly>
                  </div>
                </div>

                <div class="modal-footer">

                  <a href="<?= base_url('Welcome') ?>" type="button" class="btn btn-secondary">Close</a>

                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Header Icon -->
      </div>
    </div>


    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
      <!-- Logo moblie -->
      <a href="index.html" class="logo-mobile">
        <!-- <img src="<?= base_url('assetsfront/') ?>images/icons/logo.png" alt="IMG-LOGO"> -->
      </a>

      <!-- Button show menu -->
      <div class="btn-show-menu">
        <!-- Header Icon mobile -->
        <div class="header-icons-mobile">
          <a href="#" class="header-wrapicon1 dis-block">
            <img src="<?= base_url('assetsfront/') ?>images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
          </a>

          <span class="linedivide2"></span>

          <div class="header-wrapicon2">
            <img src="<?= base_url('assetsfront/') ?>images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
            <span class="header-icons-noti">0</span>

            <!-- Header cart noti -->

          </div>
        </div>

        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </div>
      </div>
    </div>

    <!-- Menu Mobile -->

  </header>

  <section class="bgwhite p-t-55 p-b-65">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <!--  -->

          <?php $this->load->view($content); ?>

          <!-- Pagination -->

        </div>
      </div>
    </div>
  </section>

  <!-- Title Page -->



  <!-- Footer -->
  <footer class="col-md-12" style="background: black;">
    <div class="flex-w p-b-30">

    </div>

  </footer>



  <!-- Back to top -->






  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/jquery/jquery-3.2.1.min.js"></script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/animsition/js/animsition.min.js"></script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect2')
    });
  </script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/daterangepicker/daterangepicker.js"></script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/slick/slick.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>js/slick-custom.js"></script>
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <!-- Datatables -->
  <script src="<?= base_url('assets') ?>/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?= base_url('assets') ?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/sweetalert/sweetalert.min.js"></script>
  <script type="text/javascript">
    $('.block2-btn-addcart').each(function() {
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function() {
        swal(nameProduct, "is added to cart !", "success");
      });
    });

    $('.block2-btn-addwishlist').each(function() {
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function() {
        swal(nameProduct, "is added to wishlist !", "success");
      });
    });
  </script>

  <!--===============================================================================================-->
  <!-- <script type="text/javascript" src="<?= base_url('assetsfront/') ?>vendor/noui/nouislider.min.js"></script> -->
  <script type="text/javascript">
    /*[ No ui ]
      ===========================================================*/
    // var filterBar = document.getElementById('filter-bar');

    // noUiSlider.create(filterBar, {
    //   start: [50, 200],
    //   connect: true,
    //   range: {
    //     'min': 50,
    //     'max': 200
    //   }
    // });

    //   var skipValues = [
    //     document.getElementById('value-lower'),
    //     document.getElementById('value-upper')
    //   ];

    //   filterBar.noUiSlider.on('update', function(values, handle) {
    //     skipValues[handle].innerHTML = Math.round(values[handle]);
    //   });
    // 
  </script>
  <script type="text/javascript">
    function cekform() {
      if (!$("#email").val()) {
        alert("Maaf Nama pemilik rekening tidak boleh kosong");
        $("#email").focus();
        return false;
      }
      if (!$("#password").val()) {
        alert("Maaf Nama pemilik rekening tidak boleh kosong");
        $("#password").focus();
        return false;
      }
    }
    $('#lama_booking').on('change, keyup', function() {
      lama = $('#lama_booking').val();
      tanggal = $('#tanggal_booking').val();
      waktu = $('#jam_booking').val();

      $.ajax({
        type: "POST",
        url: '<?= base_url('Transaksi/jumlahTime'); ?>',
        data: {
          'lama_booking': lama,
          'jam_booking': waktu
        },
        success: function() {
          console.log('YY')
        },
      });
    })
  </script>
  <!--===============================================================================================-->
  <script src="<?= base_url('assetsfront/') ?>js/main.js"></script>
  <script src="<?= base_url('assetsfront/') ?>js/costom.js"></script>

</body>

</html>