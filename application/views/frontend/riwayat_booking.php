
<div class="col-md-12 border" style="border: 1px solid #000000!important">
<br>
<div class="row">
<div class="col-md-12">
	<div class="col-md-12 border" style="background: #ccc;">
		<center><h2><font face="Arial Black">Tagihan pembayaran</font></h2></center>
	</div>
</div>
<br>
<br>
<br>
<br>
<div class="col-md-12">
	<div class="col-md-5">
		<h4>Total Harga :  <font face="Arial Black" name="harga" id="harga" style="font-size: 20px">Rp. <?php echo nominal($total_harga);?></font></h4>
		<h5>Kode unik transfer anda adalah : <font face="Arial Black"><?php echo $keuntungan;?></font></h5>
		<h5 style="color: red">Total Harga sudah Termasuk Ongkos Kirim</h5>
	</div>
</div>
</div>
<br/>

<div class="col-md-12">
<h2>Silahkan transfer ke :</h2>
</div>
<div class="col-md-12">
	<div class="row" style="border: 1px solid #000000!important">
			<div class="col-md-6" style="border: 1px solid #000000!important">
				<div class="row">
					<div class="col-md-12">
						<font face="Arial Black"><h3>1010-01-017971-34-4</h3></font>
					</div>
					<div class="col-md-12">
						an <font face="Arial Black">Risky Amallia</font>
					</div>
				</div>
			</div>
			<div class="col-md-6" style="height: 100px; border: 1px solid #000000!important">
				<img style="height: 90px; width: 250px;" src="<?php echo base_url();?>assets/images/BNI.png" alt="IMG-LOGO">
			</div>
	</div>
</div>

<div class="col-md-12">
<?php if($this->session->flashdata('gagal')== TRUE): ?>
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-close"></i> Gagal!</h4>
		<?php echo $this->session->flashdata('gagal') ?>
	</div>
<?php endif; ?>

<br>
<br>
	<div class="col-md-5">
		<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo base_url();?>frontend/user/C_riwayat_transaksi/proses_bayar" onsubmit="return cekform();">
			<input type="hidden" name="id_barang" value="<?php echo $id_barang ?>">
	        <input type="hidden" class="form-control" name="status_pembayaran" id="status_pembayaran" class="form-control" value="Dalam Proses">
	        <input type="hidden" name="jumlah_beli" value="<?php echo $jumlah_beli ?>">

	        <div class="flex-m flex-w p-b-10">
					<div class="s-text15 w-size15 t-center">
						Pemilik Rekening
					</div>

					<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
					    <div class="col-sm-10">
					        <input type="text" class="form-control" name="nama_rekening" id="nama_rekening" placeholder="Nama Pemilik Rekening..." class="form-control" value="<?php echo $nama_rekening?>">
					    </div>
					</div>
			</div>

			<div class="flex-m flex-w p-b-10">
					<div class="s-text15 w-size15 t-center">
						Foto Struk
					</div>

					<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
					    <div class="col-sm-10">
					        <input style="margin:1%;" id="img" name="img" class="btn btn-default" type="file" value="Pilih" onchange="PreviewImagesp();"/>
					    </div>
					</div>
			</div>
			<input type="hidden" name="id_transaksi" value="<?php echo $id_transaksi;?>">

	        <div class="col-md-12">
	        	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	        	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	        	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	        	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	        	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	        	<a data-toggle="modal" data-target="#exampleModalKodeBooking" class="btn btn-danger">BAYAR NANTI</a>
	        	&nbsp;
                <button type="submit" class="btn btn-primary">BAYAR</button>
	        </div>
	        <br/>

		</form>
		<br>
		<br>
	</div>
</div>
</div>
