<section class="content">
    <div class="row">
        <div class="col-md-3">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="card-title">Menu Member</h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item active">
                            <a href="#" class="nav-link">
                                <i class="fa fa-inbox"></i> Histori Booking
                                <?php if (isset($this->session->userdata('member_sess')['logged_member'])) : ?>
                                    <?= (getNotif() > 0) ? ' <span class="badge bg-danger float-right">' . getNotif() . '</span>' : ''; ?>
                                <?php else : ?>
                                    <?= (getNotiftamu() > 0) ? ' <span class="badge badge-info right">' . getNotiftamu() . '</span>' : ''; ?>
                                <?php endif ?>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-user-o"></i> Info Member
                            </a>
                        </li> -->
                        <li class="nav-item">
                            <a href="<?= base_url('Registrasi/logout') ?>" class="nav-link">
                                <i class="fa fa-sign-out"></i> Log Out
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-filter"></i> Junk
                                <span class="badge bg-warning float-right">65</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-trash-alt"></i> Trash
                            </a>
                        </li> -->
                    </ul>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><?= ($title) ? $title : '' ?></h3>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="alert alert-warning" role="alert">
                        Segera Selesaikan Pembayaran maksimal 1 Jam setelah Pemesanan ! <br>
                        <strong>Notice !</strong> Status Pemesanan Akan dicancel oleh sistem setalah 1 Jam
                    </div>
                    <?php (isset($content_member)) ? $this->load->view($content_member) : '';
                    ?>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>