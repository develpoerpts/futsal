<div class="cycle-slideshow">
  <span class="cycle-prev">&#9001;</span> <!-- Untuk membuat tanda panah di kiri slider -->
  <span class="cycle-next">&#9002;</span> <!-- Untuk membuat tanda panah di kanan slider -->
  <span class="cycle-pager"></span> <!-- Untuk membuat tanda bulat atau link pada slider -->
  <?php foreach ($data->result() as $row) : ?>
    <img src="<?php echo base_url('assets/lapangan/') . '' . $row->foto_lapangan ?>" style="width: 1200px; height: 400px;" alt="Gambar Pertama">
    <!-- <img src="<?= base_url('assetsfront/') ?>images/b.jpg" style="width: 1200px; height: 400px;" alt="Gambar Kedua">
 <img src="<?= base_url('assetsfront/') ?>images/c.jpg" style="width: 1200px; height: 400px;" alt="Gambar Ketiga">
 <img src="<?= base_url('assetsfront/') ?>images/4.jpg" style="width: 1200px; height: 400px;" alt="Gambar Keempat"> -->
  <?php endforeach; ?>
</div>

<!-- <section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?php echo base_url(); ?>assets/images/heading-pages-02.jpg);">

</section> -->
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <h1>Detail Tempat Futsal</h1>
    <div class="col-md-12">

      <div class="panel panel-primary">
        <div class="panel-heading">
          Informasi Tempat Futsal
        </div>
        <div class="panel-body">
          <li class="list-group-item">
            <h5>
              <font face=" Arial Black" style="font-weight: bold; color: red;"><?php echo $row->nama_futsal ?></font>
            </h5>
          </li>
          <br>
          <li class="list-group-item">
            <h5 class="product-detail-name m-text16 p-b-13" style="font-weight: small; font-family: Arial Yellow;">
              <?php echo $row->alamat; ?>
          </li>
          <li class="list-group-item">
            <h5 class="product-detail-name m-text16 p-b-13" style="font-weight: small; font-family: Arial Yellow;">
              <?php echo $row->no_telepon; ?>
          </li>
          <hr>
          <strong><i class="fa fa fa-info margin-r-5" text-center></i>Fasilitas : <?php echo $row->fasilitas ?></strong>
          <p></p>
          <hr>
          <hr>
          <strong><i class="fa fa fa-info margin-r-5" text-center></i>Keterangan : <?php echo $row->keterangan ?></strong>
          <p></p>
          <hr>



        </div>
      </div>
    </div>
    <?php foreach ($data->result() as $row) : ?>
      <div class="col-md-3" style="margin-top: 30px">
        <div class="col-md-12 border">
          <br>
          <center>
            <a href="<?php echo base_url(); ?>DetailLapangan/detail/<?php echo $row->id_lapangan; ?>"><img style="width: 250px; height: 175px;" class="thumbnail" src="<?php echo base_url('assets/lapangan/') . '' . $row->foto_lapangan ?>" /></a>
          </center>
          <div class="col-md-12">

            <a>
              <h5>
                <font face=" Arial Black" style=" color: grey;"><?php echo $row->jenis ?></font>
              </h5>
            </a>
            <a>
              <h5>Non Member <font face="Arial Black" style="color: #750303">Rp. <?php echo nominal($row->harga_non_member) ?></font>
              </h5>
            </a>
            <br>

          </div>
          <center>
            <a style="height: 40px; width: 220px; font-family: Arial" href="<?php echo base_url(); ?>Transaksi/booking/<?php echo $row->id_lapangan; ?>" class="btn btn-success">BOOKING NOW</a>
          </center>


          <br>
        </div>
      </div>
    <?php endforeach; ?>
  </div>