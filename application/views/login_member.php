<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BookingFut | </title>
    <!-- Bootstrap -->
    <link href="<?= base_url('assets') ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets') ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url('assets') ?>/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets') ?>/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <?php if (!empty($this->session->flashdata('failed'))) {
                    echo ' <div id="alert"><div class="alert alert-danger alert-dismissible fade in info" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Maaf!</strong> ' . $this->session->flashdata('failed') . '
                  </div></div>';
                } ?>

                <section class="login_content">

                    <form action="<?= base_url('Registrasi/loginProgres') ?>" method="POST" role="form" id="form_login">
                        <h1>Login Form</h1>
                        <div>
                            <input type="email" class="form-control" name="email" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                        </div>
                        <div>
                            <button class="btn btn-default" type="submit">Log in</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>
<!-- jQuery -->
<script src="<?= base_url('assets') ?>/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url('assets') ?>/bootstrap/dist/js/bootstrap.min.js"></script>

</html>