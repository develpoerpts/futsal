<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php if (!empty($table_header)) echo $table_header ?> <small>Member</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					<!-- DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
				</p>
				<!-- <div>
		    	<button id="" onclick="modal()" class="btn btn-primary">Add</button>
		    </div> -->
				<div class="table-responsive">
					<table id="tables" data-url="<?= base_url('Member/get_tables') ?>" class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Member</th>
								<th>Alamat Member</th>
								<th>No Telephone</th>
								<th>Email</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Member</h4>
			</div>
			<div class="modal-body">
				<form id="form_input" action="<?= base_url('Member/save') ?>" class="form-horizontal form-label-left">
					<input type="hidden" name="id_member" id="id_member" value="">
					<input type="hidden" name="metode" id="metode">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Member <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="nama_member" name="nama_member" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat_member">Alamat Member <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="alamat_member" name="alamat_member" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telephone">No Telephone <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="no_telephone" name="no_telephone" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_transfer">Foto Bukti Transfer </span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="foto_bukti_transfer_daftar" name="foto_bukti_transfer_daftar" type="file" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12" id="image">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status_booking">Status Member <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="status" id="status" class="form-control select2" style="width: 100%;">
								<option value="Aktif">-Aktif-</option>
								<option value="Non Aktif">-Non Aktif-</option>

							</select>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>

							<button type="button" class="btn btn-success" onclick="save_status()">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>