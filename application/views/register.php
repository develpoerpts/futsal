<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets2/') ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets2/') ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets2/') ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets2/') ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets2/') ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="register-page">
  <div class="register-box">
    <div class="register-box-body">
      <p class="login-box-msg">Register</p>
      <?php if (!empty($this->session->flashdata('error'))) { ?>
        <div class="form-group">
          <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        </div>
      <?php } ?>
      <?php if (!empty($this->session->flashdata('success'))) { ?>
        <div class="form-group">
          <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        </div>
      <?php } ?>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">
            <legend>Data Member </legend>
          </h3>
        </div>
        <div class="panel-body">
          <form name="basicform" id="basicform" method="post" action="<?php echo base_url('Registrasi/register') ?>" enctype="multipart/form-data">

            <input type="hidden" name="id" value="">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Nama Member" name="nama_member">
              <span class=" form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Alamat" name="alamat_member">
              <span class=" form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="email" class="form-control" placeholder="Email" id="email" name="email">
              <span class=" form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password" name="password">
              <span class=" form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="No Telephone" name="no_telephone">
              <span class=" form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label for="ijin">Foto Bukti Transfer Daftar Member</label>
              <input type="file" class="form-control" id="foto_bukti_transfer_daftar" name="foto_bukti_transfer_daftar" placeholder="Bukti Transfer">
              <span class="form-control-feedback"></span>
            </div>
        </div>
      </div>
      <div class="clearfix" style="height: 10px;clear: both;"></div>
      <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
          <button class="btn btn-primary " type="submit"> Daftar</button>
        </div>
      </div>

      <!-- </div> -->
      </form>
    </div>

  </div>
  </div>

  <!-- jQuery 3 -->
  <script src="<?php echo base_url('assets2/') ?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url('assets2/') ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url('assets2/') ?>plugins/iCheck/icheck.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

  <script type="text/javascript">
    jQuery().ready(function() {

      // validate form on keyup and submit
      var v = jQuery("#basicform").validate({
        rules: {
          nama_member: {
            required: true,
            minlength: 2,
            maxlength: 20
          },

          email: {
            required: true,
            minlength: 2,
            email: true,
            remote: {
              url: "<?= base_url('Registrasi/check_email') ?>",
              type: "post",
              data: {
                email: function() {
                  return $("#email").val();
                }
              }
            }

          },
          alamat_member: {
            required: true,
          },
          no_telephone: {
            required: true,
          },
          password: {
            required: true,

            foto_bukti_transfer_daftar: {
              required: true,
            }
          },
        },
        messages: {

          password: {
            required: "<p class='errors'>Masukkan Password</p>", //pesan ini muncul apabila email tidak dimasukkan

          },
          email: {
            required: "<p class='errors'>Masukkan Email</p>",
            remote: "<p class='errors'>Email Sudah Terdaftar</p>", //pesan ini muncul apabila email tidak dimasukkan

          }
        },
        errorElement: "em",
        errorPlacement: function(error, element) {
          // Add the `help-block` class to the error element
          error.addClass("help-block");

          // Add `has-feedback` class to the parent div.form-group
          // in order to add icons to inputs
          element.parents(".form-group").addClass("has-feedback");

          error.insertAfter(element);

        },
        success: function(label, element) {
          // Add the span element, if doesn't exists, and apply the icon classes to it.
          // if ( !$( element ).next( "span" )[ 0 ] ) {
          //     $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $('.form-group') );
          // }
        },
        highlight: function(element, errorClass, validClass) {
          $(element).parents(".form-group").addClass("has-error").removeClass("has-success");

        },
        unhighlight: function(element, errorClass, validClass) {
          $(element).parents(".form-group").addClass("has-success").removeClass("has-error");

        }

      });
    });
  </script>
</body>

</html>