<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php if (!empty($table_header)) echo $table_header ?> <small>Booking</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					<!-- DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
				</p>
				<div class="row clearfix">
					<form action="<?= base_url('Booking/filter/') ?>" class="col-md-8 form-horizontal form-label-left" method="post">
						<div class="form-group">
							<?php $bln = date('m'); ?>
							<div class="form-row form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Dari Bulan <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select id="start" name="start" class="form-control col-md-7 col-xs-12">
										<option value=""></option>
										<option value="1" <?php if ($bln == 1) echo "selected"; ?>>Januari</option>
										<option value="2" <?php if ($bln == 2) echo "selected"; ?>>Februari</option>
										<option value="3" <?php if ($bln == 3) echo "selected"; ?>>Maret</option>
										<option value="4" <?php if ($bln == 4) echo "selected"; ?>>April</option>
										<option value="5" <?php if ($bln == 5) echo "selected"; ?>>Mei</option>
										<option value="6" <?php if ($bln == 6) echo "selected"; ?>>Juni</option>
										<option value="7" <?php if ($bln == 7) echo "selected"; ?>>Juli</option>
										<option value="8" <?php if ($bln == 8) echo "selected"; ?>>Agustus</option>
										<option value="9" <?php if ($bln == 9) echo "selected"; ?>>September</option>
										<option value="10" <?php if ($bln == 10) echo "selected"; ?>>Oktober</option>
										<option value="11" <?php if ($bln == 11) echo "selected"; ?>>Nopember</option>
										<option value="12" <?php if ($bln == 12) echo "selected"; ?>>Desember</option>
									</select>
								</div>
							</div>
							<div class="form-row form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Sampai Bulan <span class="required">*</span>
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<select id="end" name="end" class="form-control col-md-7 col-xs-12">
										<option value=""></option>
										<option value="1" <?php if ($bln == 1) echo "selected"; ?>>Januari</option>
										<option value="2" <?php if ($bln == 2) echo "selected"; ?>>Februari</option>
										<option value="3" <?php if ($bln == 3) echo "selected"; ?>>Maret</option>
										<option value="4" <?php if ($bln == 4) echo "selected"; ?>>April</option>
										<option value="5" <?php if ($bln == 5) echo "selected"; ?>>Mei</option>
										<option value="6" <?php if ($bln == 6) echo "selected"; ?>>Juni</option>
										<option value="7" <?php if ($bln == 7) echo "selected"; ?>>Juli</option>
										<option value="8" <?php if ($bln == 8) echo "selected"; ?>>Agustus</option>
										<option value="9" <?php if ($bln == 9) echo "selected"; ?>>September</option>
										<option value="10" <?php if ($bln == 10) echo "selected"; ?>>Oktober</option>
										<option value="11" <?php if ($bln == 11) echo "selected"; ?>>Nopember</option>
										<option value="12" <?php if ($bln == 12) echo "selected"; ?>>Desember</option>
									</select>
								</div>
							</div>
							<div class="form-row form-group">
								<button type="button" class="bnt btn-primary" id="show-filter">Tampilkan</button>
							</div>

						</div>
					</form>
				</div>
				<div class="table-responsive">
					<table id="table-booking" data-url="<?= base_url('Admin_Futsal/Booking/get_tables') ?>" class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>No Booking</th>
								<th>Nama</th>
								<th>Tanggal </th>
								<th>Waktu</th>
								<th>Status</th>
								<th>Lapangan</th>
								<th>Total Pembayaran</th>
								<th>Status Pemesanan</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form id="form_input" action="<?= base_url('Admin_Futsal/Booking/save') ?>" class="form-horizontal form-label-left">
					<input type="hidden" name="id_booking" id="id_booking" value="">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_booking">No Booking <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="no_booking" name="no_booking" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_booking">Nama Pemesan <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="nama_booking" name="nama_booking" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hp_booking">No Telephone <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="no_hp_booking" name="no_hp_booking" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_booking">Tanggal Booking <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="tanggal_booking" name="tanggal_booking" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jam_booking">Waktu <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="jam_booking" name="jam_booking" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status_member">Status User <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="status_member" name="status_member" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis">Lapangan <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="jenis" name="jenis" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_harga">Total Pembayaran <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="total_harga" name="total_harga" required="required" class="form-control col-md-7 col-xs-12" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_transfer">Foto Bukti Transfer </span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="foto_transfer" name="foto_transfer" type="file" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12" id="image">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status_booking">Status Pemesanan <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="status_booking" id="status_booking" class="form-control select2" style="width: 100%;">
								<option value="Pending">-Pending-</option>
								<option value="Booked">-Booked-</option>
								<option value="Closed">-Closed-</option>
							</select>
						</div>
					</div>



					<!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">BKK <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="id_bkk" id="id_bkk" data-url="<?= base_url('Booking/get_bkk') ?>" class="form-control select2" style="width: 100%;">
			      	<option value=""> BKK </option>
			      	<?php foreach ($bkk as $key => $value) {
							echo '<option value="' . $value->id_bkk . '">' . $value->nama_bkk . '</option>';
						} ?>

                </select>
			    </div>
			  </div> -->

					<!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">Level <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="level" id="level" class="form-control select2" style="width: 100%;">
			      	<option value=""> -- Level --</option>
			      	<option value="1">-Booking-</option>
			      	<option value="2">-Booking BKK-</option>
                </select>
			    </div>
			  </div> -->
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>

							<button type="button" class="btn btn-success" onclick="save()">Save</button>
						</div>
					</div>
				</form>
			</div>
			<!-- <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="button" class="btn btn-primary">Save changes</button>
	    </div> -->

		</div>
	</div>
</div>