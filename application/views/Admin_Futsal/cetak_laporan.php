<!DOCTYPE>
<html>

<head>
  <title>Laporan</title>
  <style>
    table {
      border-collapse: collapse;
      width: 100%;
    }

    table th {
      border: 1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: left;
    }

    table td {
      border: 1px solid #000;
      padding: 3px;
      vertical-align: top;
    }
  </style>
</head>

<body>
  <!-- <p style="text-align: right; font-size: 12px;">Tgl Cetak : <?= date('d F, Y') ?></p> -->
  <p style="text-align: center; margin-bottom: 0; font-weight:bold;"> LAPORAN DATA CLOSED BOOK</p>
  <!-- <p style="text-align: center; margin-top: 0; font-weight:bold;"></p> -->
  <br>
  <table>
    <thead>
      <tr>
        <th>No</th>
        <!-- <th style="width: 20%">Gambar</th> -->
        <th>No Booking</th>
        <th>Nama Booking</th>
        <th>Tanggal Booking</th>
        <th>Jam Booking</th>
        <th>Lama Sewa</th>
        <th>Status Member</th>
        <th>Jenis Lapangan</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      foreach ($data->result() as $row) { ?>
        <tr>
          <td><?php echo $no++ ?></td>
          <!-- <td><img src="<?php echo base_url('image/logo/' . $row->gambar); ?>" class="profil-user-img img-responsive img-circle"></td> -->
          <td><?php echo $row->no_booking; ?></td>
          <td><?php echo $row->nama_booking; ?></td>
          <td><?php echo $row->tanggal_booking; ?></td>
          <td><?php echo $row->jam_booking; ?></td>
          <td><?php echo $row->lama_booking; ?></td>
          <td><?php echo $row->status_member; ?></td>
          <td><?php echo $row->jenis; ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</body>

</html>