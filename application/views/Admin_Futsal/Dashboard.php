<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-user"></i></div>
      <div class="count"><?php echo $member ?></div>
      <h3>Member</h3>
      <p></p>
    </div>
  </div>

  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i></div>
      <div class="count"><?php echo $lapangan ?></div>
      <h3>Lapangan Futsal</h3>
      <p></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i></div>
      <div class="count"><?php echo $report ?></div>
      <h3>Report Close Booking</h3>
      <p></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
      <div class="count"><?php echo $booking ?></div>
      <h3>Booking</h3>
      <p></p>
    </div>
  </div>
</div>
<div class="row">
  <!-- bar chart -->
  <div class="col-md-9 col-sm-9 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Penggunaan Lapangan </h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="jam_graph" style="width:100%; height:300px;" data-url="<?= base_url('Dashboard/getChart') ?>"></div>
      </div>
    </div>
  </div>
  <!-- /bar charts -->
</div>