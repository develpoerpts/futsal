<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?php if (!empty($table_header)) echo $table_header ?> <small>Lapangan</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
					<!-- DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
				</p>
				<div>
					<button id="" onclick="modal()" class="btn btn-primary">Add</button>
				</div>

				<div class="table-responsive">
					<table id="tables" data-url="<?= base_url('Admin_Futsal/Lapangan/get_tables') ?>" class="table table-striped table-bordered" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Jenis Lapangan</th>
								<th>Harga Non Member (Rp)</th>
								<th>Diskon Member (%)</th>
								<th>Ukuran Lapangan</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal" id="modal_form" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form id="form_input" action="<?= base_url('Admin_Futsal/Lapangan/save') ?>" class="form-horizontal form-label-left">
					<input type="hidden" name="id_lapangan" id="id_lapangan" value="">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_jenis">Jenis Lapangan <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="id_jenis" id="id_jenis" data-url="<?= base_url('Admin_Futsal/Lapangan/get_bkk') ?>" class="form-control select2" style="width: 100%;">
								<option value=""> Jenis Lapangan </option>
								<?php foreach ($jenis_lapangan as $key => $value) {
									echo '<option value="' . $value->id_jenis . '">' . $value->jenis . '</option>';
								} ?>

							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="harga_non_member">Harga Non Member <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="harga_non_member" name="harga_non_member" required="required" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon_member">Diskon Member <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="diskon_member" name="diskon_member" required="required" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ukuran_lapangan">Ukuran Lapangan</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="ukuran_lapangan" name="ukuran_lapangan" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="info_lainnya">Info Lainnya </span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="info_lainnya" name="info_lainnya" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_lapangan">Upload Photo </span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="foto_lapangan" name="foto_lapangan" type="file" class="form-control col-md-7 col-xs-12">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12" id="image">
						</div>
					</div>




					<!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">BKK <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="id_bkk" id="id_bkk" data-url="<?= base_url('Lapangan/get_bkk') ?>" class="form-control select2" style="width: 100%;">
			      	<option value=""> BKK </option>
			      	<?php foreach ($bkk as $key => $value) {
							echo '<option value="' . $value->id_bkk . '">' . $value->nama_bkk . '</option>';
						} ?>

                </select>
			    </div>
			  </div> -->

					<!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">Level <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="level" id="level" class="form-control select2" style="width: 100%;">
			      	<option value=""> -- Level --</option>
			      	<option value="1">-Lapangan-</option>
			      	<option value="2">-Lapangan BKK-</option>
                </select>
			    </div>
			  </div> -->
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
							<button class="btn btn-primary" type="reset">Reset</button>
							<button type="button" class="btn btn-success" onclick="save_lapangan()">Save</button>
						</div>
					</div>
				</form>
			</div>
			<!-- <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="button" class="btn btn-primary">Save changes</button>
	    </div> -->

		</div>
	</div>
</div>