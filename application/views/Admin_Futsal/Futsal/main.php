<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
		  <div class="x_title">
		    <h2><?php if(!empty($table_header)) echo $table_header ?> <small>Futsal</small></h2>
		    <ul class="nav navbar-right panel_toolbox">
		      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		      </li>
		      <li><a class="close-link"><i class="fa fa-close"></i></a>
		      </li>
		    </ul>
		    <div class="clearfix"></div>
		  </div>
		  <div class="x_content">
		    <p class="text-muted font-13 m-b-30">
		      <!-- DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
		    </p>

		    <div class="table-responsive">
		    <table id="tables" data-url="<?=base_url('Admin_Futsal/Futsal/get_tables')?>" class="table table-striped table-bordered" width="100%">
		      <thead>
		        <tr>
		          <th>No</th>
		          <th>Nama Tempat Futsal</th>
		          <th>Alamat</th>
		          <th>Keterangan</th>
							<th>Nama Bank</th>
						  <th>No Rekening</th>
						  <th>No Telepon</th>
		          <th></th>
		        </tr>
		      </thead>
		      <tbody>
		      </tbody>
		    </table>
		</div>
		  </div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">Modal title</h4>
	    </div>
	    <div class="modal-body">
	    	<form id="form_input" action="<?=base_url('Admin_Futsal/Futsal/save')?>" class="form-horizontal form-label-left">
	    		<input type="hidden" name="id_futsal" id="id_futsal" value="">
			  <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Tempat <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <input type="text" id="nama_futsal" name="nama_futsal" required="required" class="form-control col-md-7 col-xs-12">
			    </div>
			  </div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="alamat" name="alamat" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="keterangan" name="keterangan" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fasilitas">Fasilitas <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
					 <textarea type="text" id="fasilitas" name="fasilitas" required="required" rows="8" class="form-control col-md-7 col-xs-12"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_bank">Nama Bank <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="nama_bank" name="nama_bank" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_rek">No Rekening <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="no_rek" name="no_rek" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="username" name="username" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">No Telepon <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="no_telepon" name="no_telepon" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div>
				<!-- <div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
					</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
					</div>
				</div> -->


			  <!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">BKK <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="id_bkk" id="id_bkk" data-url="<?=base_url('Futsal/get_bkk')?>" class="form-control select2" style="width: 100%;">
			      	<option value=""> BKK </option>
			      	<?php foreach ($bkk as $key => $value) {
			      		echo '<option value="'.$value->id_bkk.'">'.$value->nama_bkk.'</option>';
			      	} ?>

                </select>
			    </div>
			  </div> -->

			  <!-- <div class="form-group">
			    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_bkk">Level <span class="required">*</span>
			    </label>
			    <div class="col-md-6 col-sm-6 col-xs-12">
			      <select name="level" id="level" class="form-control select2" style="width: 100%;">
			      	<option value=""> -- Level --</option>
			      	<option value="1">-Futsal-</option>
			      	<option value="2">-Futsal BKK-</option>
                </select>
			    </div>
			  </div> -->
			  <div class="ln_solid"></div>
			  <div class="form-group">
			    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			      <button class="btn btn-primary" name="btnCancel" id="btnCancel" type="button" data-dismiss="modal">Cancel</button>
				  <button class="btn btn-primary" name="btnResetFutsal" id="btnResetFutsal" type="reset">Reset</button>
			     <button type="button" class="btn btn-success" name="btnSaveFutsal" id="btnSaveFutsal" onclick="save()">Save</button>
			    </div>
			  </div>
			</form>
	    </div>
	    <!-- <div class="modal-footer">
	      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      <button type="button" class="btn btn-primary">Save changes</button>
	    </div> -->

	  </div>
	</div>
</div>
