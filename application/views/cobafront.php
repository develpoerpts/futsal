
<!DOCTYPE html>
<html lang="en">
<head>
  <title>BookingFut</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
  <link rel="icon" type="image/png" href="<?=base_url('assetsfront/')?>images/icons/favicon.png"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>fonts/themify/themify-icons.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>fonts/elegant-font/html-css/style.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/animate/animate.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/select2/select2.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/slick/slick.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>css/util.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>css/main.css">
<!--===============================================================================================-->

  <link rel="stylesheet" type="text/css" href="<?=base_url('assetsfront/')?>css/bootstrap.css">
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>js/jQuery-2.1.4.min.js"></script>
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>js/jquery.cycle2.min.js"></script>


  <style type="text/css">
    .form {
        margin: 250px auto;
        width: 600px;
        padding: 10px;
        object-position: center;
    }
    .rating{
      position: absolute;
      transform: rotateY(180deg);
      display: flex;
    }
    .rating input{
      display: none;
    }
    .rating label{
      display: block;
      cursor: pointer;
      width: 30px;

    }
    .rating label:before{
      content: '\f005';
      font-family: fontAwesome;
      position: relative;
      display: block;
      font-size: 20px;
      color: #ccc;

    }
    .rating label:after{
      content: '\f005';
      font-family: fontAwesome;
      position: absolute;
      display: block;
      font-size: 20px;
      color: yellow;
      top: 0;
      opacity: 0;
      transition: .5s;
      text-shadow: 0 2px 5px rgba(0,0,0,.5);
    }

    .rating label:hover:after,
    .rating label:hover ~ label:after,
    .rating input:checked ~  label:after
    {
      opacity: 1;
    }

    .rating2{
      position: absolute;
      transform: rotateY(180deg);
      display: flex;
    }
    .rating2 input{
      display: none;
    }
    .rating2 label{
      display: block;
      cursor: pointer;
      width: 30px;

    }
    .rating2 label:before{
      content: '\f005';
      font-family: fontAwesome;
      position: relative;
      display: block;
      font-size: 20px;
      color: #ccc;

    }
    .rating2 label:after{
      content: '\f005';
      font-family: fontAwesome;
      position: absolute;
      display: block;
      font-size: 20px;
      color: yellow;
      top: 0;
      opacity: 0;
      transition: .5s;
      text-shadow: 0 2px 5px rgba(0,0,0,.5);
    }

    .rating2 label:hover:after,
    .rating2 label:hover ~ label:after,
    .rating2 input:checked ~  label:after
    {
      opacity: 1;
    }

    * { margin: 0; padding: 0; }
 img { max-width: 100%; }
 .cycle-slideshow {
  width: 100%;
  max-width: 1500px;
  display: block;
  position: relative;
  margin: 20px auto;
  overflow: hidden;
 }
 .cycle-prev, .cycle-next {
  font-size: 200%;
  color: #fff;
  display: block;
  position: absolute;
  top: 50%;
  z-index: 9999;
  cursor: pointer;
  margin-top: -16px;
 }
 .cycle-prev { left: 42px; }
 .cycle-next { right: 62px; }
 .cycle-pager {
  position: absolute;
  width: 100%;
  height: 10px;
  bottom: 10px;
  z-index: 9999;
  text-align: center;
 }
 .cycle-pager span {
  text-indent: 100%;
  top: 100px;
  width: 10px;
  height: 10px;
  display: inline-block;
  border: 1px solid #fff;
  border-radius: 50%;
  margin: 0 10px;
  white-space: nowrap;
  cursor: pointer;
 }
 .cycle-pager-active { background-color: #25a0b3; }
 .tulisan{
  color: white;
 }

  </style>
</head>

<!-- <body class="animsition"> -->
<body class="animsition">

  <!-- Header -->
  <header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">
      <div class="topbar">





      </div>

      <div class="wrap_header">
        <!-- Logo -->
        <a href="http://localhost/besi/C_home_frontend" class="logo">
          <img src="<?=base_url('assetsfront/')?>images/icons/logo.png" alt="IMG-LOGO">
        </a>
        <!-- <p class="bold"><b>No Telepon :</b> 081388999876 &nbsp; &nbsp;
          <b>Email :</b> rpttegal@gmail.com &nbsp; &nbsp;
         <b>Alamat :</b> Jl. Projosumarto II, Dukuhmalang Talang, Tegal </p> -->


          <div class="header-wrapicon2">
            <div class="form-group">
            <a href="http://localhost/besi/C_home_frontend"><font face="Arial Black">Home</font></a>&nbsp;|&nbsp;
            <!-- <a href="http://localhost/besi/C_home_frontend"><font face="Arial Black">Kategori</font></a>&nbsp;|&nbsp;
 -->
            <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown"><font face="Arial Black">Kategori</font></a>&nbsp;|&nbsp;

            </a> -->
            <!-- <li class="dropdown"> -->
                <a href="" class="dropdown-toggle" data-toggle="dropdown"><span class=""></span><font face="Arial Black">Kategori</font></a>

                <ul class="dropdown-menu">
                  <li><a href="http://localhost/besi/Kategori/kategori_pipa"><span class=""></span> Pipa</a></li>
                      <li class="divider"></li>
                  <li><a href="http://localhost/besi/Kategori/kategori_plat"><span class=""></span> Plat</a></li>
                      <li class="divider"></li>
                  <li><a href="http://localhost/besi/Kategori/kategori_aksesoris"><span class=""></span>Aksesoris</a></li>
                      <li class="divider"></li>
                  <li><a href="http://localhost/besi/Kategori/kategori_ring"><span class=""></span> Ring</a></li>
                      <li class="divider"></li>
                  <li><a href="http://localhost/besi/Kategori/kategori_tanki"><span class=""></span> Tanki Oli</a></li>
                </ul>
                        <!-- end dropdown-menu -->
              <!-- </li>  -->
              &nbsp;|&nbsp;

            <a href="http://localhost/besi/C_home_frontend/profil_cv"><font face="Arial Black">Profil CV RPT</font></a>&nbsp;|&nbsp;
            <a href="http://localhost/besi/C_home_frontend/kontak"><font face="Arial Black">Kontak</font></a>&nbsp;&nbsp;
            </div>
          </div>


            <div class="col-md-5">
              <li>
                <form action="http://localhost/besi/Pencarian/cari" method="post">
                <div class="row">
                  <div class="col-md-8">
                    <input style="width: 350px" type="text" class="form-control flat" id="cari" name="cari" placeholder="Cari Barang...">
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                  </div>
                </div>
                </form>
              </li>
            </div>

        <!-- Menu -->


        <!-- Header Icon -->
        <div class="header-icons">
          <div class="header-wrapicon2">
            <div class="form-group">
            <a href="http://localhost/besi/frontend/C_login_user"><font face="Arial Black">Login</font></a>&nbsp;|&nbsp;
            <a href="http://localhost/besi/frontend/C_daftar_user"><font face="Arial Black">Daftar</font></a>&nbsp;|&nbsp;
            <a href="http://localhost/besi/C_home_frontend/carapemesanan"><font face="Arial Black"> Cara Pemesanan</font></a>
            </div>
          </div>

        </div>
      </div>
    </div>


    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
      <!-- Logo moblie -->
      <a href="index.html" class="logo-mobile">
        <img src="<?=base_url('assetsfront/')?>images/icons/logo.png" alt="IMG-LOGO">
      </a>

      <!-- Button show menu -->
      <div class="btn-show-menu">
        <!-- Header Icon mobile -->
        <div class="header-icons-mobile">
          <a href="#" class="header-wrapicon1 dis-block">
            <img src="<?=base_url('assetsfront/')?>images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
          </a>

          <span class="linedivide2"></span>

          <div class="header-wrapicon2">
            <img src="<?=base_url('assetsfront/')?>images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
            <span class="header-icons-noti">0</span>

            <!-- Header cart noti -->

          </div>
        </div>

        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </div>
      </div>
    </div>

    <!-- Menu Mobile -->

  </header>

  <!-- Title Page -->

  <!-- Content page -->
  <section class="bgwhite p-t-55 p-b-65">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <!--  -->


          <!-- Product -->

<div class="cycle-slideshow">
 <span class="cycle-prev">&#9001;</span> <!-- Untuk membuat tanda panah di kiri slider -->
 <span class="cycle-next">&#9002;</span> <!-- Untuk membuat tanda panah di kanan slider -->
 <span class="cycle-pager"></span>  <!-- Untuk membuat tanda bulat atau link pada slider -->
 <img src="<?=base_url('assetsfront/')?>images/a.jpg" style="width: 1200px; height: 400px;" alt="Gambar Pertama">
 <img src="<?=base_url('assetsfront/')?>images/b.jpg" style="width: 1200px; height: 400px;" alt="Gambar Kedua">
 <img src="<?=base_url('assetsfront/')?>images/c.jpg" style="width: 1200px; height: 400px;" alt="Gambar Ketiga">
 <img src="<?=base_url('assetsfront/')?>images/4.jpg" style="width: 1200px; height: 400px;" alt="Gambar Keempat">
</div>
<!-- <section class="bg-title-page p-t-50 p-b-40 flex-col-c-m" style="background-image: url(<?=base_url('assetsfront/')?>images/heading-pages-02.jpg);">

</section> -->
<br>
<br>

<div class="row">
	<div class="col-md-12">

				<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/22"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817160700.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Pipa 75 A</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Pipa</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 150.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/23"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817161000.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Handle </font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">plat</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 120.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/24"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817161419.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Klem UK 41-43</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Ring</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 875.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/25"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817162827.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Pipa 75 B</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Pipa</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 100.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/26"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817172526.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Ring 2mm</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Ring</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 115.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/27"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817163421.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Saring Tanki</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Tanki Oli</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 625.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/28"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817165630.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Body Hinge</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">plat</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 450.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/29"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817170154.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Tanki oil </font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Tanki Oli</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 550.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/30"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817170549.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Tanki Oil Bulat</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Tanki Oli</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 375.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/31"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817172452.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Padock</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Pipa</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 175.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/32"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817172159.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Karet Handle</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Aksesoris</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 150.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/33"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180817172828.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">Pipa Sambung Oil</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Pipa</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 220.000</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
			<div class="col-md-3" style="margin-top: 30px">
			<div class="col-md-12 border">
				<br>

				<center>
				<a href="http://localhost/besi/frontend/C_detail_barang/detail/34"><img style="width: 250px; height: 250px;" class="thumbnail" src="<?=base_url('assetsfront/')?>upload/20180830122357.jpg"/></a>
				</center>
				<div class="col-md-12">
					<a href="#" class="block2-name dis-block s-text3 p-b-5">
						<h5><font face=" Arial Black" style="font-weight: bold; color: red;">hhjh</font></h5>
					</a>

					<a>
						<h7>Kategori : <font face=" Arial Black" style=" color: grey;">Tanki Oli</font></h7>
					</a>

					<h4>
						<font face="Arial Black" style="color: #750303">Rp. 676.767</font>
					</h4>

					<h6>
						<!-- <font face="Arial Black">Stok 200 pcs</font> -->
					</h6>

					<br>

				</div>
				<center>
				<a style="height: 40px; width: 220px; font-family: Arial" href="http://localhost/besi/frontend/auth/C_login_user" class="btn btn-success">BELI SEKARANG</a>
			</center>
			<br>
			</div>

		</div>
</div>
</div>

          <!-- Pagination -->

        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="col-md-12" style="background: black;">
    <div class="flex-w p-b-30">

    </div>

  </footer>



  <!-- Back to top -->






<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/bootstrap/js/popper.js"></script>
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/select2/select2.min.js"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect2')
    });
  </script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/slick/slick.min.js"></script>
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>js/slick-custom.js"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/sweetalert/sweetalert.min.js"></script>
  <script type="text/javascript">
    $('.block2-btn-addcart').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to cart !", "success");
      });
    });

    $('.block2-btn-addwishlist').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to wishlist !", "success");
      });
    });
  </script>

<!--===============================================================================================-->
  <script type="text/javascript" src="<?=base_url('assetsfront/')?>vendor/noui/nouislider.min.js"></script>
  <script type="text/javascript">
    /*[ No ui ]
      ===========================================================*/
      var filterBar = document.getElementById('filter-bar');

      noUiSlider.create(filterBar, {
          start: [ 50, 200 ],
          connect: true,
          range: {
              'min': 50,
              'max': 200
          }
      });

      var skipValues = [
      document.getElementById('value-lower'),
      document.getElementById('value-upper')
      ];

      filterBar.noUiSlider.on('update', function( values, handle ) {
          skipValues[handle].innerHTML = Math.round(values[handle]) ;
      });
  </script>
<!--===============================================================================================-->
  <script src="<?=base_url('assetsfront/')?>js/main.js"></script>

</body>
</html>
