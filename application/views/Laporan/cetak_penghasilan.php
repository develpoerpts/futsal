<!DOCTYPE>
<html>

<head>
    <title>Laporan</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table th {
            /* border: 1px solid #000; */
            padding: 3px;
            font-weight: bold;
            text-align: left;
        }

        table td {
            /* border: 1px solid #000; */
            padding: 3px;
            vertical-align: top;
        }
    </style>
</head>

<body>
    <!-- <p style="text-align: right; font-size: 12px;">Tgl Cetak : <?= date('d F, Y') ?></p> -->
    <p style="text-align: center; margin-bottom: 0; font-weight:bold;"> LAPORAN PENGHASILAN FUTSAL</p>
    <!-- <p style="text-align: center; margin-top: 0; font-weight:bold;"></p> -->
    <br>
    <?php if (isset($futsal)) : ?>
        <table border="0">
            <tr>
                <td width="100">Nama Futsal</td>
                <td>: <?php echo $futsal['futsal_name'] ?></td>
            </tr>
            <tr>
                <td>Bulan</td>
                <td>: <?php echo $futsal['bulan'] ?> </td>
            </tr>
            <tr>
                <td>Tahun</td>
                <td>: <?php echo $futsal['tahun'] ?></td>
            </tr>
        </table>
    <?php endif ?>
    <br>
    <table border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Lapangan</th>
                <th class="text-center">Jumlah (Jam)</th>
                <th>Nominal</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            $sum = 0;
            if ($data !== false) {
                foreach ($data->result() as $row) { ?>
                    <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo date('d F Y', strtotime($row->tanggal_booking)); ?></td>
                        <td><?php echo $row->nama_futsal . ' ' . $row->jenis; ?></td>
                        <td class="text-center"><?php echo $row->lama_booking; ?></td>
                        <td><?php echo $row->total_harga; ?></td>
                    </tr>
            <?php $sum += $row->total_harga;
                }
            } else {
                echo ' <tr class="text-center"><td colspan="5"><i>Tidak Ada Data </i></td> </tr>';
            } ?>
            <tr>
                <td colspan="4">Total</td>
                <td><?php echo $sum; ?></td>
            </tr>
        </tbody>
    </table>
</body>

</html>