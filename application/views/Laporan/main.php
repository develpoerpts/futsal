<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php if (!empty($table_header)) echo $table_header ?> <small>Lapangan</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p class="text-muted font-13 m-b-30">
                    <!-- DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
                </p>
                <form target="_blank" action="<?= base_url('Laporan/cetak') ?>" method="post" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="futsal">Futsal <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="id_futsal" id="id_futsal_lap" class="form-control">
                                <option value="" selected disabled> -- Pilih Tempat Futsal -- </option>
                                <?php foreach ($futsal as $key => $val) {
                                    echo '<option value="' . $val->id_futsal . '">' . $val->nama_futsal . '</option>';
                                } ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="futsal">Lapangan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="id_lapangan" id="id_lapangan" class="form-control">
                                <option value="" selected disabled> -- Pilih Lapangan -- </option>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="futsal">Bulan <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <select name="bulan" id="id_bulan" class="form-control" required>
                                <option value="" selected disabled> -- Pilih Bulan -- </option>
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="futsal">Tahun <span class="required">*</span>
                        </label>
                        <?php $thn = date('Y');
                        $strt = '2020'; ?>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <select name="tahun" id="tahun" class="form-control" required>
                                <option value="" selected disabled> -- Pilih Tahun -- </option>
                                <?php for ($strt = 2019; $strt <= $thn; $strt++) {
                                    echo ' <option value="' . $strt . '">' . $strt . '</option>';
                                } ?>

                            </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="futsal">
                        </label>
                        <div class="col-md-2 col-sm-2 col-xs-3">
                            <button type="submit" class="btn btn-success"> Cetak </button>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-3">
                            <button type="submit" name="cetak_all" class="btn btn-primary"> Cetak Semua </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>