<?php

function tampil_nama_barang($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('barang')
		->where('id_barang', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->nama_barang;
		return $stmt;
	}
}

function tampil_nama_user($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('transaksi')
		->where('id_transaksi', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->nama_user;
		return $stmt;
	}
}

function tampil_alamat_user($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('transaksi')
		->where('id_transaksi', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->alamat_user;
		return $stmt;
	}
}

function tampil_hp_user($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('transaksi')
		->where('id_transaksi', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->no_hp;
		return $stmt;
	}
}

function tampil_status_penerimaan($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pengiriman')
		->where('id_transaksi', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->status_penerimaan;
		return $stmt;
	}
}

function tampil_nama_peternak($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('peternak')
		->where('id_peternak', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->nama;
		return $stmt;
	}
}

function tampil_nama_pembeli($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pembeli')
		->where('id_pembeli', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->nama;
		return $stmt;
	}
}

function tampil_no_hp_pembeli($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pembeli')
		->where('id_pembeli', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->no_hp;
		return $stmt;
	}
}



function tampil_status_pengiriman($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pengiriman')
		->where('id_transaksi', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->status_pengiriman;
		return $stmt;
	}
}

function tampil_alamat_pembeli($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pembeli')
		->where('id_pembeli', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->alamat;
		return $stmt;
	}
}

function tampil_nama_peternakan2($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('peternakan')
		->where('id_peternakan', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->nama_peternakan;
		return $stmt;
	}
}

function tampil_rating_peternak($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('peternakan')
		->where('id_peternakan', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->jumlah_rating;
		return $stmt;
	}
}

function tampil_bukti_pengiriman($id)
{
	$ci = &get_instance();
	$ci->load->database();
	$result = $ci->db->select('*')
		->from('pengiriman')
		->where('id_pengiriman', $id)
		->get();
	foreach ($result->result() as $c) {
		$stmt = $c->bukti_pengiriman;
		return $stmt;
	}
}

function nominal($angka)
{
	$jd = number_format($angka, 0, ',', '.');
	return $jd;
}
function sendEmail($email, $theme, $message = array())
{
	$CI = &get_instance();
	$CI->load->library('email');
	$CI->email->from('info@bookingfut.com', 'Booking Futsal');
	$CI->email->to($email);
	if (!isset($message['subject'])) {
		$CI->email->subject('Subject Kosong');
	} else {
		$CI->email->subject($message['subject']);
	}
	$data = array();
	if (isset($message['data'])) {
		$data['data'] = $message['data'];
	}

	$body = $CI->load->view($theme, $data, TRUE);

	$CI->email->message($body);
	//Send mail
	if ($CI->email->send()) {
		$CI->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");
		return true;
		// echo 'send';
	} else {
		$CI->session->set_flashdata("email_fail", "You have encountered an error");
		return false;
		// show_error($CI->email->print_debugger());
	}
}
function getNotif()
{
	$CI = &get_instance();
	$CI->load->model('App_models');
	$id_member = $CI->session->userdata('member_sess')['id_member'];
	$get = $CI->App_models->get_Notifmember();
	return $get;
}
function getNotiftamu()
{
	$CI = &get_instance();
	$CI->load->model('App_models');
	// $id_member = $CI->session->userdata('tamu')['email'];
	$get = $CI->App_models->get_Notiftamu();
	return $get;
}
function notifBooking()
{
	$CI = &get_instance();
	$CI->load->database();
	$id_futsal = $CI->session->userdata('id_futsal');
	$CI->db->join('tb_lapangan', 'tb_lapangan.id_lapangan=tb_booking.id_lapangan');
	$CI->db->where('id_futsal', $id_futsal);
	$CI->db->where('status_booking', 'On Process');
	$get = $CI->db->get('tb_booking');
	if ($get->num_rows() > 0) {
		return $get;
	} else {
		return false;
	}
}
function refresPayment()
{
	$CI = &get_instance();
	$CI->load->database();
	$now = date('Y-m-d H:i:s');
	$end = date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($now)));
	$update = array('payment_stat' => 3);
	$CI->db->where('expired_time <', $now);
	$CI->db->where('status_booking', 'Pending');
	// $CI->db->where('payment_stat', 'Pending');
	$CI->db->update('tb_booking', $update);
}
