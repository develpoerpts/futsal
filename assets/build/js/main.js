function data_table() {
	// $('#tables').dataTable();
	var url = $('#tables').data('url');
	console.log(url);
	table = $('#tables').DataTable({
		dom: 'Bfrtip',
		"columnDefs": [{
			"width": "6%",
			"targets": 0
		}],
		"processing": true,
		"ajax": {
			"url": url,
			"type": "POST"
		}
	});
}

function select2() {
	var url = $('select#id_bkk').data('url');
	$('select#').select2({
		ajax: {
			url: url,
			type: "post",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					searchTerm: params.term
				};
			},
			processResults: function (response) {
				return {
					results: response
				};
			},
		}
	});
}

function remove_validation() {
	$('p.text-danger').remove();
	$('div.has-error').removeClass('has-error');
}

function reload_table() {
	table.ajax.reload(null, false); //reload datatable ajax
}

function save() {
	remove_validation();

	var url = $('#form_input').attr('action');

	metode = $('#saveBtn').data('metode');
	console.log(metode)
	var dataForm = $('#form_input').serialize() + '&metode=' + metode
	console.log(dataForm);
	$.ajax({
		url: url,
		type: "POST",
		data: dataForm,
		dataType: "JSON",
		success: function (data) {
			if (data.success) {
				new PNotify({
					title: 'Success!',
					text: data.notif,
					type: 'success',
					styling: 'bootstrap3'
				});
				$('#modal_form').modal('hide');
				$('#saveBtn').removeAttr('data-metode');
				reload_table();

			} else if (data.messages) {
				$.each(data.messages, function (key, value) {
					var element = $('#' + key);

					element.closest('div.form-group')
						.removeClass('has-error')
						.addClass(value.length > 0 ? 'has-error' : 'has-success')
						.find('.text-danger')
						.remove();

					element.after(value);

				})
			}
		},
		error: function (ts) {
			alert(ts.responseText)
		}
	})
}
function save_status() {
	remove_validation();

	var url = $('#form_input').attr('action');
	var dataForm = $('#form_input').serialize()
	console.log(dataForm);
	$.ajax({
		url: url,
		type: "POST",
		data: dataForm,
		dataType: "JSON",
		success: function (data) {
			if (data.success) {
				new PNotify({
					title: 'Success!',
					text: data.notif,
					type: 'success',
					styling: 'bootstrap3'
				});
				$('#modal_form').modal('hide');
				$('#saveBtn').removeAttr('data-metode');
				reload_table();

			} else if (data.messages) {
				$.each(data.messages, function (key, value) {
					var element = $('#' + key);

					element.closest('div.form-group')
						.removeClass('has-error')
						.addClass(value.length > 0 ? 'has-error' : 'has-success')
						.find('.text-danger')
						.remove();

					element.after(value);

				})
			}
		},
		error: function (ts) {
			alert(ts.responseText)
		}
	})
}

function save_lapangan() {
	console.log('Upload');
	var file_data = $('#foto_lapangan').prop('files')[0];
	var form_data = new FormData($('#form_input')[0]);
	var url = $('#form_input').attr('action');
	// form_data.append('foto_lapangan', file_data);
	$.ajax({
		url: url,
		type: "POST",
		data: form_data,
		dataType: "JSON",
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) {
			if (data.success) {
				new PNotify({
					title: 'Success!',
					text: data.notif,
					type: 'success',
					styling: 'bootstrap3'
				});
				$('#modal_form').modal('hide');
				$('#saveBtn').removeAttr('data-metode');
				reload_table();

			} else if (data.messages) {
				$.each(data.messages, function (key, value) {
					var element = $('#' + key);

					element.closest('div.form-group')
						.removeClass('has-error')
						.addClass(value.length > 0 ? 'has-error' : 'has-success')
						.find('.text-danger')
						.remove();

					element.after(value);

				})
			}
		},
		error: function (ts) {
			alert(ts.responseText)
		}
	})

}

function save_turnamen() {
	console.log('Upload');
	var file_data = $('#foto_turnamen').prop('files')[0];
	var file_data = $('#file_pendaftaran').prop('files')[0];
	var form_data = new FormData($('#form_input')[0]);
	var url = $('#form_input').attr('action');
	// form_data.append('foto_lapangan', file_data);
	$.ajax({
		url: url,
		type: "POST",
		data: form_data,
		dataType: "JSON",
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) {
			if (data.success) {
				new PNotify({
					title: 'Success!',
					text: data.notif,
					type: 'success',
					styling: 'bootstrap3'
				});
				$('#modal_form').modal('hide');
				$('#saveBtn').removeAttr('data-metode');
				reload_table();

			} else if (data.messages) {
				$.each(data.messages, function (key, value) {
					var element = $('#' + key);

					element.closest('div.form-group')
						.removeClass('has-error')
						.addClass(value.length > 0 ? 'has-error' : 'has-success')
						.find('.text-danger')
						.remove();

					element.after(value);

				})
			}
		},
		error: function (ts) {
			alert(ts.responseText)
			console.log(ts.responseText);
		}
	})

}


function save_pengguna() {
	remove_validation();
	var selected = $("select#id_bkk > option:selected").val();
	var url = $('#form_input').attr('action');
	console.log(url)
	var dataForm = $('#form_input').serialize() + '&id_bkk=' + selected;
	console.log(dataForm);
	$.ajax({
		url: url,
		type: "POST",
		data: dataForm,
		dataType: "JSON",
		success: function (data) {
			if (data.success) {
				new PNotify({
					title: 'Success!',
					text: data.notif,
					type: 'success',
					styling: 'bootstrap3'
				});
				$('#modal_form').modal('hide');
				reload_table();

			} else if (data.messages) {
				$.each(data.messages, function (key, value) {
					var element = $('#' + key);

					element.closest('div.form-group')
						.removeClass('has-error')
						.addClass(value.length > 0 ? 'has-error' : 'has-success')
						.find('.text-danger')
						.remove();

					element.after(value);

				})
			}
		},
		error: function (ts) {
			alert(ts.responseText)
		}
	})
}

function edit_modal(id) {
	remove_validation();
	$('#form_input')[0].reset();
	$('.modal-title').text('Edit Data');
	$('#modal_form').modal({
		backdrop: "static",
		show: true
	});
	$('#saveBtn').attr('data-metode', 'update');
	var url = $('#edit').data('url');
	console.log(url + id)
	$.ajax({
		url: url + id,
		type: "GET",
		dataType: "JSON",
		success: function (data) {
			$.each(data, function (key, value) {
				$('#' + key).val(value);
			});

		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert('Error get data from ajax');
		}
	});
}
// function untuk EDIT VIEW lapangan
function lapangan() {
	console.log('Lapangan Run');

	$("#tables tbody").on("click", "#edit_lapangan", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_lapangan').attr('type', 'file');
				$('#btnReset').show();
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table
	$("#tables tbody").on("click", "#view_lapangan", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				$('#foto_lapangan').attr('type', 'hidden'); //menghapus form input foto
				$('#btnReset').hide();
				$('#btnSave').hide();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', true) // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}



function bookingMember() {
	console.log('Transaksi Run');

	$("#tables tbody").on("click", "#bookingMember", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_lapangan').attr('type', 'file');
				$('#btnReset').show();
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}

function daftarMember() {
	console.log('Member Run');

	$("#tables tbody").on("click", "#edit_member", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_bukti_transfer_daftar').attr('type', 'file');
				$('#btnReset').show();
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table
	$("#tables tbody").on("click", "#view_daftar_member", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				$('#foto_bukti_transfer_daftar').attr('type', 'hidden'); //menghapus form input foto
				$('#btnReset').hide();

				$.each(data, function (key, value) {
					$('#metode').val('status');
					$('#' + key).val(value).attr('readonly', true)
					$('#status').attr('readonly', false); // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				if (data.foto_bukti_transfer_daftar != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto_bukti_transfer_daftar + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}



function booking() {
	console.log('Booking Run');

	$("#table-booking tbody").on("click", "#edit", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				$('#foto_transfer').attr('type', 'hidden'); //menghapus form input foto
				$('#btnReset').hide();

				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', true)
					$('#status_booking').attr('readonly', false); // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-12" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail" style="max-widht:100%">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})

	$("#tables tbody").on("click", "#edit_lapangan", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_lapangan').attr('type', 'file');
				$('#btnReset').show();
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table

}


function daftarTurnamen() {
	console.log('daftarTurnamen Run');

	$("#tables tbody").on("click", "#edit_daftar", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_bukti_transfer').attr('type', 'file');
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto_bukti_transfer != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto_bukti_transfer + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table
	$("#tables tbody").on("click", "#view_daftar", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				$('#btnSave').hide();
				$('#foto_bukti_transfer').attr('type', 'hidden'); //menghapus form input foto
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', true) // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				if (data.foto_bukti_transfer != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto_bukti_transfer + '"  class="thumbnail">' +
						'</div>';
					$('#image').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}



function turnamen() {
	console.log('Turnamen Run');

	$("#tables tbody").on("click", "#edit_turnamen", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#foto_turnamen').attr('type', 'file');
				$('#file_pendaftaran').attr('type', 'file');
				$('#btnReset').show();
				$('#btnSave').show();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				if (data.foto_turnamen != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto_turnamen + '"  class="thumbnail">' +
						'</div>';
					$('#image_turnamen').append(img);
				} else if (data.file_pendaftaran != '') {
					var filedf = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<text src="' + data.file_pendaftaran + '"  class="thumbnail">' +
						'</div>';
					$('#file_daftar').append(filedf);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table
	$("#tables tbody").on("click", "#view_turnamen", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				$('#foto_turnamen').attr('type', 'hidden');
				$('#file_pendaftaran').attr('type', 'text'); //menghapus form input foto
				$('#btnReset').hide();
				$('#btnSave').hide();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', true) // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				if (data.foto_turnamen != '') {
					var img = '<div class="col-xs-6 col-md-2" id="priview">' +
						'<img src="' + data.foto_turnamen + '"  class="thumbnail">' +
						'</div>';
					$('#image_turnamen').append(img);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}

function futsal() {
	console.log('Futsal Run');

	$("#tables tbody").on("click", "#edit_futsal", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Edit Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();

				//	$('#foto_lapangan').attr('type','hidden');//menghapus form input foto
				$('#btnResetFutsal').show();
				$('#btnSaveFutsal').show();
				$('#foto_lapangan').attr('type', 'file');
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', false); // menampikan dan mengubah readonly false
				});
				// if(data.foto!=''){
				// 	var img = '<div class="col-xs-6 col-md-2" id="priview">' +
				// 	'<img src="' + data.foto + '"  class="thumbnail">' +
				//                               '</div>';
				// $('#image').append(img);
				// }
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
	// get byId dari body table
	$("#tables tbody").on("click", "#view_futsal", function () {
		remove_validation();
		$('#form_input')[0].reset();
		$('.modal-title').text('Detail Data');
		$('#modal_form').modal({
			backdrop: "static",
			show: true
		});
		$('#saveBtn').attr('data-metode', 'update');
		var url = $(this).data('url');
		console.log(url)
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			success: function (data) {
				$('#priview').remove();
				$('#priview').hide();
				//	$('#foto_lapangan').attr('type','hidden');//menghapus form input foto
				$('#btnResetFutsal').hide();
				$('#btnSaveFutsal').hide();
				$.each(data, function (key, value) {
					$('#' + key).val(value).attr('readonly', true) // menampilkan data dengan mengubah readonly
				});
				// cek jika foto kosong pada db maka tidak menampilkan view
				// if(data.foto!=''){
				// 	var img = '<div class="col-xs-6 col-md-2" id="priview">' +
				// 	'<img src="' + data.foto + '"  class="thumbnail">' +
				// 															'</div>';
				// $('#image').append(img);
				// }
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	})
}

$(function () {
	var myTable = $('#table-booking').DataTable({
		bAutoWidth: false,
		processing: true,
		paging: true,
		ordering: true,
		ajax: {
			url: $("#table-booking").data("url"),
			type: "POST",
			data: {
				'start': function () {
					return $('#start').val()
				},
				'end': function () {
					return $('#end').val()
				}
			},
			columnDefs: [{
				className: "text-center",
				targets: [0, -1]
			}]
		}
	});
	$('#show-filter').click(function (e) {
		e.preventDefault();
		myTable.ajax.reload();
	});

});

$(function () {
	var myTable = $('#tables-booking-af').DataTable({
		bAutoWidth: false,
		processing: true,
		paging: true,
		ordering: true,
		ajax: {
			url: $("#tables-booking-af").data("url"),
			type: "POST",
			data: {
				'start': function () {
					return $('#start').val()
				},
				'end': function () {
					return $('#end').val()
				}
			},
			columnDefs: [{
				className: "text-center",
				targets: [0, -1]
			}]
		}
	});
	$('#show-filter-af').click(function (e) {
		e.preventDefault();
		myTable.ajax.reload();
	});
});


$(function () {
	var myTable = $('#tables-booking-afr').DataTable({
		bAutoWidth: false,
		processing: true,
		paging: true,
		ordering: true,
		ajax: {
			url: $("#tables-booking-afr").data("url"),
			type: "POST",
			data: {
				'start': function () {
					return $('#start').val()
				},
				'end': function () {
					return $('#end').val()
				}
			},
			columnDefs: [{
				className: "text-center",
				targets: [0, -1]
			}]
		}
	});
	$('#show-filter-afr').click(function (e) {
		e.preventDefault();
		myTable.ajax.reload();
	});
});
$(document).ready(function () {
	data_table();
	lapangan();
	futsal();
	daftarTurnamen();
	turnamen();
	daftarMember();
	booking();
	bookingMember();

	if ($('#jam_graph').length) {
		$.getJSON($('#jam_graph').data('url'),
			function (data) {
				Morris.Bar({
					element: 'jam_graph',
					data: data.data,
					xkey: 'lapangan',
					ykeys: ['nilai'],
					labels: ['Lama'],
					// parseTime: false,
					// barRatio: 0.4,
					// barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
					xLabelAngle: 35,
					hideHover: 'auto',
					resize: true
				});
			});

		// Morris.Bar({
		// 	element: 'jam_graph1',
		// 	data: [
		// 		{ device: 'iPhone 4', geekbench: 380 },
		// 		{ device: 'iPhone 4S', geekbench: 655 },
		// 		{ device: 'iPhone 3GS', geekbench: 275 },
		// 		{ device: 'iPhone 5', geekbench: 1571 },
		// 		{ device: 'iPhone 5S', geekbench: 655 },
		// 		{ device: 'iPhone 6', geekbench: 2154 },
		// 		{ device: 'iPhone 6 Plus', geekbench: 1144 },
		// 		{ device: 'iPhone 6S', geekbench: 2371 },
		// 		{ device: 'iPhone 6S Plus', geekbench: 1471 },
		// 		{ device: 'Other', geekbench: 1371 }
		// 	],
		// 	xkey: 'device',
		// 	ykeys: ['geekbench'],
		// 	labels: ['Geekbench'],
		// 	barRatio: 0.4,
		// 	barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
		// 	xLabelAngle: 35,
		// 	hideHover: 'auto',
		// 	resize: true
		// });

	}
});
