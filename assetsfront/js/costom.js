function checkbook() {
    console.log('time');

}
$(document).ready(function () {
    $('#jam_booking').change(function (e) {
        e.preventDefault();
        console.log('time');
        var url = $(this).data('url');
        var dataform = $('#form_input1').serializeArray();
        dataform.push({ name: "id_lapangan", value: $('#id_lapangan').val() })
        dataform.push({ name: "tanggal_booking", value: $('#tanggal_booking').val() })
        dataform.push({ name: "lama_booking", value: $('#lama_booking').val() })
        dataform.push({ name: "jam_booking", value: $(this).val() })
        console.log('pop');
        $.ajax({
            type: "POST",
            url: url,
            data: dataform,
            dataType: "JSON",
            success: function (data) {
                if (data.gagal) {
                    $('#span-booking').show();
                    $("#btn-member").hide();
                    $("#btn-submit").hide();
                }
                if (data.success) {
                    $('#span-booking').hide();
                    $("#btn-member").show();
                    $("#btn-submit").show();
                }
            }
        });

    });
    $('#btn-submit').click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        var dataform = $('#form_input1').serializeArray();
        // alert(url);
        // console.log(dataform);
        $.ajax({
            url: url,
            type: "POST",
            data: dataform,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    window.location.replace(data.go_to);

                } else if (data.messages) {
                    $.each(data.messages, function (key, value) {
                        var element = $('#' + key);

                        element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();

                        element.after(value);

                    })
                }
            },
            error: function (ts) {
                alert(ts.responseText)
            }
        })
    });
    var myTable =
        $('#info-booking')
            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
            .DataTable({
                bAutoWidth: true,
                processing: true,
                serverSide: false,
                paging: true,
                ordering: true,
                ajax: {
                    url: $("#info-booking").data("url"),
                    type: "POST",
                    data: { 'tanggal': function () { return $('#date_hidden').val() } }
                },
                columnDefs: [{
                    className: "text-center",
                    targets: [0, -1]
                }]
            });
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
    });
    $('#datepicker').on('changeDate', function () {
        $('#date_hidden').val(
            $('#datepicker').datepicker('getFormattedDate')
        );
        var now_date = $('#date_hidden').val();
        myTable.ajax.reload();
        console.log(now_date);
    });
    $('#nama_booking').keyup(function () {
        $.ajax({
            type: "POST",
            url: $(this).data('validation'),
            data: { 'nama_booking': $(this).val() },
            dataType: "JSON",
            success: function (res) {
                if (res.messages) {
                    $.each(res.messages, function (key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                        element.after(value);
                    })
                } else if (res.success) {
                    var element = $('#nama_booking');
                    element.closest('div.form-group')
                        .removeClass('has-error')
                        .addClass('has-success')
                        .find('.text-danger')
                        .remove();
                }
            }
        });
    });
    $('#no_hp_booking').keyup(function () {
        $.ajax({
            type: "POST",
            url: $(this).data('validation'),
            data: { 'no_hp_booking': $(this).val() },
            dataType: "JSON",
            success: function (res) {
                if (res.messages) {
                    $.each(res.messages, function (key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                        element.after(value);
                    })
                } else if (res.success) {
                    var element = $('#no_hp_booking');
                    element.closest('div.form-group')
                        .removeClass('has-error')
                        .addClass('has-success')
                        .find('.text-danger')
                        .remove();
                }
            }
        });
    });
    $('#email').keyup(function () {
        $.ajax({
            type: "POST",
            url: $(this).data('validation'),
            data: { 'email': $(this).val() },
            dataType: "JSON",
            success: function (res) {
                if (res.messages) {
                    $.each(res.messages, function (key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length > 0 ? 'has-error' : 'has-success')
                            .find('.text-danger')
                            .remove();
                        element.after(value);
                    })
                } else if (res.success) {
                    var element = $('#email');
                    element.closest('div.form-group')
                        .removeClass('has-error')
                        .addClass('has-success')
                        .find('.text-danger')
                        .remove();
                }
            }
        });
    });
    function hidebtn() {
        $("#btn-member").hide();
        $("#btn-submit").hide();
    }
    function show(params) {

    }
});