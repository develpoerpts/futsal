/*
 Navicat Premium Data Transfer

 Source Server         : Root
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : futsal

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 16/07/2020 23:20:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES (1, 'feri rotinsulu', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@email.com');

-- ----------------------------
-- Table structure for tb_booking
-- ----------------------------
DROP TABLE IF EXISTS `tb_booking`;
CREATE TABLE `tb_booking`  (
  `id_booking` int(11) NOT NULL AUTO_INCREMENT,
  `no_booking` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_booking` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_booking` date NOT NULL,
  `jam_booking` time(0) NOT NULL,
  `selesai_booking` time(0) NULL DEFAULT NULL,
  `lama_booking` int(11) NOT NULL,
  `no_hp_booking` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_member` enum('Member','Non Member','','') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_lapangan` int(11) NOT NULL,
  `id_member` int(11) NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `total_harga` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_booking` enum('Pending','On Process','Booked','Closed','Expired') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto_transfer` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_rekening_booking` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `payment_stat` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `expired_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_booking`) USING BTREE,
  UNIQUE INDEX `no_booking`(`no_booking`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_booking
-- ----------------------------
INSERT INTO `tb_booking` VALUES (40, 'I7WXQ', 'rfv', '2019-06-25', '12:00:00', '12:00:00', 3, '08656282913', 'Non Member', 1, NULL, '', '300000', 'Pending', '', '', '0', NULL);
INSERT INTO `tb_booking` VALUES (41, 'MXS1M', 'ecang', '2019-06-27', '09:00:00', '12:00:00', 3, '093848399223', 'Non Member', 1, NULL, '', '300000', 'Closed', '', '', '0', NULL);
INSERT INTO `tb_booking` VALUES (42, '2JM72', 'ecang', '2019-06-27', '09:00:00', '12:00:00', 3, '093848399223', 'Non Member', 1, NULL, '', '300000', 'On Process', '', 'coba nama rek lagi 2', '0', NULL);
INSERT INTO `tb_booking` VALUES (43, 'YV67E', 'coba member baru lagi', '2019-06-23', '12:12:00', '14:12:00', 2, '+6285740684929', 'Member', 3, NULL, '', '270000', 'Closed', '', 'cang re', '0', NULL);
INSERT INTO `tb_booking` VALUES (44, 'ZUXcN', 'fajar handika', '2019-06-24', '12:00:00', '15:00:00', 3, '093848399223', 'Non Member', 1, NULL, '', '300000', 'Closed', '', 'rek fajar', '0', NULL);
INSERT INTO `tb_booking` VALUES (45, 'W7U3K', 'coba member baru lagi', '2019-06-25', '10:10:00', '13:10:00', 3, '+6285740684929', 'Member', 5, NULL, '', '270000', 'Pending', '', '', '3', '2020-07-14 23:02:59');
INSERT INTO `tb_booking` VALUES (46, '1Y32b', 'coba baru', '2019-06-24', '10:00:00', '12:00:00', 2, '08656282913', 'Non Member', 1, NULL, '', '200000', 'On Process', '', 'coba nama rek', '0', NULL);
INSERT INTO `tb_booking` VALUES (47, '1762714', 'coba baru 2', '2020-06-16', '17:00:00', '18:00:00', 1, '08656282913', 'Non Member', 2, NULL, '', '130000', 'Pending', '', '', '3', '2020-07-14 23:03:07');
INSERT INTO `tb_booking` VALUES (48, '8819344', 'coba baru 2', '2020-06-16', '19:00:00', '21:00:00', 2, '08656282913', 'Non Member', 1, NULL, '', '200000', 'Booked', '', 'rek fajar', '0', NULL);
INSERT INTO `tb_booking` VALUES (49, '2541321', 'coba baru 3', '2020-06-16', '17:00:00', '19:00:00', 2, '08656282913', 'Non Member', 1, NULL, '', '200000', 'Pending', '', '', '0', NULL);
INSERT INTO `tb_booking` VALUES (50, '5824321', 'sandy', '2020-06-19', '15:00:00', '08:00:07', 1, '09809080', 'Non Member', 3, NULL, '', '150000', 'Pending', '', '', '0', NULL);
INSERT INTO `tb_booking` VALUES (51, '7531939', 'sandy', '2020-06-25', '10:00:00', '11:00:00', 1, '09809080', 'Non Member', 3, NULL, '', '150000', 'Booked', '', 'Sandy', '0', NULL);
INSERT INTO `tb_booking` VALUES (64, '8662231', 'sandy', '2020-07-08', '10:00:00', '11:00:00', 1, '09809080', 'Non Member', 3, NULL, 'sandytesar@gmail.com', '150000', 'Booked', '', 'ecang', '0', NULL);
INSERT INTO `tb_booking` VALUES (65, '2182598', 'zidan', '2020-07-09', '11:00:00', '12:00:00', 1, '123456', 'Member', 3, 7, 'ecangsandy@gmail.com', '135000', 'Closed', '1594143986076.jpg', 'Edi', '0', NULL);
INSERT INTO `tb_booking` VALUES (66, '6766479', 'zidan', '2020-07-15', '10:00:00', '11:00:00', 1, '123456', 'Member', 3, 7, 'ecangsandy@gmail.com', '135000', 'Booked', '1594740548582.png', 'ZIDANE', '0', NULL);
INSERT INTO `tb_booking` VALUES (72, '5621444', 'arsa', '2020-07-17', '10:10:00', '11:10:00', 1, '09809080', 'Non Member', 2, NULL, 'admin@admin.com', '130000', 'Pending', '', '', '0', '2020-07-17 00:11:35');

-- ----------------------------
-- Table structure for tb_daftar_turnamen
-- ----------------------------
DROP TABLE IF EXISTS `tb_daftar_turnamen`;
CREATE TABLE `tb_daftar_turnamen`  (
  `id_daftar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pendaftar` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telephone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_formulir` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_turnamen` int(11) NOT NULL,
  `foto_bukti_transfer` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_daftar` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_daftar` enum('Pending','Accept','','') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_daftar`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_daftar_turnamen
-- ----------------------------
INSERT INTO `tb_daftar_turnamen` VALUES (1, 'nama saya2', '08578363527', '1554174764406.pdf', 3, '1553658766894.jpg', 'XnbhAd', 'Accept');

-- ----------------------------
-- Table structure for tb_futsal
-- ----------------------------
DROP TABLE IF EXISTS `tb_futsal`;
CREATE TABLE `tb_futsal`  (
  `id_futsal` int(11) NOT NULL AUTO_INCREMENT,
  `nama_futsal` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fasilitas` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_bank` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_rek` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_rek` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telepon` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_futsal`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_futsal
-- ----------------------------
INSERT INTO `tb_futsal` VALUES (1, 'Dewi sri', 'Jalan Sama Aku Nikah Sama Dia, Perih', 'murah lah pokoky', '1. Kamar mandi\r\n2. Toilet', 'BNI', '192822132132', 'dewi sri', '09080909090909', 'dewisri', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `tb_futsal` VALUES (2, 'Rajawali Futsal', 'Jatibarang', 'Belaknang hotel ', '', 'BNI', '12836917301', 'susilo', NULL, 'rajawali', '21232f297a57a5a743894a0e4a801fc3');
INSERT INTO `tb_futsal` VALUES (3, 'Rajawali Futsal', 'Kersana', 'Satu satunya tempat futsal dikersana', '', 'BRI', '12340912384319', 'prabowo', NULL, 'rajawalifutsal', '21232f297a57a5a743894a0e4a801fc3');
INSERT INTO `tb_futsal` VALUES (6, 'TES', 'tes', 'tes', '', 'tes', '1010010', '', NULL, 'tes', 'e10adc3949ba59abbe56e057f20f883e');

-- ----------------------------
-- Table structure for tb_jenis_lapangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_jenis_lapangan`;
CREATE TABLE `tb_jenis_lapangan`  (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_jenis`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_jenis_lapangan
-- ----------------------------
INSERT INTO `tb_jenis_lapangan` VALUES (1, 'Indor Sintetis');
INSERT INTO `tb_jenis_lapangan` VALUES (2, 'Indor Vinyl');
INSERT INTO `tb_jenis_lapangan` VALUES (3, 'Outdur Sistetis');
INSERT INTO `tb_jenis_lapangan` VALUES (5, 'Outdor Vynil');

-- ----------------------------
-- Table structure for tb_lapangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_lapangan`;
CREATE TABLE `tb_lapangan`  (
  `id_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `id_futsal` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `harga_non_member` int(11) NOT NULL,
  `diskon_member` int(2) NOT NULL,
  `foto_lapangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ukuran_lapangan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `info_lainnya` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status_lapangan` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id_lapangan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_lapangan
-- ----------------------------
INSERT INTO `tb_lapangan` VALUES (1, 1, 1, 100000, 5, '1553658766894.jpg', '10x30', 'bangku penonton', 0);
INSERT INTO `tb_lapangan` VALUES (2, 2, 2, 130000, 7, '1553519484914.jpg', NULL, NULL, 0);
INSERT INTO `tb_lapangan` VALUES (3, 1, 3, 150000, 10, '1553658790647.jpg', NULL, NULL, 0);
INSERT INTO `tb_lapangan` VALUES (4, 1, 2, 150000, 15, '1553612551471.jpg', NULL, NULL, 0);
INSERT INTO `tb_lapangan` VALUES (5, 1, 3, 100000, 10, '1555987140127.jpg', NULL, NULL, 1);

-- ----------------------------
-- Table structure for tb_member
-- ----------------------------
DROP TABLE IF EXISTS `tb_member`;
CREATE TABLE `tb_member`  (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_member` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto_bukti_transfer_daftar` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(34) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telephone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` enum('Non Aktif','Aktif','','') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_member`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_member
-- ----------------------------
INSERT INTO `tb_member` VALUES (4, 'coba', 'coba', 'coba1.png', 'coba@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '85740684929', 'Aktif');
INSERT INTO `tb_member` VALUES (5, 'coba member baru', 'alamat baru', 'coba_member_baru.jpeg', 'anggit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '+6285740684929', 'Aktif');
INSERT INTO `tb_member` VALUES (7, 'zidan', 'tegal', 'zidan.png', 'ecangsandy@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Aktif');

-- ----------------------------
-- Table structure for tb_stemple
-- ----------------------------
DROP TABLE IF EXISTS `tb_stemple`;
CREATE TABLE `tb_stemple`  (
  `id_stempel` int(11) NOT NULL AUTO_INCREMENT,
  `id_futsal` int(11) NOT NULL,
  `stampel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_stempel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_stemple
-- ----------------------------
INSERT INTO `tb_stemple` VALUES (1, 1, 'stample1.png');
INSERT INTO `tb_stemple` VALUES (2, 1, 'stample1.png');

-- ----------------------------
-- Table structure for tb_turnamen
-- ----------------------------
DROP TABLE IF EXISTS `tb_turnamen`;
CREATE TABLE `tb_turnamen`  (
  `id_turnamen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_turnamen` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_turnamen` date NOT NULL,
  `id_futsal` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `biaya_daftar` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `syarat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `file_pendaftaran` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telephone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto_turnamen` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_turnamen` enum('Incomming','Now','Finished','') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_turnamen`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_turnamen
-- ----------------------------
INSERT INTO `tb_turnamen` VALUES (3, 'coba turnamen', '2019-04-30', 1, 'ddddddddddddd', '20000', 'eeeeeeeeeeeeee', '1554174764406.pdf', '85740684929', '1554174764291.jpg', 'Incomming');

SET FOREIGN_KEY_CHECKS = 1;
