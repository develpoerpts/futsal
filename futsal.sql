/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 100134
Source Host           : localhost:3306
Source Database       : futsal

Target Server Type    : MYSQL
Target Server Version : 100134
File Encoding         : 65001

Date: 2020-07-05 14:05:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('1', 'feri rotinsulu', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@email.com');

-- ----------------------------
-- Table structure for tb_booking
-- ----------------------------
DROP TABLE IF EXISTS `tb_booking`;
CREATE TABLE `tb_booking` (
  `id_booking` int(11) NOT NULL AUTO_INCREMENT,
  `no_booking` varchar(20) NOT NULL,
  `nama_booking` varchar(30) NOT NULL,
  `tanggal_booking` date NOT NULL,
  `jam_booking` time NOT NULL,
  `selesai_booking` time DEFAULT NULL,
  `lama_booking` int(11) NOT NULL,
  `no_hp_booking` varchar(15) NOT NULL,
  `status_member` enum('Member','Non Member','','') NOT NULL,
  `id_lapangan` int(11) NOT NULL,
  `total_harga` varchar(10) NOT NULL,
  `status_booking` enum('Pending','On Process','Booked','Closed') NOT NULL,
  `foto_transfer` varchar(25) NOT NULL,
  `nama_rekening_booking` varchar(25) NOT NULL,
  PRIMARY KEY (`id_booking`) USING BTREE,
  UNIQUE KEY `no_booking` (`no_booking`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_booking
-- ----------------------------
INSERT INTO `tb_booking` VALUES ('49', '2541321', 'coba baru 3', '2020-06-16', '17:00:00', '19:00:00', '2', '08656282913', 'Non Member', '1', '200000', 'Pending', '', '');
INSERT INTO `tb_booking` VALUES ('50', '5824321', 'sandy', '2020-06-19', '15:00:00', '08:00:07', '1', '09809080', 'Non Member', '3', '150000', 'Pending', '', '');
INSERT INTO `tb_booking` VALUES ('51', '6392156', 'peri', '2020-06-30', '17:24:00', '02:00:01', '1', '09898987888', 'Non Member', '1', '100000', 'On Process', '', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('52', '8844915', 'coba baru 2', '2020-06-30', '17:43:00', '02:00:01', '1', '08656282913', 'Non Member', '1', '100000', 'On Process', '', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('53', '7921574', 'coba baru 2', '2020-06-25', '18:12:00', '03:00:01', '2', '08656282913', 'Non Member', '1', '200000', 'On Process', '', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('55', '8329268', 'coba baru 2', '2020-06-29', '14:40:00', '02:00:01', '1', '08656282913', 'Non Member', '1', '100000', 'Closed', '1593416439866.jpg', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('56', '7842641', 'coba baru 2', '2020-06-29', '20:38:00', '02:00:01', '1', '08656282913', 'Non Member', '1', '100000', 'On Process', '1593437882517.png', 'cang re');
INSERT INTO `tb_booking` VALUES ('57', '2237276', 'feri', '2020-06-29', '21:25:00', '03:00:01', '2', '08656282913', 'Non Member', '1', '200000', 'On Process', '1593440761148.jpg', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('58', '7539669', 'wwwsss', '2020-06-29', '22:09:00', '03:00:01', '2', '093848399223', 'Non Member', '1', '200000', 'Pending', '', '');
INSERT INTO `tb_booking` VALUES ('59', '1892274', 'coba baru 2', '2020-06-30', '00:00:00', '03:00:01', '2', '08656282913', 'Non Member', '2', '260000', 'Pending', '', '');
INSERT INTO `tb_booking` VALUES ('60', '8293636', 'coba baru 2', '2020-06-30', '07:21:00', '09:21:00', '2', '08656282913', 'Non Member', '1', '200000', 'Booked', '1593476479953.jpg', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('61', '6422498', 'coba baru 3', '2020-06-30', '07:22:00', '03:00:01', '2', '09898987888', 'Non Member', '1', '200000', 'Booked', '1593476588659.jpg', 'cang re');
INSERT INTO `tb_booking` VALUES ('62', '9931652', 'TES', '2020-06-30', '14:00:00', '16:00:00', '2', '09898987888', 'Non Member', '1', '200000', 'Booked', '1593477789638.jpg', 'coba nama rek');
INSERT INTO `tb_booking` VALUES ('63', '1688241', 'coba baru 10', '2020-06-30', '15:00:00', '16:00:00', '1', '08656282913', 'Non Member', '1', '100000', 'Pending', '', '');
INSERT INTO `tb_booking` VALUES ('64', '8492941', 'coba baru 2', '2020-06-30', '22:00:00', '23:00:00', '1', '08656282913', 'Non Member', '1', '200000', 'Booked', '1593525956385.jpg', 'coba nama rek');

-- ----------------------------
-- Table structure for tb_daftar_turnamen
-- ----------------------------
DROP TABLE IF EXISTS `tb_daftar_turnamen`;
CREATE TABLE `tb_daftar_turnamen` (
  `id_daftar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pendaftar` varchar(25) NOT NULL,
  `no_telephone` varchar(15) NOT NULL,
  `file_formulir` text NOT NULL,
  `id_turnamen` int(11) NOT NULL,
  `foto_bukti_transfer` varchar(25) NOT NULL,
  `kode_daftar` varchar(25) NOT NULL,
  `status_daftar` enum('Pending','Accept','','') NOT NULL,
  PRIMARY KEY (`id_daftar`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_daftar_turnamen
-- ----------------------------
INSERT INTO `tb_daftar_turnamen` VALUES ('1', 'nama saya2', '08578363527', '1554174764406.pdf', '3', '1553658766894.jpg', 'XnbhAd', 'Accept');

-- ----------------------------
-- Table structure for tb_futsal
-- ----------------------------
DROP TABLE IF EXISTS `tb_futsal`;
CREATE TABLE `tb_futsal` (
  `id_futsal` int(11) NOT NULL AUTO_INCREMENT,
  `nama_futsal` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `fasilitas` text NOT NULL,
  `nama_bank` varchar(25) NOT NULL,
  `no_rek` varchar(20) NOT NULL,
  `nama_rek` varchar(20) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(40) NOT NULL,
  `no_telepon` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_futsal`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_futsal
-- ----------------------------
INSERT INTO `tb_futsal` VALUES ('1', 'Dewi sri', 'Jalan Sama Aku Nikah Sama Dia, Perih', 'murah lah pokoky', '1. Kamar mandi\r\n2. Toilet', 'BNI', '192822132132', 'dewi sri', 'dewisri', 'e10adc3949ba59abbe56e057f20f883e', null);
INSERT INTO `tb_futsal` VALUES ('2', 'Rajawali Futsal', 'Jatibarang', 'Belaknang hotel ', '', 'BNI', '12836917301', 'susilo', 'rajawali', '21232f297a57a5a743894a0e4a801fc3', null);
INSERT INTO `tb_futsal` VALUES ('3', 'Rajawali Futsal', 'Kersana', 'Satu satunya tempat futsal dikersana', '', 'BRI', '12340912384319', 'prabowo', 'rajawalifutsal', '21232f297a57a5a743894a0e4a801fc3', null);
INSERT INTO `tb_futsal` VALUES ('6', 'TES', 'tes', 'tes', '', 'tes', '1010010', '', 'tes', 'e10adc3949ba59abbe56e057f20f883e', null);

-- ----------------------------
-- Table structure for tb_jenis_lapangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_jenis_lapangan`;
CREATE TABLE `tb_jenis_lapangan` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_jenis_lapangan
-- ----------------------------
INSERT INTO `tb_jenis_lapangan` VALUES ('1', 'Indor Sintetis');
INSERT INTO `tb_jenis_lapangan` VALUES ('2', 'Indor Vinyl');
INSERT INTO `tb_jenis_lapangan` VALUES ('3', 'Outdur Sistetis');
INSERT INTO `tb_jenis_lapangan` VALUES ('5', 'Outdor Vynil');

-- ----------------------------
-- Table structure for tb_lapangan
-- ----------------------------
DROP TABLE IF EXISTS `tb_lapangan`;
CREATE TABLE `tb_lapangan` (
  `id_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `id_futsal` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `harga_non_member` int(11) NOT NULL,
  `diskon_member` int(2) NOT NULL,
  `foto_lapangan` varchar(50) NOT NULL,
  `ukuran_lapangan` varchar(50) DEFAULT NULL,
  `info_lainnya` text,
  `status_lapangan` int(1) DEFAULT '0',
  PRIMARY KEY (`id_lapangan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_lapangan
-- ----------------------------
INSERT INTO `tb_lapangan` VALUES ('1', '1', '1', '100000', '5', '1553658766894.jpg', '10x30', 'bangku penonton', '0');
INSERT INTO `tb_lapangan` VALUES ('2', '2', '2', '130000', '7', '1553519484914.jpg', null, null, '0');
INSERT INTO `tb_lapangan` VALUES ('3', '1', '3', '150000', '10', '1553658790647.jpg', null, null, '0');
INSERT INTO `tb_lapangan` VALUES ('4', '1', '2', '150000', '15', '1553612551471.jpg', null, null, '0');
INSERT INTO `tb_lapangan` VALUES ('5', '1', '3', '100000', '10', '1555987140127.jpg', null, null, '0');

-- ----------------------------
-- Table structure for tb_member
-- ----------------------------
DROP TABLE IF EXISTS `tb_member`;
CREATE TABLE `tb_member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(50) NOT NULL,
  `alamat_member` varchar(50) NOT NULL,
  `foto_bukti_transfer_daftar` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(34) NOT NULL,
  `no_telephone` varchar(15) NOT NULL,
  `status` enum('Non Aktif','Aktif','','') NOT NULL,
  PRIMARY KEY (`id_member`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_member
-- ----------------------------
INSERT INTO `tb_member` VALUES ('4', 'coba', 'coba', 'coba1.png', 'coba@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '85740684929', 'Aktif');
INSERT INTO `tb_member` VALUES ('5', 'coba member baru', 'alamat baru', 'coba_member_baru.jpeg', 'anggit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '+6285740684929', 'Non Aktif');
INSERT INTO `tb_member` VALUES ('6', 'coba member baru lagi', 'alamat baru lago', 'coba_member_baru_lagi.png', 'dwiseptirizqiana976@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '+6285740684929', 'Non Aktif');

-- ----------------------------
-- Table structure for tb_turnamen
-- ----------------------------
DROP TABLE IF EXISTS `tb_turnamen`;
CREATE TABLE `tb_turnamen` (
  `id_turnamen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_turnamen` varchar(50) NOT NULL,
  `tanggal_turnamen` date NOT NULL,
  `id_futsal` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `biaya_daftar` varchar(15) NOT NULL,
  `syarat` text NOT NULL,
  `file_pendaftaran` varchar(25) NOT NULL,
  `no_telephone` varchar(15) NOT NULL,
  `foto_turnamen` varchar(25) NOT NULL,
  `status_turnamen` enum('Incomming','Now','Finished','') NOT NULL,
  PRIMARY KEY (`id_turnamen`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_turnamen
-- ----------------------------
INSERT INTO `tb_turnamen` VALUES ('3', 'coba turnamen', '2019-04-30', '1', 'ddddddddddddd', '20000', 'eeeeeeeeeeeeee', '1554174764406.pdf', '85740684929', '1554174764291.jpg', 'Incomming');
SET FOREIGN_KEY_CHECKS=1;
